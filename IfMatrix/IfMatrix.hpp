
#include "Interface.h"

//#define CONVOLUTION_ACTIVATION_TANH
#define CONVOLUTION_ACTIVATION_RELU

#define CONVOLUTION_COLUMNS		3
#define CONVOLUTION_ROWS		3
#define MAX_CONVOLUTION_WIDTH	512

#define matrixFormat float

#define ACTIVATION_TYPE

enum ActivationType : int
{
	ActivationTypeTanh = 0,
	ActivationTypeRelu
};

#ifndef USE_CUDA
#include "stdafx.h"


#include "mkl.h"
#include <ippcv.h>
#include <ippi.h>
#include "if_threadpool.hpp"

#else

//#define USE_SEPARATE_STREAM

typedef struct {
	int width;
	int height;
	int stride;
	matrixFormat * elements;
} CudaMatrix;

#endif

#pragma pack(8)

struct ConvolutionInfo
{
	pt08u * convolutionTempBuffer;
	pt32u sizeS1_width;
	pt32u sizeS1_height;
	pt32u sizeS2_width;
	pt32u sizeS2_height;
	ActivationType activation;
};

#pragma pack()

__interface IfMatrix : public PtInterface
{
	void InitWithRandomValues(bool lastColumnIsBais);
	void InitWithZero(void);
	void Dot(IfMatrix * _matrixA, IfMatrix * _matrixB, matrixFormat alpha);
	void Dot_aT(IfMatrix * _matrixA, IfMatrix * _matrixB, matrixFormat alpha);
	void Dot_bT(IfMatrix * _matrixA, IfMatrix * _matrixB, matrixFormat alpha);
	void ActivationTanh(IfMatrix * _matrixA);
	void ActivationRelu(IfMatrix * _matrixA);
	void GradientTanh(IfMatrix * _matrixA, IfMatrix * _matrixB);
	void GradientRelu(IfMatrix * _matrixA, IfMatrix * _matrixB);
	void Add(IfMatrix * _matrixA);
	void Subtract(IfMatrix * _matrixA);
	void Multiply(IfMatrix * _matrixA);
	void Copy(IfMatrix * _matrixA);
	void Copy(int startIndex, IfMatrix * _matrixA);
	void Copy(IfMatrix * _matrixA, int sourceIndex);
	void Rotate180(IfMatrix * _matrixA);
	matrixFormat SquareSumm(void);
	void Convolution(IfMatrix * _weights, IfMatrix * _inputMatrix, ConvolutionInfo * convolutionInfo);
	void ConvolutionErrorSumm(IfMatrix * _errorMatrix, float lRate);
	void MaxPooling(IfMatrix * _input);
	void MaxExpand(IfMatrix * _input, IfMatrix * _referenceData);
	void SetBuffer(matrixFormat * data, int dataSize);
	void GetBuffer(matrixFormat * data, int dataSize);
	int Rows(void);
	int Columns(void);
};

struct PredictParams
{
	IfMatrix * inputSignal;
	IfMatrix * outputSignal;
	IfMatrix * inputBuffer;
	IfMatrix * weights;
	IfMatrix * signals;	
	ConvolutionInfo * convolutionInfo;
};

struct BackPropagationParams
{
	IfMatrix * errorCurrentLayer;
	IfMatrix * outputCurrentLayer;
	IfMatrix * outputPreviousLayer;
	IfMatrix * errorPreviousLayer;
	float lRate;
	IfMatrix * delta;
	IfMatrix * errorMatrix;
	IfMatrix * weights;
	ConvolutionInfo * convolutionInfo;
};

__interface IfNnPrimitives
{
	void PredictDense(PredictParams &predictParams);
	void BackPropagationDense(BackPropagationParams &backPropagationParams);
	void PredictConvolution(PredictParams &predictParams);
	void BackPropagationConvolution(BackPropagationParams &backPropagationParams);
	void PredictPooling(PredictParams &predictParams);
	void BackPropagationPooling(BackPropagationParams &backPropagationParams);
};

#ifndef CL_IF_MATRIX

class MatrixClassFactory : public PtInterface
{
	HMODULE hModule;
	IfMatrix * (*entry1)(void);
	IfMatrix * (*entry2)(int, int);
	IfMatrix * (*entry3)(int, int, matrixFormat *);
	IfNnPrimitives * (*entry4)(void);

	int instance;

public:
	MatrixClassFactory(PREFER_DEVICE prefer)
	{
		instance = 0;

		if (prefer == PREFER_DEVICE::PREFER_CPU)
		{
			hModule = LoadLibrary("IfMatrix.dll");

			if (hModule == NULL)
				hModule = LoadLibrary("IfMatrixCuda.dll");
		}
		else
		{
			hModule = LoadLibrary("IfMatrixCuda.dll");

			if (hModule == NULL)
				hModule = LoadLibrary("IfMatrix.dll");
		}

		if (hModule != NULL)
		{
#ifndef _M_X64 
			entry1 = (IfMatrix * (*)(void))GetProcAddress(hModule, "?CreateIfMatrix@@YAPAUIfMatrix@@XZ");
			entry2 = (IfMatrix * (*)(int, int))GetProcAddress(hModule, "?CreateIfMatrix@@YAPAUIfMatrix@@HH@Z");
			entry3 = (IfMatrix * (*)(int, int, matrixFormat *))GetProcAddress(hModule, "?CreateIfMatrix@@YAPAUIfMatrix@@HHPAM@Z");
			entry4 = (IfNnPrimitives * (*)(void))GetProcAddress(hModule, "?GetPrimitives@@YAPAUIfNnPrimitives@@XZ");
#else
			entry1 = (IfMatrix * (*)(void))GetProcAddress(hModule, "?CreateIfMatrix@@YAPEAUIfMatrix@@XZ");
			entry2 = (IfMatrix * (*)(int, int))GetProcAddress(hModule, "?CreateIfMatrix@@YAPEAUIfMatrix@@HH@Z");
			entry3 = (IfMatrix * (*)(int, int, matrixFormat *))GetProcAddress(hModule, "?CreateIfMatrix@@YAPEAUIfMatrix@@HHPEAM@Z");
			entry4 = (IfNnPrimitives * (*)(void))GetProcAddress(hModule, "?GetPrimitives@@YAPEAUIfNnPrimitives@@XZ");
#endif
		}

		DuplicateObject();
	};

	~MatrixClassFactory(void)
	{
		if (hModule)
			FreeLibrary(hModule);
	};

	void __stdcall DuplicateObject(void)
	{
		++instance;
	}

	void __stdcall ReleaseObject(void)
	{
		if (--instance == 0)
			delete this;
	}

	IfMatrix * CreateIfMatrix(void)
	{
		if (entry1)
			return((*entry1)());
		else
			return((IfMatrix * )NULL);
	};

	IfMatrix * CreateIfMatrix(int _rows, int _columns)
	{
		if (entry2)
			return((*entry2)(_rows, _columns));
		else
			return((IfMatrix *)NULL);
	};

	IfMatrix * CreateIfMatrix(int _rows, int _columns, matrixFormat * data)
	{
		if (entry3)
			return((*entry3)(_rows, _columns, data));
		else
			return((IfMatrix *)NULL);
	};

	IfNnPrimitives * GetPrimitives(void)
	{
		if (entry4)
			return((*entry4)());
		else
			return((IfNnPrimitives *)NULL);
	};
};

#endif // !CL_IF_MATRIX
