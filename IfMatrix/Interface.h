#pragma once

enum PREFER_DEVICE
{
	PREFER_CPU,
	PREFER_GPU
};

#define matrixFormat float

typedef char				pt08s;
typedef unsigned char		pt08u;
typedef short	   			pt16s;
typedef unsigned short		pt16u;
typedef int					pt32s;
typedef unsigned int		pt32u;
typedef __int64				pt64s;
typedef unsigned __int64	pt64u;
typedef float				pt32f;
typedef double				pt64f;

__interface PtInterface
{
	void	__stdcall DuplicateObject(void);
	void	__stdcall ReleaseObject(void);
};

