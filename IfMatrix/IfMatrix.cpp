// IfMatrix.cpp : Definiert die exportierten Funktionen f�r die DLL-Anwendung.
//

#define CL_IF_MATRIX

#include "stdafx.h"
#include <math.h>"

#include "IfMatrix.hpp"

struct FunctionData
{
	int startIndex;
	int lastIndex;
	int sStride;
	int dStride;
	int mStride;
	matrixFormat * sData;
	matrixFormat * dData;
	matrixFormat * mData;
};

void loppBody_tanh(volatile void * _fd)
{
	FunctionData * fd = (FunctionData *)_fd;
	matrixFormat * sData = &(fd->sData[fd->startIndex]);
	matrixFormat * dData = &(fd->dData[fd->startIndex]);

	for (int count = fd->startIndex; count < fd->lastIndex; ++count)
	{
		*dData++ = (matrixFormat)(tanh((double)*sData++));
	}
}

void loppBody_relu(volatile void * _fd)
{
	FunctionData * fd = (FunctionData *)_fd;
	matrixFormat * sData = &(fd->sData[fd->startIndex]);
	matrixFormat * dData = &(fd->dData[fd->startIndex]);

	for (int count = fd->startIndex; count < fd->lastIndex; ++count)
	{
		matrixFormat val = *sData++;
		*dData++ = (val > 0) ? val : 0;
	}
}

void loppBody_GradientTanh(volatile void * _fd)
{
	FunctionData * fd = (FunctionData *)_fd;
	matrixFormat * sData = &(fd->sData[fd->startIndex]);
	matrixFormat * dData = &(fd->dData[fd->startIndex]);
	matrixFormat * mData = &(fd->mData[fd->startIndex]);

	for (int count = fd->startIndex; count < fd->lastIndex; ++count)
	{
		matrixFormat a = *sData++;
		*dData++ = (matrixFormat)(1 - a * a) * *mData++;
	}
}

void loppBody_GradientRelu(volatile void * _fd)
{
	FunctionData * fd = (FunctionData *)_fd;
	matrixFormat * sData = &(fd->sData[fd->startIndex]);
	matrixFormat * dData = &(fd->dData[fd->startIndex]);
	matrixFormat * mData = &(fd->mData[fd->startIndex]);

	for (int count = fd->startIndex; count < fd->lastIndex; ++count)
	{
		*dData++ = (matrixFormat)((*sData++ > 0) ? *mData : 0);
		mData++;
	}
}

void loppBody_Add(volatile void * _fd)
{
	FunctionData * fd = (FunctionData *)_fd;
	matrixFormat * sData = &(fd->sData[fd->startIndex]);
	matrixFormat * dData = &(fd->dData[fd->startIndex]);

	for (int count = fd->startIndex; count < fd->lastIndex; ++count)
	{
		*dData++ += *sData++;
	}
}

void loppBody_Subtract(volatile void * _fd)
{
	FunctionData * fd = (FunctionData *)_fd;
	matrixFormat * sData = &(fd->sData[fd->startIndex]);
	matrixFormat * dData = &(fd->dData[fd->startIndex]);

	for (int count = fd->startIndex; count < fd->lastIndex; ++count)
	{
		*dData++ -= *sData++;
	}
}

void loppBody_Multiply(volatile void * _fd)
{
	FunctionData * fd = (FunctionData *)_fd;
	matrixFormat * sData = &(fd->sData[fd->startIndex]);
	matrixFormat * dData = &(fd->dData[fd->startIndex]);

	for (int count = fd->startIndex; count < fd->lastIndex; ++count)
	{
		*dData++ *= *sData++;
	}
}

class PtMatrix : public IfMatrix
{
	int columns;
	int rows;
	matrixFormat * matrixData;
	static int seed;
	int instance;

	WorkThreadPool * workThreadPool;
	int threadCount;

	FunctionData * fd;

	void LoopParallelized(int start, int last, matrixFormat * _sData, int _sStride, matrixFormat * _dData, int _dStride, matrixFormat * _mData, int _mStride, void(*function)(volatile void *))
	{
		int i;
		int range = (last - start) / threadCount;

		for (i = 0; i < threadCount; ++i)
		{
			fd[i].startIndex = start + i * range;
			fd[i].lastIndex = (i == (threadCount - 1)) ? last : fd[i].startIndex + range;
			fd[i].sStride = _sStride;
			fd[i].dStride = _dStride;
			fd[i].mStride = _mStride;
			fd[i].sData = _sData;
			fd[i].dData = _dData;
			fd[i].mData = _mData;

			workThreadPool->StartWorkThread(function, &fd[i]);
		}

		workThreadPool->WaitForAllThreadsFinished();
	}

	matrixFormat * Buffer(void)
	{
		return(matrixData);
	}

public:

	PtMatrix(void)
	{
		rows = 0;
		columns = 0;
		matrixData = NULL;
		instance = 0;

		workThreadPool = NULL;

		threadCount = 1;
		fd = NULL;
	}

	PtMatrix(int _rows, int _columns)
	{
		rows = _rows;
		columns = _columns;
		matrixData = (matrixFormat *)mkl_calloc(rows * columns, sizeof(matrixFormat), 64);
		instance = 0;

		for (int count = 0; count < (rows * columns); ++count)
		{
			matrixData[count] = 0.0;
		}

		workThreadPool = PtCreateWorkThreadPool();

		threadCount = 1;
		fd = NULL;

		if (workThreadPool != NULL)
		{
			threadCount = workThreadPool->MaxThreadCount();
			fd = new FunctionData[threadCount];
		}
	}

	PtMatrix(int _rows, int _columns, matrixFormat * data)
	{
		rows = _rows;
		columns = _columns;
		matrixData = (matrixFormat *)mkl_calloc(rows * columns, sizeof(matrixFormat), 64);
		memcpy_s(matrixData, rows * columns * sizeof(matrixFormat), data, rows * columns * sizeof(matrixFormat));
		instance = 0;

		workThreadPool = PtCreateWorkThreadPool();

		threadCount = 1;
		fd = NULL;

		if (workThreadPool != NULL)
		{
			threadCount = workThreadPool->MaxThreadCount();
			fd = new FunctionData[threadCount];
		}
	}

	~PtMatrix(void)
	{
		mkl_free(matrixData);

		if (workThreadPool != NULL)
			workThreadPool->ReleaseObject();

		if (fd != NULL)
			delete[] fd;
	}

	void __stdcall DuplicateObject(void)
	{
		++instance;
	}

	void __stdcall ReleaseObject(void)
	{
		if (--instance == 0)
			delete this;
	}

	void InitWithRandomValues(bool lastColumnIsBais)
	{
		VSLStreamStatePtr stream;
		matrixFormat range = 1 / sqrt(rows * columns);
		int stride = columns;
		int count = rows * (columns - (lastColumnIsBais ? 1 : 0));
		int * buff = new int[count];

		if (buff != NULL)
		{
			if (VSL_STATUS_OK == vslNewStream(&stream, VSL_BRNG_R250, seed))
			{
				viRngUniform(VSL_RNG_METHOD_UNIFORM_STD, stream, count, buff, 0, 16777216);

				count = 0;

				for (int row = 0; row < rows; ++row)
				{
					int column;

					for (column = 0; column < (columns - (lastColumnIsBais ? 1 : 0)); ++column)
					{
						matrixData[column + row * stride] = (matrixFormat)(buff[count++]) * 2 * range / 16777216.0 - range;
					}

					if (lastColumnIsBais)
						matrixData[column + row * stride] = 1;
				}

				seed = buff[count - 1];

				vslDeleteStream(&stream);
			}

			delete[] buff;
		}
	}

	void InitWithZero(void)
	{
		if (matrixData)
			memset(matrixData, 0, rows * columns * sizeof(matrixFormat));
	}

	void Dot(IfMatrix * _matrixA, IfMatrix * _matrixB, matrixFormat alpha)
	{
		PtMatrix * matrixA = (PtMatrix*)_matrixA;
		PtMatrix * matrixB = (PtMatrix*)_matrixB;
		int m = matrixA->rows;					// A.rows and C.rows
		int n = matrixB->columns;				// B.columns and C.columns
		int k = matrixB->rows;					// A.colums and B.rows

		if ((m == rows) && (n == columns) && (k == matrixA->columns))
			cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, m, n, k, alpha, matrixA->matrixData, matrixA->columns, matrixB->matrixData, matrixB->columns, 0, matrixData, columns);
	}

	void Dot_aT(IfMatrix * _matrixA, IfMatrix * _matrixB, matrixFormat alpha)
	{
		PtMatrix * matrixA = (PtMatrix*)_matrixA;
		PtMatrix * matrixB = (PtMatrix*)_matrixB;
		int m = min(matrixA->columns, rows);	// A.rows and C.rows, exception to exclude bais from backprop
		int n = matrixB->columns;				// B.columns and C.columns
		int k = matrixB->rows;					// A.colums and B.rows

		if ((m == rows) && (n == columns) && (k == matrixA->rows))
			cblas_sgemm(CblasRowMajor, CblasTrans, CblasNoTrans, m, n, k, alpha, matrixA->matrixData, matrixA->columns, matrixB->matrixData, matrixB->columns, 0, matrixData, columns);
	}

	void Dot_bT(IfMatrix * _matrixA, IfMatrix * _matrixB, matrixFormat alpha)
	{
		PtMatrix * matrixA = (PtMatrix*)_matrixA;
		PtMatrix * matrixB = (PtMatrix*)_matrixB;
		int m = matrixA->rows;					// A.rows and C.rows
		int n = matrixB->rows;					// B.columns and C.columns
		int k = matrixB->columns;				// A.colums and B.rows

		if ((m == rows) && (n == columns) && (k == matrixA->columns))
			cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasTrans, m, n, k, alpha, matrixA->matrixData, matrixA->columns, matrixB->matrixData, matrixB->columns, 0, matrixData, columns);
	}

	void ActivationTanh(IfMatrix * _matrixA)
	{
		PtMatrix * matrixA = (PtMatrix*)_matrixA;

		if ((matrixA->rows == rows) && (matrixA->columns == columns))
		{
			if (threadCount == 1)
			{
				matrixFormat * sData = matrixA->matrixData;
				matrixFormat * dData = matrixData;

				for (int count = 0; count < (rows * columns); ++count)
				{
					*dData++ = (matrixFormat)(tanh((double)*sData++));
				}
			}
			else
			{
				LoopParallelized(0, rows * columns, matrixA->matrixData, 1, matrixData, 1, NULL, 0, loppBody_tanh);
			}
		}
	}

	void ActivationRelu(IfMatrix * _matrixA)
	{
		PtMatrix * matrixA = (PtMatrix*)_matrixA;

		if ((matrixA->rows == rows) && (matrixA->columns == columns))
		{
			if (threadCount == 1)
			{
				matrixFormat * sData = matrixA->matrixData;
				matrixFormat * dData = matrixData;

				for (int count = 0; count < (rows * columns); ++count)
				{
					matrixFormat val = *sData++;
					*dData++ = (val > 0) ? val : 0;
				}
			}
			else
			{
				LoopParallelized(0, rows * columns, matrixA->matrixData, 1, matrixData, 1, NULL, 0, loppBody_relu);
			}
		}
	}

	void GradientTanh(IfMatrix * _matrixA, IfMatrix * _matrixB)
	{
		PtMatrix * matrixA = (PtMatrix*)_matrixA;
		PtMatrix * matrixB = (PtMatrix*)_matrixB;

		if ((matrixA->rows == rows) && (matrixA->columns == columns) && (matrixB->rows == rows) && (matrixB->columns == columns))
		{
			if (threadCount == 1)
			{
				matrixFormat * sData = matrixA->matrixData;
				matrixFormat * mData = matrixB->matrixData;
				matrixFormat * dData = matrixData;

				for (int count = 0; count < (rows * columns); ++count)
				{
					matrixFormat a = *sData++;
					*dData++ = (matrixFormat)(1 - a * a) * *mData++;
				}
			}
			else
			{
				LoopParallelized(0, rows * columns, matrixA->matrixData, 1, matrixData, 1, matrixB->matrixData, 1, loppBody_GradientTanh);
			}
		}
	}

	void GradientRelu(IfMatrix * _matrixA, IfMatrix * _matrixB)
	{
		PtMatrix * matrixA = (PtMatrix*)_matrixA;
		PtMatrix * matrixB = (PtMatrix*)_matrixB;

		if ((matrixA->rows == rows) && (matrixA->columns == columns) && (matrixB->rows == rows) && (matrixB->columns == columns))
		{
			if (threadCount == 1)
			{
				matrixFormat * sData = matrixA->matrixData;
				matrixFormat * mData = matrixB->matrixData;
				matrixFormat * dData = matrixData;

				for (int count = 0; count < (rows * columns); ++count)
				{
					*dData++ = (matrixFormat)((*sData++ > 0) ? *mData : 0);
					mData++;
				}
			}
			else
			{
				LoopParallelized(0, rows * columns, matrixA->matrixData, 1, matrixData, 1, matrixB->matrixData, 1, loppBody_GradientRelu);
			}
		}
	}

	void Add(IfMatrix * _matrixA)
	{
		PtMatrix * matrixA = (PtMatrix*)_matrixA;

		if ((matrixA->rows == rows) && (matrixA->columns == columns))
		{
			if (threadCount == 1)
			{
				matrixFormat * sData = matrixA->matrixData;
				matrixFormat * dData = matrixData;

				for (int count = 0; count < (rows * columns); ++count)
				{
					*dData++ += *sData++;
				}
			}
			else
			{
				LoopParallelized(0, rows * columns, matrixA->matrixData, 1, matrixData, 1, NULL, 1, loppBody_Add);
			}
		}
	}

	void Subtract(IfMatrix * _matrixA)
	{
		PtMatrix * matrixA = (PtMatrix*)_matrixA;

		if ((matrixA->rows == rows) && (matrixA->columns == columns))
		{
			if (threadCount == 1)
			{
				matrixFormat * sData = matrixA->matrixData;
				matrixFormat * dData = matrixData;

				for (int count = 0; count < (rows * columns); ++count)
				{
					*dData++ -= *sData++;
				}
			}
			else
			{
				LoopParallelized(0, rows * columns, matrixA->matrixData, 1, matrixData, 1, NULL, 1, loppBody_Subtract);
			}
		}
	}

	void Multiply(IfMatrix * _matrixA)
	{
		PtMatrix * matrixA = (PtMatrix*)_matrixA;

		if ((matrixA->rows == rows) && (matrixA->columns == columns))
		{
			if (threadCount == 1)
			{
				matrixFormat * sData = matrixA->matrixData;
				matrixFormat * dData = matrixData;

				for (int count = 0; count < (rows * columns); ++count)
				{
					*dData++ *= *sData++;
				}
			}
			else
			{
				LoopParallelized(0, rows * columns, matrixA->matrixData, 1, matrixData, 1, NULL, 1, loppBody_Multiply);
			}
		}
	}

	void Copy(IfMatrix * _matrixA)
	{
		PtMatrix * matrixA = (PtMatrix*)_matrixA;

		int cellCount = min(rows * columns, matrixA->rows * matrixA->columns);
		int count = 0;

		matrixFormat * sData = matrixA->matrixData;
		matrixFormat * dData = matrixData;

		for (; count < cellCount; ++count)
		{
			*dData++ = *sData++;
		}

		// for bais 
		for (; count < rows * columns; ++count)
		{
			*dData++ = 1;
		}
	}

	void Copy(int startIndex, IfMatrix * _matrixA)
	{
		PtMatrix * matrixA = (PtMatrix*)_matrixA;

		int cellCount = matrixA->rows * matrixA->columns;
		int count = 0;

		if (startIndex + cellCount <= (rows * columns))
		{
			matrixFormat * sData = matrixA->matrixData;
			matrixFormat * dData = matrixData + startIndex;

			for (; count < cellCount; ++count)
			{
				*dData++ = *sData++;
			}
		}
	}

	void Copy(IfMatrix * _matrixA, int sourceIndex)
	{
		PtMatrix * matrixA = (PtMatrix*)_matrixA;

		int cellCount = rows * columns;
		int count = 0;

		if (sourceIndex + cellCount <= (matrixA->rows *  matrixA->columns))
		{
			matrixFormat * sData = matrixA->matrixData + sourceIndex;
			matrixFormat * dData = matrixData;

			for (; count < cellCount; ++count)
			{
				*dData++ = *sData++;
			}
		}
	}

	void Rotate180(IfMatrix * _matrixA)
	{
		PtMatrix * matrixA = (PtMatrix*)_matrixA;

		int cellCount = min(rows * columns, matrixA->rows * matrixA->columns);
		int count = 0;

		matrixFormat * sData = matrixA->matrixData;
		matrixFormat * dData = matrixData + rows * columns - 1;

		for (; count < cellCount; ++count)
		{
			*dData-- = *sData++;
		}
	}

	matrixFormat SquareSumm(void)
	{
		matrixFormat a, summ = 0;

		matrixFormat * sData = matrixData;

		for (int count = 0; count < (rows * columns); ++count)
		{
			a = *sData++;
			summ += a * a;
		}

		return(summ);
	}

	void Convolution(IfMatrix * _weights, IfMatrix * _inputMatrix, ConvolutionInfo * convolutionInfo)
	{
		PtMatrix * weights = (PtMatrix*)_weights;
		PtMatrix * inputMatrix = (PtMatrix*)_inputMatrix;
		IppiSize sizeS1, sizeS2;

		sizeS1.width = convolutionInfo->sizeS1_width;
		sizeS1.height = convolutionInfo->sizeS1_height;
		sizeS2.width = convolutionInfo->sizeS2_width;
		sizeS2.height = convolutionInfo->sizeS2_height;

		ippiConv_32f_C1R(inputMatrix->matrixData, sizeS1.width * sizeof(matrixFormat), sizeS1,
			weights->matrixData, sizeS2.width * sizeof(matrixFormat), sizeS2,
			matrixData + 1 + columns, columns * sizeof(matrixFormat), IppiROIShape::ippiROIValid, convolutionInfo->convolutionTempBuffer);
	}

	void ConvolutionErrorSumm(IfMatrix * errorMatrix, float lRate)
	{
		IppiSize roiSize;
		matrixFormat * integralImage;
		matrixFormat * deltaBuffer;

		if ((rows == CONVOLUTION_ROWS) && (columns == CONVOLUTION_COLUMNS))
		{
			deltaBuffer = matrixData;

			int inputColumns = errorMatrix->Columns();
			int inputRows = errorMatrix->Rows();
			roiSize.width = inputColumns;
			roiSize.height = inputRows;

			integralImage = (matrixFormat *)mkl_calloc((inputColumns + 1) * (inputRows + 1), sizeof(matrixFormat), 64);

			if (integralImage)
			{
				IppStatus ippStatus;

				matrixFormat * imageBuffer = new matrixFormat[inputRows * inputColumns];

				errorMatrix->GetBuffer(imageBuffer, inputRows * inputColumns);

				ippStatus = ippiIntegral_32f_C1R(imageBuffer, sizeof(matrixFormat) * roiSize.width, integralImage, sizeof(matrixFormat) * (roiSize.width + 1), roiSize);

				delete imageBuffer;

				if (ippStatus == ippStsNoErr)
				{
					int stride = roiSize.width + 1;
					// step one pixel inside because these are calculated with zero padding
					deltaBuffer[0] = (integralImage[1 + 1 * stride] - integralImage[inputColumns - 3 + 1 * stride] - integralImage[1 + (inputRows - 3) * stride] + integralImage[inputColumns - 3 + (inputRows - 3) * stride]) * lRate;
					deltaBuffer[1] = (integralImage[2 + 1 * stride] - integralImage[inputColumns - 2 + 1 * stride] - integralImage[2 + (inputRows - 3) * stride] + integralImage[inputColumns - 2 + (inputRows - 3) * stride]) * lRate;
					deltaBuffer[2] = (integralImage[3 + 1 * stride] - integralImage[inputColumns - 1 + 1 * stride] - integralImage[3 + (inputRows - 3) * stride] + integralImage[inputColumns - 1 + (inputRows - 3) * stride]) * lRate;
					deltaBuffer[3] = (integralImage[1 + 2 * stride] - integralImage[inputColumns - 3 + 2 * stride] - integralImage[1 + (inputRows - 2) * stride] + integralImage[inputColumns - 3 + (inputRows - 2) * stride]) * lRate;
					deltaBuffer[4] = (integralImage[2 + 2 * stride] - integralImage[inputColumns - 2 + 2 * stride] - integralImage[2 + (inputRows - 2) * stride] + integralImage[inputColumns - 2 + (inputRows - 2) * stride]) * lRate;
					deltaBuffer[5] = (integralImage[3 + 2 * stride] - integralImage[inputColumns - 1 + 2 * stride] - integralImage[3 + (inputRows - 2) * stride] + integralImage[inputColumns - 1 + (inputRows - 2) * stride]) * lRate;
					deltaBuffer[6] = (integralImage[1 + 3 * stride] - integralImage[inputColumns - 3 + 3 * stride] - integralImage[1 + (inputRows - 1) * stride] + integralImage[inputColumns - 3 + (inputRows - 1) * stride]) * lRate;
					deltaBuffer[7] = (integralImage[2 + 3 * stride] - integralImage[inputColumns - 2 + 3 * stride] - integralImage[2 + (inputRows - 1) * stride] + integralImage[inputColumns - 2 + (inputRows - 1) * stride]) * lRate;
					deltaBuffer[8] = (integralImage[3 + 3 * stride] - integralImage[inputColumns - 1 + 3 * stride] - integralImage[3 + (inputRows - 1) * stride] + integralImage[inputColumns - 1 + (inputRows - 1) * stride]) * lRate;
				}

				mkl_free(integralImage);
			}
		}
	}

	void MaxPooling(IfMatrix * _input)
	{
		PtMatrix * input = (PtMatrix*)_input;

		if (matrixData)
		{
			for (int i = 0; i < rows * columns; ++i)
				matrixData[i] = -1;
		}
		//memset(matrixData, 0, rows * columns * sizeof(matrixFormat));

		if ((input->Columns() / 2 <= columns) && (input->Rows() / 2 <= rows))
		{
			int inputRows = input->Rows();
			int inputColumns = input->Columns();
			matrixFormat * sourceData = input->Buffer();
			matrixFormat * destData = matrixData;

			for (int row = 0; row < inputRows; ++row)
			{
				int ouputRow = row / 2;
				matrixFormat * sData = sourceData + row * inputColumns;

				for (int column = 0; column < inputColumns; ++column)
				{
					int outputColumn = column / 2;
					matrixFormat * dData = destData + outputColumn + ouputRow * columns;

					if (*sData > *dData)
						*dData = *sData;

					++sData;
				}
			}
		}
	}

	void MaxExpand(IfMatrix * _input, IfMatrix * _referenceData)
	{
		PtMatrix * input = (PtMatrix*)_input;
		PtMatrix * referenceData = (PtMatrix*)_referenceData;

		if ((input->Columns() * 2 >= columns) && (input->Rows() * 2 >= rows) && (referenceData->Columns() * 2 == columns) && (referenceData->Rows() * 2 == rows))
		{
			int inputStride = input->Columns();
			matrixFormat * sourceData = input->Buffer();
			matrixFormat * refData = referenceData->Buffer();
			matrixFormat * destData = matrixData;												// initialized with out prev. layer 

			for (int row = 0; row < rows; ++row)
			{
				int inputRow = row / 2;
				matrixFormat * dData = destData + row * columns;

				for (int column = 0; column < columns; ++column)
				{
					matrixFormat val = 0;
					int inputColumn = column / 2;
					matrixFormat * sData = sourceData + inputColumn + inputRow * inputStride;	// backprop error
					matrixFormat * rData = refData + inputColumn + inputRow * inputStride;		// output of pooling

					if (*rData == *dData)														// if output == output prev. layer then backprop error
						val = *sData;

					*dData++ = val;
				}
			}
		}
	}

	void SetBuffer(matrixFormat * data, int dataSize)
	{
		if ((matrixData != NULL) && (dataSize <= (rows * columns)))
			memcpy_s(matrixData, rows * columns * sizeof(matrixFormat), data, dataSize * sizeof(matrixFormat));
	}

	void GetBuffer(matrixFormat * data, int dataSize)
	{
		if ((matrixData != NULL) && (dataSize >= (rows * columns)))
			memcpy_s(data, dataSize * sizeof(matrixFormat), matrixData, rows * columns * sizeof(matrixFormat));
	}

	int Rows(void)
	{
		return(rows);
	}

	int Columns(void)
	{
		return(columns);
	}
};

int PtMatrix::seed = 3456;

__declspec(dllexport) IfMatrix * CreateIfMatrix(void)
{
	PtMatrix * matrix = new PtMatrix();

	if (matrix != NULL)
		matrix->DuplicateObject();

	return((IfMatrix *)matrix );
}

__declspec(dllexport) IfMatrix * CreateIfMatrix(int _rows, int _columns)
{
	PtMatrix * matrix = new PtMatrix(_rows, _columns);

	if (matrix != NULL)
		matrix->DuplicateObject();

	return((IfMatrix *)matrix);
}

__declspec(dllexport) IfMatrix * CreateIfMatrix(int _rows, int _columns, matrixFormat * data)
{
	PtMatrix * matrix = new PtMatrix(_rows, _columns, data);

	if (matrix != NULL)
		matrix->DuplicateObject();

	return((IfMatrix *)matrix);
}

class ClNnPrimitives : IfNnPrimitives
{
public :
	ClNnPrimitives(void) {};
	~ClNnPrimitives(void) {};

	void PredictDense(PredictParams &predictParams)
	{
		predictParams.inputBuffer->Copy(predictParams.inputSignal);							// add bais if used
		predictParams.signals->Dot(predictParams.weights, predictParams.inputBuffer, 1.0);
		predictParams.outputSignal->ActivationTanh(predictParams.signals);
	}

	void BackPropagationDense(BackPropagationParams &backPropagationParams)
	{
		// errorPreviousLayer has one row less than weights columns ( last column is added for bais )
		// it works because k is set by matrix B and lma is set from matrix A.column in gemm
		if (backPropagationParams.errorPreviousLayer != NULL)
			backPropagationParams.errorPreviousLayer->Dot_aT(backPropagationParams.weights, backPropagationParams.errorCurrentLayer, 1.0);

		backPropagationParams.errorMatrix->GradientTanh(backPropagationParams.outputCurrentLayer, backPropagationParams.errorCurrentLayer);

		backPropagationParams.delta->Dot_bT(backPropagationParams.errorMatrix, backPropagationParams.outputPreviousLayer, backPropagationParams.lRate);
		backPropagationParams.weights->Add(backPropagationParams.delta);
	}

	void PredictConvolution(PredictParams &predictParams)
	{
		predictParams.signals->Convolution(predictParams.weights, predictParams.inputSignal, predictParams.convolutionInfo);

		switch (predictParams.convolutionInfo->activation)
		{
			case ActivationTypeTanh:
				predictParams.outputSignal->ActivationTanh(predictParams.signals);
				break;

			case ActivationTypeRelu:
				predictParams.outputSignal->ActivationRelu(predictParams.signals);
				break;
		}
	}

	void BackPropagationConvolution(BackPropagationParams &backPropagationParams)
	{
		if (backPropagationParams.errorPreviousLayer != NULL)
		{
			backPropagationParams.delta->Rotate180(backPropagationParams.weights);
			backPropagationParams.errorPreviousLayer->Convolution(backPropagationParams.delta, backPropagationParams.errorCurrentLayer, backPropagationParams.convolutionInfo);
		}

		if (backPropagationParams.lRate != 0)
		{
			switch (backPropagationParams.convolutionInfo->activation)
			{
				case ActivationTypeTanh:
					// ( target - output ) * ( 1 - output * output ),  == error * derivate of tanh( x ), next multiplay with (x) output previous layer
					backPropagationParams.errorMatrix->GradientTanh(backPropagationParams.outputCurrentLayer, backPropagationParams.errorCurrentLayer);
					break;

				case ActivationTypeRelu:
					backPropagationParams.errorMatrix->GradientRelu(backPropagationParams.outputCurrentLayer, backPropagationParams.errorCurrentLayer);
					break;
			}

			backPropagationParams.errorMatrix->Multiply(backPropagationParams.outputPreviousLayer);

			backPropagationParams.delta->ConvolutionErrorSumm(backPropagationParams.errorMatrix, backPropagationParams.lRate);

			backPropagationParams.weights->Add(backPropagationParams.delta);
		}
	}

	void PredictPooling(PredictParams &predictParams)
	{
		predictParams.outputSignal->MaxPooling(predictParams.inputSignal);
	}

	void BackPropagationPooling(BackPropagationParams &backPropagationParams)
	{
		// mit input initialisieren
		backPropagationParams.errorPreviousLayer->Copy(backPropagationParams.outputPreviousLayer);
		// mit fehler oder 0 f�llem
		backPropagationParams.errorPreviousLayer->MaxExpand(backPropagationParams.errorCurrentLayer, backPropagationParams.outputCurrentLayer);
	}
};

ClNnPrimitives clNnPrimitives;

__declspec(dllexport) IfNnPrimitives * GetPrimitives(void)
{
	return((IfNnPrimitives *)(&clNnPrimitives));
}

