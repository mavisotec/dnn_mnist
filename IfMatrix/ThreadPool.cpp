// ThreadPool.cpp : Definiert den Einsprungpunkt f�r die DLL-Anwendung.
//

/* noafx */

#include "stdafx.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "mkl.h"
#include "Interface.h"
#include "cl_threadpool.hpp"

#define PT_THREAD_WAIT	10000

LPTHREAD_START_ROUTINE evFunction( WorkThreadInfo * wtInfo )
{
	pt32s inUse = 0 ;
	DWORD rc;

	while ( inUse != -1 )
	{
		rc = WaitForSingleObject( wtInfo->startEvent, INFINITE ) ;
				
		ResetEvent( wtInfo->startEvent ) ;

		inUse = wtInfo->inUse ;

		if ( inUse == 1 )
		{
			if (wtInfo->userFunction != NULL)
				( * wtInfo->userFunction )( wtInfo->userData ) ;

			SetEvent( wtInfo->rdyEvent ) ;
		}
		else
		{
			SleepEx( 0, false ) ;
		}
	}

	return( 0 ) ;
}

WORK_THREAD_POOL::WORK_THREAD_POOL( void ) 
{
	pt32s i ;

	instances = 0 ;

	threadCount = mkl_get_max_threads();

	infoTab = new WorkThreadInfo[threadCount] ;
	classMutex = CreateMutex( NULL, 0, NULL ) ;

	if (classMutex)
		WaitForSingleObject( classMutex, PT_THREAD_WAIT ) ;

	if ( infoTab )
	{
		for ( i = 0 ; i < threadCount ; ++i )
		{
			infoTab[i].inUse = 0 ;
			infoTab[i].userFunction = NULL;
			infoTab[i].startEvent = CreateEvent( NULL, TRUE, FALSE, NULL ) ;
			infoTab[i].rdyEvent = CreateEvent( NULL, TRUE, FALSE, NULL ) ;
			infoTab[i].classMutex = classMutex ;
			infoTab[i].workThread = CreateThread( NULL, 0, ( LPTHREAD_START_ROUTINE )evFunction, &( infoTab[i] ), 0, &( infoTab[i].threadID ) ) ;
		}
	}

	if (classMutex)
		ReleaseMutex(classMutex);
}

WORK_THREAD_POOL::~WORK_THREAD_POOL( void ) 
{
	pt32s i ;

	if (classMutex)
		WaitForSingleObject(classMutex, PT_THREAD_WAIT);

	if ( infoTab )
	{
		for ( i = 0 ; i < threadCount ; ++i )
		{	
			infoTab[i].inUse = -1 ;
			SetEvent( infoTab[i].startEvent ) ;
		}
	}

	if (classMutex)
		ReleaseMutex(classMutex);
	
	if ( infoTab )
	{
		for ( i = 0 ; i < threadCount ; ++i )
		{
			WaitForSingleObject( infoTab[i].workThread, 200 ) ;
			CloseHandle( infoTab[i].workThread ) ;
			CloseHandle( infoTab[i].startEvent ) ;
			CloseHandle( infoTab[i].rdyEvent ) ;
		}

		delete [] infoTab ;
	}

	infoTab = NULL ;

	if (classMutex)
		CloseHandle(classMutex);
}

pt32u WORK_THREAD_POOL::StartWorkThread( void ( * userFunction)( volatile void * ), volatile void * userData ) 
{
	pt32s i, index = -1 ;

	if ( infoTab )
	{
		WaitForSingleObject( classMutex, INFINITE ) ;

		for ( i = 0 ; i < threadCount ; ++i )
		{
			if ( infoTab[i].inUse == 0 )
			{
				infoTab[i].userFunction = userFunction ;
				infoTab[i].userData = userData ;
				infoTab[i].inUse = 1 ;
				index = i ;
				SetEvent( infoTab[i].startEvent ) ;
				i = threadCount ;
			}
		}

		ReleaseMutex( classMutex ) ;
	}

	return( ( index != -1 ) ? 1 : 0 ) ;
}

void WORK_THREAD_POOL::WaitForAllThreadsFinished( void ) 
{
	pt32s i, index = 0 ;
	HANDLE * tabOfWaitObjects = new HANDLE[threadCount] ;

	if ( infoTab )
	{
		WaitForSingleObject( classMutex, INFINITE ) ;

		for ( i = 0 ; i < threadCount ; ++i )
		{
			if ( infoTab[i].inUse > 0 )
				tabOfWaitObjects[index++] = infoTab[i].rdyEvent ;
		}

		ReleaseMutex( classMutex ) ;

		if ( index > 0 )
		{
			pt32s mbResult = IDRETRY ; 

			while ( mbResult == IDRETRY )
			{
				if ( WAIT_TIMEOUT == WaitForMultipleObjects( index, tabOfWaitObjects, TRUE, PT_THREAD_WAIT ) )
					mbResult = MessageBox( NULL, "WAIT_TIMEOUT for rdyEvents", "PtThreadPool", MB_ABORTRETRYIGNORE ) ; 
				else
					mbResult = IDOK ;

				if ( mbResult == IDABORT )
					abort() ;
			}
		}

		WaitForSingleObject( classMutex, INFINITE ) ;
			
		for ( i = 0 ; i < threadCount ; ++i )
		{
			if ( infoTab[i].inUse > 0 )
			{
				infoTab[i].inUse = 0 ;
				infoTab[i].userFunction = NULL;
				ResetEvent( infoTab[i].rdyEvent ) ;
			}
		}

		ReleaseMutex( classMutex ) ;
	}

	delete [] tabOfWaitObjects ;
}

pt32s WORK_THREAD_POOL::MaxThreadCount(void)
{
	return(threadCount);
}

// PtInterface Member

WORK_THREAD_POOL * threadpool_p = NULL;

void WINAPI WORK_THREAD_POOL::DuplicateObject( void )
{
	instances++ ;
}

void WINAPI WORK_THREAD_POOL::ReleaseObject( void )
{
	if ( --instances == 0 )
    {
    	delete this ;
		threadpool_p = NULL;
    }
}

WorkThreadPool * WINAPI PtCreateWorkThreadPool( void )
{
	if ( threadpool_p == NULL )
    	threadpool_p = new WORK_THREAD_POOL() ;

    if ( threadpool_p )
    	threadpool_p->DuplicateObject() ;

    return( ( WorkThreadPool * )threadpool_p ) ;
}
