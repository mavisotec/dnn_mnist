#if !defined (__IF_THREADPOOL_H)

#define __IF_THREADPOOL_H

#ifndef _M_X64 
typedef int						ptAddrOff;
#define StringLen( __param )	(pt32u)lstrlen(__param)
#else
typedef __int64					ptAddrOff;
#define StringLen( __param )	(DWORD)lstrlen(__param)
#endif

__interface WorkThreadPool : PtInterface
{
	pt32u __stdcall StartWorkThread( void ( * userFunction)( volatile void * ), volatile void * userData ) ;
	void __stdcall	WaitForAllThreadsFinished( void ) ;

	pt32s __stdcall MaxThreadCount(void);
} ;

WorkThreadPool * __stdcall PtCreateWorkThreadPool( void ) ;

#endif
