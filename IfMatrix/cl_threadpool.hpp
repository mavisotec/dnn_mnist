#if !defined(__CL_THREADPOOL_H)

#define __CL_THREADPOOL_H

#include "if_threadpool.hpp"


struct WorkThreadInfo
{
	HANDLE				workThread ;
	HANDLE				startEvent ;
	HANDLE				rdyEvent ;
	HANDLE				classMutex ;
	unsigned long		threadID ;
	volatile pt32s		inUse ;
	void				( * userFunction )( volatile void * ) ;
	volatile void		* userData ;
} ;		

class WORK_THREAD_POOL : public WorkThreadPool
{
	WorkThreadInfo		* infoTab ;
	pt32s				threadCount ;
	HANDLE				classMutex ;

	pt32u				instances ;

public :

	WORK_THREAD_POOL( void ) ;
	~WORK_THREAD_POOL( void ) ;

    // PtInterface Member

    void  WINAPI DuplicateObject( void ) ;
	void  WINAPI ReleaseObject( void ) ;

    // WorkThreadPool Member

	pt32u WINAPI StartWorkThread( void ( * userFunction)( volatile void * ), volatile void * userData ) ;
	void  WINAPI WaitForAllThreadsFinished( void ) ;

	pt32s WINAPI MaxThreadCount(void);
} ;

#endif
