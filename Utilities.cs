﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Runtime.InteropServices;

namespace Utilities
{
    public class DataWithLabel : IDisposable
    {
        Int32 label;
        Int32 width;
        Int32 height;
        Int32 format;
        IntPtr data;
        private bool disposed = false;

        public DataWithLabel( Int32 _label, Int32 _width, Int32 _height, Int32 _format)
        {
            label = _label;
            width = _width;
            height = _height;
            format = _format;

            data = Marshal.AllocHGlobal(width * height * format / 8);
        }

        // Implement IDisposable.
        // Do not make this method virtual.
        // A derived class should not be able to override this method.
        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.

        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                // If disposing is false,
                // only the following code is executed.

                Marshal.FreeHGlobal(data);

                // Note disposing has been done.
                disposed = true;
            }
        }

        // Use C# destructor syntax for finalization code.
        // This destructor will run only if the Dispose method
        // does not get called.
        // It gives your base class the opportunity to finalize.
        // Do not provide destructors in types derived from this class.
        ~DataWithLabel()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal in terms of
            // readability and maintainability.
            Dispose(false);
        }

        public Int32 Label
        {
            get
            {
                return (label);
            }
        }

        public Int32 Width
        {
            get
            {
                return (width);
            }
        }

        public Int32 Height
        {
            get
            {
                return (height);
            }
        }

        public Int32 Format
        {
            get
            {
                return (format);
            }
        }

        public IntPtr Data
        {
            get
            {
                return (data);
            }
        }
    }

    public class Helpers
    {
        public List<DataWithLabel> LoadDataWithLabelsFromCsv(String dataFileName)
        {
            StreamReader file;
            List<DataWithLabel> listWithDataLabeled = new List<DataWithLabel>();

            try
            {
                String line;
                String[] itemList;
                Char[] splitChars = { ',' };
                Int32 x, y;
                Int32 sizeX = 28, sizeY = 28;

                file = new StreamReader(dataFileName, Encoding.ASCII);

                line = file.ReadLine();

                while (line.Length > 0)
                {
                    Int32 val, count = 1;

                    itemList = line.Split(splitChars);

                    try
                    {
                        val = Int32.Parse(itemList[0]);
                    }
                    catch (Exception)
                    {
                        val = 0;
                    }

                    DataWithLabel data = new DataWithLabel(val, sizeX, sizeY, 32);

                    for (y = 0; y < sizeY; ++y)
                    {
                        for (x = 0; x < sizeX; ++x)
                        {
                            val = Int32.Parse(itemList[count++]);
                            val = ( val << 16 ) + ( val << 8 ) + val;
                            Marshal.WriteInt32(data.Data, (x + y * sizeX) * data.Format / 8, val);
                        }
                    }

                    listWithDataLabeled.Add(data);

                    line = file.ReadLine();
                }

                file.Close();

            }
            catch (Exception)
            {
            }

            return (listWithDataLabeled);
        }
    }
}
