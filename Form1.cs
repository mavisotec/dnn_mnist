﻿#define SHOW_HIDDEN 

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Threading;
using System.Globalization;
using Utilities;


namespace Workplace
{
    public partial class Form1 : Form
    {
        Helpers utils = new Helpers();
        List<DataWithLabel> trainListWithLabelsAndData = null;
        List<DataWithLabel> testListWithLabelsAndData = null;
        Int32 index = 0;
        Thread trainThread = null;
        Thread testThread = null;
        int maxIndex;
        double lRate = 0.0005;
        Random randomGenerator = new Random( 456 );
        String dataDir = "C:\\work_v\\NeuralNetwork\\Workplace\\";

        [DllImport("UseDnnLayer.dll")]
        private static extern void createDNN(bool useGPU);

        [DllImport("UseDnnLayer.dll")]
        private static extern void destroyDNN();

        [DllImport("UseDnnLayer.dll")]
        private static extern double trainDNN(IntPtr data, int size, int label, double lRate);

        [DllImport("UseDnnLayer.dll")]
        private static extern int querryDNN(IntPtr data, int size, ref double score);

        [DllImport("UseDnnLayer.dll")]
        private static extern void saveDNN(IntPtr data);

        [DllImport("UseDnnLayer.dll")]
        private static extern void loadDNN(IntPtr data);


        [DllImport("UseDnnLayer.dll")]
        private static extern void timerStart();

        [DllImport("UseDnnLayer.dll")]
        private static extern double elapsedTime_ms();

        public Form1()
        {
            InitializeComponent();

            button1.Enabled = false;
            button2.Enabled = false;
            button3.Enabled = false;
            button4.Enabled = false;
            checkBox1.Enabled = false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            timer1.Enabled = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;

            trainListWithLabelsAndData = utils.LoadDataWithLabelsFromCsv(dataDir + "MNIST\\mnist_train.csv");
            testListWithLabelsAndData = utils.LoadDataWithLabelsFromCsv(dataDir + "MNIST\\mnist_test.csv");

            maxIndex = trainListWithLabelsAndData.Count;

            DataWithLabel dataWithLabel = trainListWithLabelsAndData[index];
            pictureBox1.Image = new Bitmap(dataWithLabel.Width, dataWithLabel.Height, dataWithLabel.Width * dataWithLabel.Format / 8, System.Drawing.Imaging.PixelFormat.Format32bppRgb, dataWithLabel.Data);

            textBox1.Text = dataWithLabel.Label.ToString();

            createDNN(false);

            button1.Enabled = true;
            button2.Enabled = true;
            button3.Enabled = true;
            button4.Enabled = true;
            checkBox1.Enabled = true;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if ((trainThread != null) && (trainThread.IsAlive))
                trainThread.Abort();

            if ((testThread != null) && (testThread.IsAlive))
                testThread.Abort();

            destroyDNN();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double score = 0;

            --index;

            if (index < 0)
                index = testListWithLabelsAndData.Count - 1;

            DataWithLabel dataWithLabel = testListWithLabelsAndData[index];
            pictureBox1.Image = new Bitmap(dataWithLabel.Width, dataWithLabel.Height, dataWithLabel.Width * dataWithLabel.Format / 8, System.Drawing.Imaging.PixelFormat.Format32bppRgb, dataWithLabel.Data);

            textBox1.Text = dataWithLabel.Label.ToString();

            timerStart();

            int label = querryDNN(dataWithLabel.Data, dataWithLabel.Width * dataWithLabel.Height, ref score);

            textBox3.Text = label.ToString();
            textBox6.Text = score.ToString();
            label6.Text = String.Format("{0:f3}", elapsedTime_ms()) + " ms";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            double score = 0;

            ++index;

            if (index > (testListWithLabelsAndData.Count - 1))
                index = 0;

            DataWithLabel dataWithLabel = testListWithLabelsAndData[index];
            pictureBox1.Image = new Bitmap(dataWithLabel.Width, dataWithLabel.Height, dataWithLabel.Width * dataWithLabel.Format / 8, System.Drawing.Imaging.PixelFormat.Format32bppRgb, dataWithLabel.Data);

            textBox1.Text = dataWithLabel.Label.ToString();

            timerStart();

            int label = querryDNN(dataWithLabel.Data, dataWithLabel.Width * dataWithLabel.Height, ref score);

            textBox3.Text = label.ToString();
            textBox6.Text = score.ToString();
            label6.Text = String.Format("{0:f3}", elapsedTime_ms()) + " ms";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            NumberFormatInfo nfi = new NumberFormatInfo();

            nfi.NumberDecimalSeparator = ".";

            label6.Text = "";

            try
            {
                lRate = Double.Parse(textBox7.Text, nfi);
                int maxEpoche = Int32.Parse(textBox8.Text);

                checkBox1.Enabled = false;

                progressBar1.Maximum = maxEpoche * maxIndex;

                if ((trainThread == null) || (trainThread.IsAlive == false))
                {
                    trainThread = new Thread(new ParameterizedThreadStart(this.TrainThread));
                    trainThread.Start(null);
                    button3.Enabled = false;
                }
            }
            catch(Exception)
            {
                label6.Text = "invalid input";
            }
        }

        private void TrainThread(object param)
        {
            int maxEpoche = 10;
            int epoche;
            int index;
            int count = 0;
            double costs = 0;
            DisplayProgressDelegate displayProgress = new DisplayProgressDelegate(DisplayProgress);
            ProgressInfo pInfo = new ProgressInfo();

            maxEpoche = Int32.Parse(textBox8.Text);

            pInfo.endFlag = false;

            timerStart();
                
            for (epoche = 0; epoche < maxEpoche; ++epoche)
            {
                for (index = 0; index < maxIndex; ++index)
                {
                    DataWithLabel dataWithLabel = trainListWithLabelsAndData[index];

                    costs += trainDNN(dataWithLabel.Data, dataWithLabel.Width * dataWithLabel.Height, dataWithLabel.Label, lRate);

                    if ((count % 1000) == 0)
                    {
                        pInfo.msg = 0;
                        pInfo.cost = ( costs / 1000 ).ToString();
                        pInfo.progress = count;
                        this.Invoke(displayProgress, pInfo);
                        costs = 0;
                    }
                    ++count;
                    /*
                    if (count == 100)
                        index = maxIndex;
                    */
                }

                maxEpoche = Int32.Parse(textBox8.Text);

                if ((epoche + 1) < maxEpoche)
                {
                    // mischen
                    List<DataWithLabel> nextTrainListWithLabelsAndData = new List<DataWithLabel>();
                    Int32 maxInList = trainListWithLabelsAndData.Count;

                    while (maxInList > 0)
                    {
                        Int32 next = randomGenerator.Next();
                        Double k = (double)next / (double)Int32.MaxValue;
                        DataWithLabel data;

                        k *= maxInList;

                        Int32 randomIndex = (Int32)k;

                        if (randomIndex == maxInList)
                            --randomIndex;

                        data = trainListWithLabelsAndData[randomIndex];
                        trainListWithLabelsAndData.Remove(data);

                        nextTrainListWithLabelsAndData.Add(data);

                        maxInList = trainListWithLabelsAndData.Count;
                    }

                    trainListWithLabelsAndData = nextTrainListWithLabelsAndData;
                }
                
            }

            pInfo.calcTime = elapsedTime_ms();
            pInfo.endFlag = true;
            this.Invoke(displayProgress, pInfo);
        }

        private void DisplayProgress(ProgressInfo pInfo)
        {
            if (pInfo.progress >= progressBar1.Maximum)
                pInfo.progress = progressBar1.Maximum;

            switch (pInfo.msg)
            {
                case 0:
                    textBox2.Text = pInfo.cost;
                    progressBar1.Value = pInfo.progress;
                    break;

                case 1:
                    textBox4.Text = pInfo.cost;
                    progressBar1.Value = pInfo.progress;
                    break;

                case 2:
                    textBox5.Text = pInfo.cost;
                    progressBar1.Value = pInfo.progress;
                    break;
            }

            if ( pInfo.endFlag)
            {
                progressBar1.Value = 0;
                if (pInfo.msg == 0)
                    button3.Enabled = true;
                else
                    button4.Enabled = true;

                label6.Text = String.Format("{0:f3}", pInfo.calcTime) + " ms";

                checkBox1.Enabled = true;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            checkBox1.Enabled = false;

            progressBar1.Maximum = maxIndex + testListWithLabelsAndData.Count;

            if ((testThread == null) || (testThread.IsAlive == false))
            {
                testThread = new Thread(new ParameterizedThreadStart(this.TestThread));
                testThread.Start(null);
                button4.Enabled = false;
            }
        }

        private void TestThread(object param)
        {
            double score = 0;
            int index;
            int countAll = 0, countOk = 0;
            DisplayProgressDelegate displayProgress = new DisplayProgressDelegate(DisplayProgress);
            ProgressInfo pInfo = new ProgressInfo();

            pInfo.endFlag = false;

            timerStart();

            for (index = 0; index < maxIndex; ++index)
            {
                DataWithLabel dataWithLabel = trainListWithLabelsAndData[index];

                if (dataWithLabel.Label == querryDNN(dataWithLabel.Data, dataWithLabel.Width * dataWithLabel.Height, ref score))
                    ++countOk;

                ++countAll;

                if ((countAll % 1000) == 0)
                {
                    pInfo.msg = 1;
                    pInfo.cost = (countAll != 0) ? (countOk * 100.0 / countAll).ToString() : "";
                    pInfo.progress = countAll;
                    this.Invoke(displayProgress, pInfo);
                }
            }

            pInfo.msg = 1;
            pInfo.cost = ( countAll != 0 ) ? (countOk * 100.0 / countAll).ToString() : "";
            pInfo.progress = countAll;
            this.Invoke(displayProgress, pInfo);

            countAll = 0;
            countOk = 0;

            for (index = 0; index < testListWithLabelsAndData.Count; ++index)
            {
                DataWithLabel dataWithLabel = testListWithLabelsAndData[index];

                if (dataWithLabel.Label == querryDNN(dataWithLabel.Data, dataWithLabel.Width * dataWithLabel.Height, ref score))
                    ++countOk;

                ++countAll;

                if ((countAll % 1000) == 0)
                {
                    pInfo.msg = 2;
                    pInfo.cost = (countAll != 0) ? (countOk * 100.0 / countAll).ToString() : "";
                    pInfo.progress = countAll + maxIndex;
                    this.Invoke(displayProgress, pInfo);
                }
            }

            pInfo.endFlag = true;
            pInfo.msg = 2;
            pInfo.cost = (countAll != 0) ? (countOk * 100.0 / countAll).ToString() : "";
            pInfo.progress = countAll + maxIndex;
            pInfo.calcTime = elapsedTime_ms();
            this.Invoke(displayProgress, pInfo);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            String filename = "network";
            IntPtr charP = Marshal.StringToHGlobalAnsi(filename);

            saveDNN(charP);

            Marshal.FreeHGlobal(charP);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            String filename = "network";
            IntPtr charP = Marshal.StringToHGlobalAnsi(filename);

            loadDNN(charP);

            Marshal.FreeHGlobal(charP);

            ++index;

            button1_Click(sender, e);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            createDNN(checkBox1.Checked);
        }

        private void textBox9_Click(object sender, EventArgs e)
        {
            timer1.Enabled = true;
        }
    }

    public class ProgressInfo
    {
        public int msg;
        public String cost;
        public int progress;
        public bool endFlag;
        public double calcTime;
    }

    delegate void DisplayProgressDelegate(ProgressInfo pInfo);

    public class HiddenPictures
    {
        public IntPtr mem;
        public PictureBox pic;
        public int width;
        public int height;
    }
}
