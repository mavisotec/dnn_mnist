
#define matrixFormat float

typedef struct {
	int width;
	int height;
	int stride;
	matrixFormat * elements;
} CudaMatrix;

__interface PtInterface
{
	void	__stdcall DuplicateObject(void);
	void	__stdcall ReleaseObject(void);
};

__interface IfMatrix : public PtInterface
{
	void InitWithRandomValues(bool lastColumnIsBais);
	void InitWithZero(void);
	void Dot(IfMatrix * _matrixA, IfMatrix * _matrixB, matrixFormat alpha);
	void Dot_aT(IfMatrix * _matrixA, IfMatrix * _matrixB, matrixFormat alpha);
	void Dot_bT(IfMatrix * _matrixA, IfMatrix * _matrixB, matrixFormat alpha);
	void Activation(IfMatrix * _matrixA);
	void Gradient(IfMatrix * _matrixA, IfMatrix * _matrixB);
	void Add(IfMatrix * _matrixA);
	void Subtract(IfMatrix * _matrixA);
	void Multiplay(IfMatrix * _matrixA);
	void Copy(IfMatrix * _matrixA);
	void Copy(int startIndex, IfMatrix * _matrixA);
	void Copy(IfMatrix * _matrixA, int sourceIndex);
	matrixFormat SquareSumm(void);
	void Convolution(IfMatrix * _weights, IfMatrix * _inputMatrix);
	void ConvolutionRot180Akku(IfMatrix * _weights, IfMatrix * _inputMatrix);
	void MaxPooling(IfMatrix * _input);
	void MaxExpand(IfMatrix * _input, IfMatrix * _referenceData);
	void SetBuffer(matrixFormat * data, int dataSize);
	void GetBuffer(matrixFormat * data, int dataSize);
	int Rows(void);
	int Columns(void);
};

#ifndef CL_IF_MATRIX

class MatrixClassFactory
{
	HMODULE hModule;
	IfMatrix * (*entry1)(void);
	IfMatrix * (*entry2)(int, int);
	IfMatrix * (*entry3)(int, int, matrixFormat *);

public:
	MatrixClassFactory(void)
	{
		hModule = LoadLibrary("IfMatrix.dll");

		if (hModule != NULL)
		{
			entry1 = (IfMatrix * (*)(void))GetProcAddress(hModule, "?CreateIfMatrix@@YAPAUIfMatrix@@XZ");
			entry2 = (IfMatrix * (*)(int, int))GetProcAddress(hModule, "?CreateIfMatrix@@YAPAUIfMatrix@@HH@Z");
			entry3 = (IfMatrix * (*)(int, int, matrixFormat *))GetProcAddress(hModule, "?CreateIfMatrix@@YAPAUIfMatrix@@HHPAM@Z");
		}
	};

	~MatrixClassFactory(void)
	{
		if (hModule)
			FreeLibrary(hModule);
	};

	IfMatrix * CreateIfMatrix(void)
	{
		if (entry1)
			return((*entry1)());
		else
			return((IfMatrix * )NULL);
	};

	IfMatrix * CreateIfMatrix(int _rows, int _columns)
	{
		if (entry2)
			return((*entry2)(_rows, _columns));
		else
			return((IfMatrix *)NULL);
	};

	IfMatrix * CreateIfMatrix(int _rows, int _columns, matrixFormat * data)
	{
		if (entry3)
			return((*entry3)(_rows, _columns, data));
		else
			return((IfMatrix *)NULL);
	};
};

#endif // !CL_IF_MATRIX
