// IfMatrix.cpp : Definiert die exportierten Funktionen f�r die DLL-Anwendung.
//

#define CL_IF_MATRIX

#include <math.h>
#include <memory.h>
#include "IfMatrix.hpp"

#include <mkl.h>

#include "cuda_runtime.h"

//#define USE_CUBLAS

#ifdef  USE_CUBLAS

#include "cublas_v2.h"

#endif //  USE_CUBLAS


extern void MatMul(const CudaMatrix &, const CudaMatrix &, CudaMatrix &, matrixFormat);
extern void MatMulT_A(const CudaMatrix &, const CudaMatrix &, CudaMatrix &, matrixFormat);
extern void MatMulT_B(const CudaMatrix &, const CudaMatrix &, CudaMatrix &, matrixFormat);
extern void MatTanh(const CudaMatrix & d_A, CudaMatrix & d_B);
extern void MatGradient(const CudaMatrix & d_A, CudaMatrix & d_B, CudaMatrix & d_C);
extern void MatAdd(const CudaMatrix & d_A, CudaMatrix & d_B);
extern void MatSub(const CudaMatrix & d_A, CudaMatrix & d_B);
extern void MatMulE(const CudaMatrix & d_A, CudaMatrix & d_B);
extern void MatCopy(const CudaMatrix & d_A, CudaMatrix & d_B);
extern void MatCopyToFlat(const CudaMatrix & d_A, CudaMatrix & d_B, int startIndex);
extern void MatCopyFromFlat(const CudaMatrix & d_A, CudaMatrix & d_B, int startIndex);
extern void MatMaxPooling(const CudaMatrix & d_A, CudaMatrix & d_B);
extern void MatMaxExpand(const CudaMatrix & d_A, const CudaMatrix & d_B, CudaMatrix & d_C);
extern void MatConvolution(const CudaMatrix & d_A, const CudaMatrix & d_B, CudaMatrix & d_C);
extern void MatConvolution180Akku(const CudaMatrix & d_A, const CudaMatrix & d_B, CudaMatrix & d_C);

class PtMatrix : public IfMatrix
{
	int columns;
	int rows;
	int stride;
	matrixFormat * matrixData;
	static int seed;
	int instance;
#ifdef  USE_CUBLAS
	static cublasHandle_t handle;
	static int globalInstances;
#endif

	matrixFormat * Buffer(void)
	{
		return(matrixData);
	}

public:

	PtMatrix(void)
	{
		stride = 0;
		rows = 0;
		columns = 0;
		matrixData = NULL;
		instance = 0;

#ifdef  USE_CUBLAS

		if (handle == NULL)
		{
			cublasStatus_t stat = cublasCreate(&handle);
			
			if (stat != CUBLAS_STATUS_SUCCESS)
			{
				handle = NULL;
			}

			globalInstances = 0;
		}

		++globalInstances;

#endif //  USE_CUBLAS
	}

	PtMatrix(int _rows, int _columns)
	{
		stride = ((_columns * sizeof(matrixFormat) / 16) + 1) * 16 / sizeof(matrixFormat);						// 128 Bit alligmant of each row
		rows = _rows;
		columns = _columns;
		cudaMalloc(&matrixData, rows * stride);
		instance = 0;

		matrixFormat * tempData = (matrixFormat *)malloc(rows * stride * sizeof(matrixFormat));

		for (int r = 0; r < rows; ++r)
		{
			for (int c = 0; c < columns; ++c)
			{
				tempData[r * stride + c] = 0.0;
			}
		}

		cudaMemcpy(matrixData, tempData, rows * stride, cudaMemcpyHostToDevice);

		free(tempData);

#ifdef  USE_CUBLAS

		if (handle == NULL)
		{
			cublasStatus_t stat = cublasCreate(&handle);

			if (stat != CUBLAS_STATUS_SUCCESS)
			{
				handle = NULL;
			}

			globalInstances = 0;
		}

		++globalInstances;

#endif //  USE_CUBLAS
	}

	PtMatrix(int _rows, int _columns, matrixFormat * data)
	{
		int count = 0;
		stride = ((_columns * sizeof(matrixFormat) / 16) + 1) * 16 / sizeof(matrixFormat);						// 128 Bit alligmant of each row
		rows = _rows;
		columns = _columns;
		cudaMalloc(&matrixData, rows * stride);
		instance = 0;

		matrixFormat * tempData = (matrixFormat *)malloc(rows * stride * sizeof(matrixFormat));

		for (int r = 0; r < rows; ++r)
		{
			for (int c = 0; c < columns; ++c)
			{
				tempData[r * stride + c] = data[count++];
			}
		}

		cudaMemcpy(matrixData, tempData, rows * stride, cudaMemcpyHostToDevice);

#ifdef  USE_CUBLAS

		if (handle == NULL)
		{
			cublasStatus_t stat = cublasCreate(&handle);

			if (stat != CUBLAS_STATUS_SUCCESS)
			{
				handle = NULL;
			}

			globalInstances = 0;
		}

		++globalInstances;

#endif //  USE_CUBLAS
	}

	~PtMatrix(void)
	{
		cudaFree(matrixData);

#ifdef  USE_CUBLAS
		--globalInstances;

		if (globalInstances == 0)
		{
			cublasDestroy(handle);
			handle = NULL;
		}

#endif //  USE_CUBLAS
	}

	void __stdcall DuplicateObject(void)
	{
		++instance;
	}

	void __stdcall ReleaseObject(void)
	{
		if (--instance == 0)
			delete this;
	}

	void InitWithRandomValues(bool lastColumnIsBais)
	{
		VSLStreamStatePtr stream;
		matrixFormat range = (matrixFormat)(1 / sqrt(rows * columns));
		int count = rows * (columns - (lastColumnIsBais ? 1 : 0));
		int * buff = new int[count];

		if (buff != NULL)
		{
			if (VSL_STATUS_OK == vslNewStream(&stream, VSL_BRNG_R250, seed))
			{
				viRngUniform(VSL_RNG_METHOD_UNIFORM_STD, stream, count, buff, 0, 16777216);

				count = 0;

				matrixFormat * tempData = (matrixFormat *)malloc(rows * stride * sizeof(matrixFormat));

				for (int row = 0; row < rows; ++row)
				{
					int column;

					for (column = 0; column < (columns - (lastColumnIsBais ? 1 : 0)); ++column)
					{
						tempData[column + row * stride] = (matrixFormat)(buff[count++] * 2 * range / 16777216.0 - range);
					}

					if (lastColumnIsBais)
						tempData[column + row * stride] = 1.0f;
				}

				cudaMemcpy(matrixData, tempData, rows * stride, cudaMemcpyHostToDevice);

				free(tempData);

				seed = buff[count - 1];

				vslDeleteStream(&stream);
			}

			delete[] buff;
		}
	}

	void InitWithZero(void)
	{
		matrixFormat * tempData = (matrixFormat *)malloc(rows * stride * sizeof(matrixFormat));

		if (tempData)
		{
			for (int r = 0; r < rows; ++r)
			{
				for (int c = 0; c < columns; ++c)
				{
					tempData[r * stride + c] = 0.0;
				}
			}

			cudaMemcpy(matrixData, tempData, rows * stride, cudaMemcpyHostToDevice);

			free(tempData);
		}
	}
	
#ifdef  USE_CUBLAS

	void Dot(IfMatrix * _matrixA, IfMatrix * _matrixB, matrixFormat alpha)
	{
		PtMatrix * matrixA = (PtMatrix*)_matrixA;
		PtMatrix * matrixB = (PtMatrix*)_matrixB;
		int m = matrixA->rows;					// A.rows and C.rows
		int n = matrixB->columns;				// B.columns and C.columns
		int k = matrixB->rows;					// A.colums and B.rows

		if (handle && (m == rows) && (n == columns) && (k == matrixA->columns))
		{
			matrixFormat beta = 0;
			cublasStatus_t status = cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, m, n, k, &alpha, matrixA->matrixData, matrixA->rows, matrixB->matrixData, matrixB->rows, &beta, matrixData, rows);
		}
	}

	void Dot_aT(IfMatrix * _matrixA, IfMatrix * _matrixB, matrixFormat alpha)
	{
		PtMatrix * matrixA = (PtMatrix*)_matrixA;
		PtMatrix * matrixB = (PtMatrix*)_matrixB;
		int m = (matrixA->columns < rows) ? matrixA->columns : rows;	// A.rows and C.rows, exception to exclude bais from backprop
		int n = matrixB->columns;										// B.columns and C.columns
		int k = matrixB->rows;											// A.colums and B.rows

		if (handle && (m == rows) && (n == columns) && (k == matrixA->rows))
		{
			matrixFormat beta = 0;
			cublasStatus_t status = cublasSgemm(handle, CUBLAS_OP_T, CUBLAS_OP_N, m, n, k, &alpha, matrixA->matrixData, matrixA->rows, matrixB->matrixData, matrixB->rows, &beta, matrixData, rows);
		}
	}

	void Dot_bT(IfMatrix * _matrixA, IfMatrix * _matrixB, matrixFormat alpha)
	{
		PtMatrix * matrixA = (PtMatrix*)_matrixA;
		PtMatrix * matrixB = (PtMatrix*)_matrixB;
		int m = matrixA->rows;					// A.rows and C.rows
		int n = matrixB->rows;					// B.columns and C.columns
		int k = matrixB->columns;				// A.colums and B.rows

		if (handle && (m == rows) && (n == columns) && (k == matrixA->columns))
		{
			matrixFormat beta = 0;
			cublasStatus_t status = cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_T, m, n, k, &alpha, matrixA->matrixData, matrixA->rows, matrixB->matrixData, matrixB->rows, &beta, matrixData, rows);
		}
	}

#else

	void Dot(IfMatrix * _matrixA, IfMatrix * _matrixB, matrixFormat alpha)
	{
		PtMatrix * matrixA = (PtMatrix*)_matrixA;
		PtMatrix * matrixB = (PtMatrix*)_matrixB;
		CudaMatrix d_A, d_B, d_C;

		d_A.width = matrixA->columns;
		d_A.height = matrixA->rows;
		d_A.stride = matrixA->stride;
		d_A.elements = matrixA->Buffer();
			
		d_B.width = matrixB->columns;
		d_B.height = matrixB->rows;
		d_B.stride = matrixB->stride;
		d_B.elements = matrixB->Buffer();

		d_C.width = columns;
		d_C.height = rows;
		d_C.stride = stride;
		d_C.elements = matrixData;

		int m = matrixA->rows;					// A.rows and C.rows
		int n = matrixB->columns;				// B.columns and C.columns
		int k = matrixB->rows;					// A.colums and B.rows

		if ((m == rows) && (n == columns) && (k == matrixA->columns))
			MatMul(d_A, d_B, d_C, alpha);
	}
	
	void Dot_aT(IfMatrix * _matrixA, IfMatrix * _matrixB, matrixFormat alpha)
	{
		PtMatrix * matrixA = (PtMatrix*)_matrixA;
		PtMatrix * matrixB = (PtMatrix*)_matrixB;
		CudaMatrix d_A, d_B, d_C;

		d_A.width = matrixA->columns;
		d_A.height = matrixA->rows;
		d_A.stride = matrixA->stride;
		d_A.elements = matrixA->Buffer();

		d_B.width = matrixB->columns;
		d_B.height = matrixB->rows;
		d_B.stride = matrixB->stride;
		d_B.elements = matrixB->Buffer();

		d_C.width = columns;
		d_C.height = rows;
		d_C.stride = stride;
		d_C.elements = matrixData;

		int m = (matrixA->columns > rows) ? rows : matrixA->columns;	// A.rows and C.rows, exception to exclude bais from backprop
		int n = matrixB->columns;										// B.columns and C.columns
		int k = matrixB->rows;											// A.colums and B.rows

		if ((m == rows) && (n == columns) && (k == matrixA->rows))
			MatMulT_A(d_A, d_B, d_C, alpha);
	}

	void Dot_bT(IfMatrix * _matrixA, IfMatrix * _matrixB, matrixFormat alpha)
	{
		PtMatrix * matrixA = (PtMatrix*)_matrixA;
		PtMatrix * matrixB = (PtMatrix*)_matrixB;
		CudaMatrix d_A, d_B, d_C;

		d_A.width = matrixA->columns;
		d_A.height = matrixA->rows;
		d_A.stride = matrixA->stride;
		d_A.elements = matrixA->Buffer();

		d_B.width = matrixB->columns;
		d_B.height = matrixB->rows;
		d_B.stride = matrixB->stride;
		d_B.elements = matrixB->Buffer();

		d_C.width = columns;
		d_C.height = rows;
		d_C.stride = stride;
		d_C.elements = matrixData;

		int m = matrixA->rows;					// A.rows and C.rows
		int n = matrixB->rows;					// B.columns and C.columns
		int k = matrixB->columns;				// A.colums and B.rows

		if ((m == rows) && (n == columns) && (k == matrixA->columns))
			MatMulT_B(d_A, d_B, d_C, alpha);
	}
	
#endif

	void Activation(IfMatrix * _matrixA)
	{
		PtMatrix * matrixA = (PtMatrix*)_matrixA;

		if ((matrixA->rows == rows) && (matrixA->columns == columns))
		{
			CudaMatrix d_A, d_B;

			d_A.width = matrixA->columns;
			d_A.height = matrixA->rows;
			d_A.stride = matrixA->stride;
			d_A.elements = matrixA->Buffer();

			d_B.width = columns;
			d_B.height = rows;
			d_B.stride = stride;
			d_B.elements = matrixData;

			MatTanh(d_A, d_B);
		}
	}

	void Gradient(IfMatrix * _matrixA, IfMatrix * _matrixB)
	{
		PtMatrix * matrixA = (PtMatrix*)_matrixA;
		PtMatrix * matrixB = (PtMatrix*)_matrixB;

		if ((matrixA->rows == rows) && (matrixA->columns == columns) && (matrixB->rows == rows) && (matrixB->columns == columns))
		{
			CudaMatrix d_A, d_B, d_C;

			d_A.width = matrixA->columns;
			d_A.height = matrixA->rows;
			d_A.stride = matrixA->stride;
			d_A.elements = matrixA->Buffer();

			d_B.width = matrixB->columns;
			d_B.height = matrixB->rows;
			d_B.stride = matrixB->stride;
			d_B.elements = matrixB->Buffer();

			d_C.width = columns;
			d_C.height = rows;
			d_C.stride = stride;
			d_C.elements = matrixData;

			MatGradient(d_A, d_B, d_C);
		}
	}

	void Add(IfMatrix * _matrixA)
	{
		PtMatrix * matrixA = (PtMatrix*)_matrixA;

		if ((matrixA->rows == rows) && (matrixA->columns == columns))
		{
			CudaMatrix d_A, d_B;

			d_A.width = matrixA->columns;
			d_A.height = matrixA->rows;
			d_A.stride = matrixA->stride;
			d_A.elements = matrixA->Buffer();

			d_B.width = columns;
			d_B.height = rows;
			d_B.stride = stride;
			d_B.elements = matrixData;

			MatAdd(d_A, d_B);
		}
	}

	void Subtract(IfMatrix * _matrixA)
	{
		PtMatrix * matrixA = (PtMatrix*)_matrixA;

		if ((matrixA->rows == rows) && (matrixA->columns == columns))
		{
			CudaMatrix d_A, d_B;

			d_A.width = matrixA->columns;
			d_A.height = matrixA->rows;
			d_A.stride = matrixA->stride;
			d_A.elements = matrixA->Buffer();

			d_B.width = columns;
			d_B.height = rows;
			d_B.stride = stride;
			d_B.elements = matrixData;

			MatSub(d_A, d_B);
		}
	}

	void Multiplay(IfMatrix * _matrixA)
	{
		PtMatrix * matrixA = (PtMatrix*)_matrixA;

		if ((matrixA->rows == rows) && (matrixA->columns == columns))
		{
			CudaMatrix d_A, d_B;

			d_A.width = matrixA->columns;
			d_A.height = matrixA->rows;
			d_A.stride = matrixA->stride;
			d_A.elements = matrixA->Buffer();

			d_B.width = columns;
			d_B.height = rows;
			d_B.stride = stride;
			d_B.elements = matrixData;

			MatMulE(d_A, d_B);
		}
	}

	void Copy(IfMatrix * _matrixA)
	{
		PtMatrix * matrixA = (PtMatrix*)_matrixA;
		CudaMatrix d_A, d_B;

		d_A.width = matrixA->columns;
		d_A.height = matrixA->rows;
		d_A.stride = matrixA->stride;
		d_A.elements = matrixA->Buffer();

		d_B.width = columns;
		d_B.height = rows;
		d_B.stride = stride;
		d_B.elements = matrixData;

		MatCopy(d_A, d_B);
	}

	void Copy(int startIndex, IfMatrix * _matrixA)
	{
		PtMatrix * matrixA = (PtMatrix*)_matrixA;

		int cellCount = matrixA->rows * matrixA->columns;

		if (startIndex + cellCount <= (rows * columns))
		{
			CudaMatrix d_A, d_B;

			d_A.width = matrixA->columns;
			d_A.height = matrixA->rows;
			d_A.stride = matrixA->stride;
			d_A.elements = matrixA->Buffer();

			d_B.width = columns;
			d_B.height = rows;
			d_B.stride = stride;
			d_B.elements = matrixData;

			MatCopyToFlat(d_A, d_B, startIndex);
		}
	}

	void Copy(IfMatrix * _matrixA, int sourceIndex)
	{
		PtMatrix * matrixA = (PtMatrix*)_matrixA;

		int cellCount = rows * columns;

		if (sourceIndex + cellCount <= (matrixA->rows *  matrixA->columns))
		{
			CudaMatrix d_A, d_B;

			d_A.width = matrixA->columns;
			d_A.height = matrixA->rows;
			d_A.stride = matrixA->stride;
			d_A.elements = matrixA->Buffer();

			d_B.width = columns;
			d_B.height = rows;
			d_B.stride = stride;
			d_B.elements = matrixData;

			MatCopyFromFlat(d_A, d_B, sourceIndex);
		}
	}

	matrixFormat SquareSumm(void)
	{
		matrixFormat a, summ = 0;

		matrixFormat * sData, * tempData = (matrixFormat *)malloc(rows * stride * sizeof(matrixFormat));

		sData = tempData;

		GetBuffer(sData, rows * columns * stride);

		for (int r = 0; r < rows; ++r)
		{
			for (int c = 0; c < columns; ++c)
			{
				a = sData[r * stride + c];
				summ += a * a;
			}
		}

		free(tempData);

		return(summ);
	}

	void Convolution(IfMatrix * _weights, IfMatrix * _inputMatrix)
	{
		PtMatrix * weights = (PtMatrix*)_weights;
		PtMatrix * inputMatrix = (PtMatrix*)_inputMatrix;
		CudaMatrix d_A, d_B, d_C;

		d_A.width = inputMatrix->columns;
		d_A.height = inputMatrix->rows;
		d_A.stride = inputMatrix->stride;
		d_A.elements = inputMatrix->Buffer();

		d_B.width = weights->columns;
		d_B.height = weights->rows;
		d_B.stride = weights->stride;
		d_B.elements = weights->Buffer();

		d_C.width = columns;
		d_C.height = rows;
		d_C.stride = stride;
		d_C.elements = matrixData;

		if ((d_B.width == 3) && (d_B.height == 3))
			MatConvolution(d_A, d_B, d_C);
	}

	void ConvolutionRot180Akku(IfMatrix * _weights, IfMatrix * _inputMatrix)
	{
		PtMatrix * weights = (PtMatrix*)_weights;
		PtMatrix * inputMatrix = (PtMatrix*)_inputMatrix;
		CudaMatrix d_A, d_B, d_C;

		d_A.width = inputMatrix->columns;
		d_A.height = inputMatrix->rows;
		d_A.stride = inputMatrix->stride;
		d_A.elements = inputMatrix->Buffer();

		d_B.width = weights->columns;
		d_B.height = weights->rows;
		d_B.stride = weights->stride;
		d_B.elements = weights->Buffer();

		d_C.width = columns;
		d_C.height = rows;
		d_C.stride = stride;
		d_C.elements = matrixData;

		if ((d_B.width == 3) && (d_B.height == 3))
			MatConvolution180Akku(d_A, d_B, d_C);
	}

	void MaxPooling(IfMatrix * _input)
	{
		PtMatrix * input = (PtMatrix*)_input;

		if ((input->Columns() / 2 <= columns) && (input->Rows() / 2 <= rows))
		{
			CudaMatrix d_A, d_B;

			d_A.width = input->columns;
			d_A.height = input->rows;
			d_A.stride = input->stride;
			d_A.elements = input->Buffer();

			d_B.width = columns;
			d_B.height = rows;
			d_B.stride = stride;
			d_B.elements = matrixData;

			MatMaxPooling(d_A, d_B);
		}
	}

	void MaxExpand(IfMatrix * _input, IfMatrix * _referenceData)
	{
		PtMatrix * input = (PtMatrix*)_input;
		PtMatrix * referenceData = (PtMatrix*)_referenceData;

		if ((input->Columns() * 2 >= columns) && (input->Rows() * 2 >= rows) && (referenceData->Columns() * 2 == columns) && (referenceData->Rows() * 2 == rows))
		{
			CudaMatrix d_A, d_B, d_C;

			d_A.width = input->columns;
			d_A.height = input->rows;
			d_A.stride = input->stride;
			d_A.elements = input->Buffer();

			d_A.width = referenceData->columns;
			d_A.height = referenceData->rows;
			d_A.stride = referenceData->stride;
			d_A.elements = referenceData->Buffer();

			d_C.width = columns;
			d_C.height = rows;
			d_C.stride = stride;
			d_C.elements = matrixData;

			MatMaxExpand(d_A, d_B, d_C);
		}
	}
	
	void SetBuffer(matrixFormat * data, int dataSize)
	{
		if ((matrixData != NULL) && (dataSize <= (rows * columns)))
		{
			matrixFormat * tempData = (matrixFormat *)malloc(rows * stride * sizeof(matrixFormat));

			if (tempData)
			{
				int count = 0;

				for (int r = 0; r < rows; ++r)
				{
					for (int c = 0; c < columns; ++c)
					{
						tempData[r * stride + c] = data[count++];
					}
				}

				cudaMemcpy(matrixData, tempData, rows * stride, cudaMemcpyHostToDevice);

				free(tempData);
			}
		}
	}

	void GetBuffer(matrixFormat * data, int dataSize)
	{
		if ((matrixData != NULL) && (dataSize >= (rows * columns)))
		{
			matrixFormat * tempData = (matrixFormat *)malloc(rows * stride * sizeof(matrixFormat));

			if (tempData)
			{
				int count = 0;

				cudaMemcpy(tempData, matrixData, rows * stride, cudaMemcpyDeviceToHost);

				for (int r = 0; r < rows; ++r)
				{
					for (int c = 0; c < columns; ++c)
					{
						data[count++] = tempData[r * stride + c];
					}
				}

				free(tempData);
			}
		}
	}

	int Rows(void)
	{
		return(rows);
	}

	int Columns(void)
	{
		return(columns);
	}
};

#ifdef  USE_CUBLAS

cublasHandle_t PtMatrix::handle = NULL;
int PtMatrix::globalInstances = 0;

#endif //  USE_CUBLAS

int PtMatrix::seed = 3456;

__declspec(dllexport) IfMatrix * CreateIfMatrix(void)
{
	PtMatrix * matrix = new PtMatrix();

	if (matrix != NULL)
		matrix->DuplicateObject();

	return((IfMatrix *)matrix );
}

__declspec(dllexport) IfMatrix * CreateIfMatrix(int _rows, int _columns)
{
	PtMatrix * matrix = new PtMatrix(_rows, _columns);

	if (matrix != NULL)
		matrix->DuplicateObject();

	return((IfMatrix *)matrix);
}

__declspec(dllexport) IfMatrix * CreateIfMatrix(int _rows, int _columns, matrixFormat * data)
{
	PtMatrix * matrix = new PtMatrix(_rows, _columns, data);

	if (matrix != NULL)
		matrix->DuplicateObject();

	return((IfMatrix *)matrix);
}

