
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

// Helper functions and utilities to work with CUDA
#include "helper_cuda.h"

#include <stdio.h>

#define CL_IF_MATRIX
#define USE_CUDA

#include "..\..\IfMatrix\IfMatrix.hpp"


// Thread block size
#define BLOCK_SIZE 16

// Get a matrix element
__forceinline__ __device__ matrixFormat GetElement(const CudaMatrix A, int row, int col)
{
	matrixFormat element = 0.0f;

	if ((row < A.height) && (col < A.width) && (row >= 0) && (col >= 0))
		element = A.elements[row * A.stride + col];

	return element;
}

// Set a matrix element
__forceinline__ __device__ void SetElement(CudaMatrix A, int row, int col,	matrixFormat value)
{
	if ((row < A.height) && (col < A.width))
		A.elements[row * A.stride + col] = value;
}

// Matrix multiplication kernel called by MatMul()
__global__ void MatMulKernel(const CudaMatrix A, const CudaMatrix B, CudaMatrix C, matrixFormat alpha)
{
	matrixFormat Cvalue = 0.0f;

	// Thread row and column within Block
	int sCol = threadIdx.x;
	int sRow = threadIdx.y;

	int col = blockIdx.x * blockDim.x + sCol;
	int row = blockIdx.y * blockDim.y + sRow;

	int blockCount = A.width / BLOCK_SIZE + ((A.width % BLOCK_SIZE != 0) ? 1 : 0);

	// Loop over all the sub-matrices of A and B that are
	// required to compute Csub
	// Multiply each pair of sub-matrices together
	// and accumulate the results
	for (int m = 0; m < blockCount; ++m)
	{
		int size = A.width - m * BLOCK_SIZE;
		
		if (size > BLOCK_SIZE)
			size = BLOCK_SIZE;

		// Shared memory used to store Asub and Bsub respectively
		__shared__ volatile matrixFormat As[BLOCK_SIZE][BLOCK_SIZE];
		__shared__ volatile matrixFormat Bs[BLOCK_SIZE][BLOCK_SIZE];

		// Load Asub and Bsub from device memory to shared memory
		// Each thread loads one element of each sub-matrix
		As[sRow][sCol] = GetElement(A, row, m * BLOCK_SIZE + sCol);
		Bs[sRow][sCol] = GetElement(B, m * BLOCK_SIZE + sRow, col);

		// Synchronize to make sure the sub-matrices are loaded
		// before starting the computation
		__syncthreads();

		// Multiply Asub and Bsub together
		for (int e = 0; e < size; ++e)
			Cvalue += As[sRow][e] * Bs[e][sCol];

		// Synchronize to make sure that the preceding
		// computation is done before loading two new
		// sub-matrices of A and B in the next iteration
		__syncthreads();
	}

	SetElement(C, row, col, Cvalue * alpha);
}

// Matrix multiplication kernel called by MatMul()
__global__ void MatMulKernelWithActivation(const CudaMatrix A, const CudaMatrix B, CudaMatrix C)
{
	matrixFormat Cvalue = 0.0f;

	// Thread row and column within Block
	int sCol = threadIdx.x;
	int sRow = threadIdx.y;

	int col = blockIdx.x * blockDim.x + sCol;
	int row = blockIdx.y * blockDim.y + sRow;

	int blockCount = A.width / BLOCK_SIZE + ((A.width % BLOCK_SIZE != 0) ? 1 : 0);

	// Loop over all the sub-matrices of A and B that are
	// required to compute Csub
	// Multiply each pair of sub-matrices together
	// and accumulate the results
	for (int m = 0; m < blockCount; ++m)
	{
		int size = A.width - m * BLOCK_SIZE;

		if (size > BLOCK_SIZE)
			size = BLOCK_SIZE;

		// Shared memory used to store Asub and Bsub respectively
		__shared__ volatile matrixFormat As[BLOCK_SIZE][BLOCK_SIZE];
		__shared__ volatile matrixFormat Bs[BLOCK_SIZE][BLOCK_SIZE];

		// Load Asub and Bsub from device memory to shared memory
		// Each thread loads one element of each sub-matrix
		As[sRow][sCol] = GetElement(A, row, m * BLOCK_SIZE + sCol);
		Bs[sRow][sCol] = GetElement(B, m * BLOCK_SIZE + sRow, col);

		// Synchronize to make sure the sub-matrices are loaded
		// before starting the computation
		__syncthreads();

		// Multiply Asub and Bsub together
		for (int e = 0; e < size; ++e)
			Cvalue += As[sRow][e] * Bs[e][sCol];

		// Synchronize to make sure that the preceding
		// computation is done before loading two new
		// sub-matrices of A and B in the next iteration
		__syncthreads();
	}

	SetElement(C, row, col, tanhf(Cvalue));
}

// Matrix multiplication - Host code
void MatMul(const CudaMatrix & d_A, const CudaMatrix & d_B, CudaMatrix & d_C, matrixFormat alpha, cudaStream_t &stream)
{
	// Invoke kernel
	dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
	dim3 dimGrid(d_B.width / BLOCK_SIZE + ((d_B.width % BLOCK_SIZE != 0) ? 1 : 0), d_A.height / BLOCK_SIZE + ((d_A.height % BLOCK_SIZE != 0) ? 1 : 0));
	MatMulKernel <<<dimGrid, dimBlock, 0, stream>>>(d_A, d_B, d_C, alpha);
}

// Matrix multiplication kernel called by MatMulT_A()
__global__ void MatMulKernelT_A(const CudaMatrix A, const CudaMatrix B, CudaMatrix C, matrixFormat alpha)
{
	matrixFormat Cvalue = 0.0f;

	// Thread row and column within Block
	int sCol = threadIdx.x;
	int sRow = threadIdx.y;

	int col = blockIdx.x * blockDim.x + sCol;
	int row = blockIdx.y * blockDim.y + sRow;

	int blockCount = B.height / BLOCK_SIZE + ((B.height % BLOCK_SIZE != 0) ? 1 : 0);

	// Loop over all the sub-matrices of A and B that are
	// required to compute Csub
	// Multiply each pair of sub-matrices together
	// and accumulate the results
	for (int m = 0; m < blockCount; ++m)
	{
		int size = B.height - m * BLOCK_SIZE;

		if (size > BLOCK_SIZE)
			size = BLOCK_SIZE;

		// Shared memory used to store Asub and Bsub respectively
		__shared__ volatile matrixFormat As[BLOCK_SIZE][BLOCK_SIZE + 1];
		__shared__ volatile matrixFormat Bs[BLOCK_SIZE][BLOCK_SIZE];

		// Load Asub and Bsub from device memory to shared memory
		// Each thread loads one element of each sub-matrix
		As[sRow][sCol] = GetElement(A, m * BLOCK_SIZE + sCol, row);
		Bs[sRow][sCol] = GetElement(B, m * BLOCK_SIZE + sRow, col);

		// Synchronize to make sure the sub-matrices are loaded
		// before starting the computation
		__syncthreads();

		// Multiply Asub and Bsub together
		for (int e = 0; e < size; ++e)
			Cvalue += As[sRow][e] * Bs[e][sCol];

		// Synchronize to make sure that the preceding
		// computation is done before loading two new
		// sub-matrices of A and B in the next iteration
		__syncthreads();
	}

	SetElement(C, row, col, Cvalue * alpha);
}

// Matrix multiplication - Host code
void MatMulT_A(const CudaMatrix & d_A, const CudaMatrix & d_B, CudaMatrix & d_C, matrixFormat alpha, cudaStream_t &stream)
{
	// Invoke kernel
	dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
	dim3 dimGrid(d_B.width / BLOCK_SIZE + ((d_B.width % BLOCK_SIZE != 0) ? 1 : 0), d_A.width / BLOCK_SIZE + ((d_A.width % BLOCK_SIZE != 0) ? 1 : 0));
	MatMulKernelT_A <<<dimGrid, dimBlock, 0, stream >>>(d_A, d_B, d_C, alpha);
}

// Matrix multiplication kernel called by MatMulT_B()
__global__ void MatMulKernelT_B(const CudaMatrix A, const CudaMatrix B, CudaMatrix C, matrixFormat alpha)
{
	matrixFormat Cvalue = 0.0f;

	// Thread row and column within Block
	int sCol = threadIdx.x;
	int sRow = threadIdx.y;

	int col = blockIdx.x * blockDim.x + sCol;
	int row = blockIdx.y * blockDim.y + sRow;

	int blockCount = A.width / BLOCK_SIZE + ((A.width % BLOCK_SIZE != 0) ? 1 : 0);

	// Loop over all the sub-matrices of A and B that are
	// required to compute Csub
	// Multiply each pair of sub-matrices together
	// and accumulate the results
	for (int m = 0; m < blockCount; ++m)
	{
		int size = A.width - m * BLOCK_SIZE;

		if (size > BLOCK_SIZE)
			size = BLOCK_SIZE;

		// Shared memory used to store Asub and Bsub respectively
		__shared__ volatile matrixFormat As[BLOCK_SIZE][BLOCK_SIZE];
		__shared__ volatile matrixFormat Bs[BLOCK_SIZE][BLOCK_SIZE + 1];

		// Load Asub and Bsub from device memory to shared memory
		// Each thread loads one element of each sub-matrix
		As[sRow][sCol] = GetElement(A, row, m * BLOCK_SIZE + sCol);
		Bs[sRow][sCol] = GetElement(B, col, m * BLOCK_SIZE + sRow);

		// Synchronize to make sure the sub-matrices are loaded
		// before starting the computation
		__syncthreads();

		// Multiply Asub and Bsub together
		for (int e = 0; e < size; ++e)
			Cvalue += As[sRow][e] * Bs[e][sCol];

		// Synchronize to make sure that the preceding
		// computation is done before loading two new
		// sub-matrices of A and B in the next iteration
		__syncthreads();
	}

	SetElement(C, row, col, Cvalue * alpha);
}

// Matrix multiplication - Host code
void MatMulT_B(const CudaMatrix & d_A, const CudaMatrix & d_B, CudaMatrix & d_C, matrixFormat alpha, cudaStream_t &stream)
{
	// Invoke kernel
	dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
	dim3 dimGrid(d_B.height / BLOCK_SIZE + ((d_B.height % BLOCK_SIZE != 0) ? 1 : 0), d_A.height / BLOCK_SIZE + ((d_A.height % BLOCK_SIZE != 0) ? 1 : 0));
	MatMulKernelT_B <<<dimGrid, dimBlock, 0, stream >>>(d_A, d_B, d_C, alpha);
}

// Matrix tanh kernel called by MatTanh()
__global__ void MatActivationTanhKernel(const CudaMatrix A, CudaMatrix B)
{
	// Each thread computes one element of B
	int col = blockIdx.x * blockDim.x + threadIdx.x;
	int row = blockIdx.y * blockDim.y + threadIdx.y;
	int sCol = threadIdx.x;
	int sRow = threadIdx.y;

	__shared__ volatile matrixFormat As[BLOCK_SIZE][BLOCK_SIZE];
	As[sRow][sCol] = GetElement(A, row, col);

	if ((col < A.width) && (row < A.height))
	{
		B.elements[row * B.stride + col] = tanhf(As[sRow][sCol]);
	}
}

// Matrix tanh - Host code
void MatActivationTanh(const CudaMatrix & d_A, CudaMatrix & d_B, cudaStream_t &stream)
{
	// Invoke kernel
	dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
	dim3 dimGrid(d_A.width / BLOCK_SIZE + ((d_A.width % BLOCK_SIZE != 0) ? 1 : 0), d_A.height / BLOCK_SIZE + ((d_A.height % BLOCK_SIZE != 0) ? 1 : 0));
	MatActivationTanhKernel <<<dimGrid, dimBlock, 0, stream >>>(d_A, d_B);
}

// Matrix tanh kernel called by MatTanh()
__global__ void MatActivationReluKernel(const CudaMatrix A, CudaMatrix B)
{
	// Each thread computes one element of B
	int col = blockIdx.x * blockDim.x + threadIdx.x;
	int row = blockIdx.y * blockDim.y + threadIdx.y;
	int sCol = threadIdx.x;
	int sRow = threadIdx.y;

	__shared__ volatile matrixFormat As[BLOCK_SIZE][BLOCK_SIZE];
	As[sRow][sCol] = GetElement(A, row, col);

	if ((col < A.width) && (row < A.height))
	{
		matrixFormat a = As[sRow][sCol];
		B.elements[row * B.stride + col] = ( a > 0 ) ? a : 0;
	}
}

// Matrix tanh - Host code
void MatActivationRelu(const CudaMatrix & d_A, CudaMatrix & d_B, cudaStream_t &stream)
{
	// Invoke kernel
	dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
	dim3 dimGrid(d_A.width / BLOCK_SIZE + ((d_A.width % BLOCK_SIZE != 0) ? 1 : 0), d_A.height / BLOCK_SIZE + ((d_A.height % BLOCK_SIZE != 0) ? 1 : 0));
	MatActivationReluKernel << <dimGrid, dimBlock, 0, stream >> >(d_A, d_B);
}

// Matrix Gradient kernel called by MatGradient()
__global__ void MatGradientTanhKernel(const CudaMatrix A, const CudaMatrix B, CudaMatrix C)
{
	// Each thread computes one element of C
	int col = blockIdx.x * blockDim.x + threadIdx.x;
	int row = blockIdx.y * blockDim.y + threadIdx.y;
	int sCol = threadIdx.x;
	int sRow = threadIdx.y;

	__shared__ volatile matrixFormat As[BLOCK_SIZE][BLOCK_SIZE];
	__shared__ volatile matrixFormat Bs[BLOCK_SIZE][BLOCK_SIZE];
	As[sRow][sCol] = GetElement(A, row, col);
	Bs[sRow][sCol] = GetElement(B, row, col);

	if ((col < A.width) && (row < A.height))
	{
		matrixFormat a = As[sRow][sCol];
		C.elements[row * C.stride + col] = ( 1 - a * a ) * Bs[sRow][sCol];
	}
}

// Matrix Gradient - Host code
void MatGradientTanh(const CudaMatrix & d_A, CudaMatrix & d_B, CudaMatrix & d_C, cudaStream_t &stream)
{
	// Invoke kernel
	dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
	dim3 dimGrid(d_A.width / BLOCK_SIZE + ((d_A.width % BLOCK_SIZE != 0) ? 1 : 0), d_A.height / BLOCK_SIZE + ((d_A.height % BLOCK_SIZE != 0) ? 1 : 0));
	MatGradientTanhKernel <<<dimGrid, dimBlock, 0, stream >>>(d_A, d_B, d_C);
}

// Matrix Gradient kernel called by MatGradientRelu()
__global__ void MatGradientReluKernel(const CudaMatrix A, const CudaMatrix B, CudaMatrix C)
{
	// Each thread computes one element of C
	int col = blockIdx.x * blockDim.x + threadIdx.x;
	int row = blockIdx.y * blockDim.y + threadIdx.y;
	int sCol = threadIdx.x;
	int sRow = threadIdx.y;

	__shared__ volatile matrixFormat As[BLOCK_SIZE][BLOCK_SIZE];
	__shared__ volatile matrixFormat Bs[BLOCK_SIZE][BLOCK_SIZE];
	As[sRow][sCol] = GetElement(A, row, col);
	Bs[sRow][sCol] = GetElement(B, row, col);

	if ((col < A.width) && (row < A.height))
	{
		matrixFormat a = As[sRow][sCol];
		C.elements[row * C.stride + col] = (a > 0 ) ? Bs[sRow][sCol] : 0;
	}
}

// Matrix Gradient - Host code
void MatGradientRelu(const CudaMatrix & d_A, CudaMatrix & d_B, CudaMatrix & d_C, cudaStream_t &stream)
{
	// Invoke kernel
	dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
	dim3 dimGrid(d_A.width / BLOCK_SIZE + ((d_A.width % BLOCK_SIZE != 0) ? 1 : 0), d_A.height / BLOCK_SIZE + ((d_A.height % BLOCK_SIZE != 0) ? 1 : 0));
	MatGradientReluKernel <<<dimGrid, dimBlock, 0, stream >>>(d_A, d_B, d_C);
}

// Matrix add kernel called by MatAdd()
__global__ void MatAddKernel(const CudaMatrix A, CudaMatrix B)
{
	// Each thread computes one element of B
	int col = blockIdx.x * blockDim.x + threadIdx.x;
	int row = blockIdx.y * blockDim.y + threadIdx.y;
	int sCol = threadIdx.x;
	int sRow = threadIdx.y;

	__shared__ volatile matrixFormat As[BLOCK_SIZE][BLOCK_SIZE];
	As[sRow][sCol] = GetElement(A, row, col);

	if ((col < A.width) && (row < A.height))
	{
		B.elements[row * B.stride + col] += As[sRow][sCol];
	}
}

// Matrix add - Host code
void MatAdd(const CudaMatrix & d_A, CudaMatrix & d_B, cudaStream_t &stream)
{
	// Invoke kernel
	dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
	dim3 dimGrid(d_A.width / BLOCK_SIZE + ((d_A.width % BLOCK_SIZE != 0) ? 1 : 0), d_A.height / BLOCK_SIZE + ((d_A.height % BLOCK_SIZE != 0) ? 1 : 0));
	MatAddKernel <<<dimGrid, dimBlock, 0, stream >>>(d_A, d_B);
}

// Matrix add kernel called by MatSub()
__global__ void MatSubKernel(const CudaMatrix A, CudaMatrix B)
{
	// Each thread computes one element of B
	int col = blockIdx.x * blockDim.x + threadIdx.x;
	int row = blockIdx.y * blockDim.y + threadIdx.y;
	int sCol = threadIdx.x;
	int sRow = threadIdx.y;

	__shared__ volatile matrixFormat As[BLOCK_SIZE][BLOCK_SIZE];
	As[sRow][sCol] = GetElement(A, row, col);

	if ((col < A.width) && (row < A.height))
	{
		B.elements[row * B.stride + col] -= As[sRow][sCol];
	}
}

// Matrix add - Host code
void MatSub(const CudaMatrix & d_A, CudaMatrix & d_B, cudaStream_t &stream)
{
	// Invoke kernel
	dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
	dim3 dimGrid(d_A.width / BLOCK_SIZE + ((d_A.width % BLOCK_SIZE != 0) ? 1 : 0), d_A.height / BLOCK_SIZE + ((d_A.height % BLOCK_SIZE != 0) ? 1 : 0));
	MatSubKernel <<<dimGrid, dimBlock, 0, stream >>>(d_A, d_B);
}

// Matrix mul kernel called by MatMulE()
__global__ void MatMulEKernel(const CudaMatrix A, CudaMatrix B)
{
	// Each thread computes one element of B
	int col = blockIdx.x * blockDim.x + threadIdx.x;
	int row = blockIdx.y * blockDim.y + threadIdx.y;
	int sCol = threadIdx.x;
	int sRow = threadIdx.y;

	__shared__ volatile matrixFormat As[BLOCK_SIZE][BLOCK_SIZE];
	As[sRow][sCol] = GetElement(A, row, col);

	if ((col < A.width) && (row < A.height))
	{
		B.elements[row * B.stride + col] *= As[sRow][sCol];
	}
}

// Matrix mul element wise - Host code
void MatMulE(const CudaMatrix & d_A, CudaMatrix & d_B, cudaStream_t &stream)
{
	// Invoke kernel
	dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
	dim3 dimGrid(d_A.width / BLOCK_SIZE + ((d_A.width % BLOCK_SIZE != 0) ? 1 : 0), d_A.height / BLOCK_SIZE + ((d_A.height % BLOCK_SIZE != 0) ? 1 : 0));
	MatMulEKernel <<<dimGrid, dimBlock, 0, stream >>>(d_A, d_B);
}

// Matrix copy kernel called by MatCopy(), if row(B) > row(A) set B[] = 1 ( bais ) 
__global__ void MatCopyKernel(const CudaMatrix A, CudaMatrix B)
{
	// Each thread computes one element of B
	int col = blockIdx.x * blockDim.x + threadIdx.x;
	int row = blockIdx.y * blockDim.y + threadIdx.y;
	int sCol = threadIdx.x;
	int sRow = threadIdx.y;

	__shared__ volatile matrixFormat As[BLOCK_SIZE][BLOCK_SIZE];
	As[sRow][sCol] = GetElement(A, row, col);

	if ((col < A.width) && (row < A.height))
	{
		B.elements[row * B.stride + col] = As[sRow][sCol];
	}
	else
	{
		if ((col < B.width) && (row < B.height))
			B.elements[row * B.stride + col] = 1.0f;
	}
}

// Matrix copy - Host code
void MatCopy(const CudaMatrix & d_A, CudaMatrix & d_B, cudaStream_t &stream)
{
	// Invoke kernel
	dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
	dim3 dimGrid(d_B.width / BLOCK_SIZE + ((d_B.width % BLOCK_SIZE != 0) ? 1 : 0), d_B.height / BLOCK_SIZE + ((d_B.height % BLOCK_SIZE != 0) ? 1 : 0));
	MatCopyKernel <<<dimGrid, dimBlock, 0, stream >>>(d_A, d_B);
}

// Matrix copy kernel called by MatCopy(), if row(B) > row(A) set B[] = 1 ( bais ) 
__global__ void MatCopyKernelToFlat(const CudaMatrix A, CudaMatrix B, int startIndex)
{
	// Each thread computes one element of B
	int col = blockIdx.x * blockDim.x + threadIdx.x;
	int row = blockIdx.y * blockDim.y + threadIdx.y;
	int index = startIndex + col + row * A.width;
	int dColumn = index % B.width;
	int dRow = index / B.width;
	int sCol = threadIdx.x;
	int sRow = threadIdx.y;

	__shared__ volatile matrixFormat As[BLOCK_SIZE][BLOCK_SIZE];
	As[sRow][sCol] = GetElement(A, row, col);

	if ((col < A.width) && (row < A.height) && (dColumn < B.width) && (dRow < B.height))
	{
		B.elements[dRow * B.stride + dColumn] = As[sRow][sCol];;
	}
}

// Matrix copy - Host code
void MatCopyToFlat(const CudaMatrix & d_A, CudaMatrix & d_B, int startIndex, cudaStream_t &stream)
{
	// Invoke kernel
	dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
	dim3 dimGrid(d_A.width / BLOCK_SIZE + ((d_A.width % BLOCK_SIZE != 0) ? 1 : 0), d_A.height / BLOCK_SIZE + ((d_A.height % BLOCK_SIZE != 0) ? 1 : 0));
	MatCopyKernelToFlat <<<dimGrid, dimBlock, 0, stream >>>(d_A, d_B, startIndex);
}

// Matrix copy kernel called by MatCopy(), if row(B) > row(A) set B[] = 1 ( bais ) 
__global__ void MatCopyKernelFromFlat(const CudaMatrix A, CudaMatrix B, int startIndex)
{
	// Each thread computes one element of B
	int col = blockIdx.x * blockDim.x + threadIdx.x;
	int row = blockIdx.y * blockDim.y + threadIdx.y;
	int index = startIndex + col + row * B.width;
	int sColumn = index % A.width;
	int sRow = index / A.width;

	if ((col < B.width) && (row < B.height) && (sColumn < A.width) && (sRow < A.height))
	{
		B.elements[row * B.stride + col] = A.elements[sRow * A.stride + sColumn];
	}
}

// Matrix copy - Host code
void MatCopyFromFlat(const CudaMatrix & d_A, CudaMatrix & d_B, int startIndex, cudaStream_t &stream)
{
	// Invoke kernel
	dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
	dim3 dimGrid(d_B.width / BLOCK_SIZE + ((d_B.width % BLOCK_SIZE != 0) ? 1 : 0), d_B.height / BLOCK_SIZE + ((d_B.height % BLOCK_SIZE != 0) ? 1 : 0));
	MatCopyKernelFromFlat <<<dimGrid, dimBlock, 0, stream >>>(d_A, d_B, startIndex);
}

// Matrix rotate convolution kernel 180� 
__global__ void MatRot180Kernel(const CudaMatrix A, CudaMatrix B)
{
	// Each thread computes one element of B
	int col = blockIdx.x * blockDim.x + threadIdx.x;
	int row = blockIdx.y * blockDim.y + threadIdx.y;
	int sCol = threadIdx.x;
	int sRow = threadIdx.y;

	__shared__ volatile matrixFormat As[BLOCK_SIZE][BLOCK_SIZE];
	As[sRow][sCol] = GetElement(A, row, col);

	__syncthreads();

	if ((col < A.width) && (row < A.height))
	{
		B.elements[row * B.stride + col] = As[CONVOLUTION_ROWS - sRow - 1][CONVOLUTION_COLUMNS - sCol - 1];
	}
}

// Matrix rotate 180 - Host code
void MatRot180(const CudaMatrix & d_A, CudaMatrix & d_B, cudaStream_t &stream)
{
	// Invoke kernel
	dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
	dim3 dimGrid(d_B.width / BLOCK_SIZE + ((d_B.width % BLOCK_SIZE != 0) ? 1 : 0), d_B.height / BLOCK_SIZE + ((d_B.height % BLOCK_SIZE != 0) ? 1 : 0));
	MatRot180Kernel <<<dimGrid, dimBlock, 0, stream >>>(d_A, d_B);
}

// Matrix maxPooling kernel called by MatMaxPooling()
__global__ void MatMaxPoolingKernel(const CudaMatrix A, CudaMatrix B)
{
	// Each thread computes one element of B
	int col = blockIdx.x * blockDim.x + threadIdx.x;
	int row = blockIdx.y * blockDim.y + threadIdx.y;
	int sCol = threadIdx.x * 2;
	int sRow = threadIdx.y * 2;

	if ((col < B.width) && (row < B.height))
	{
		__shared__ volatile matrixFormat As[BLOCK_SIZE * 2][BLOCK_SIZE * 2];

		As[sRow][sCol] = GetElement(A, row * 2, col * 2);
		As[sRow][sCol + 1] = GetElement(A, row * 2, col * 2 + 1);
		As[sRow + 1][sCol] = GetElement(A, row * 2 + 1, col * 2);
		As[sRow + 1][sCol + 1] = GetElement(A, row * 2 + 1, col * 2 + 1);

		matrixFormat max = -1000000.0f;

		if (max < As[sRow][sCol])
			max = As[sRow][sCol];

		if (max < As[sRow][sCol + 1])
			max = As[sRow][sCol + 1];

		if (max < As[sRow + 1][sCol])
			max = As[sRow + 1][sCol];

		if (max < As[sRow + 1][sCol + 1])
			max = As[sRow + 1][sCol + 1];

		B.elements[row * B.stride + col] = max;
	}
}

// Matrix maxPooling - Host code
void MatMaxPooling(const CudaMatrix & d_A, CudaMatrix & d_B, cudaStream_t &stream)
{
	// Invoke kernel
	dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
	dim3 dimGrid(d_B.width / BLOCK_SIZE + ((d_B.width % BLOCK_SIZE != 0) ? 1 : 0), d_B.height / BLOCK_SIZE + ((d_B.height % BLOCK_SIZE != 0) ? 1 : 0));
	MatMaxPoolingKernel <<<dimGrid, dimBlock, 0, stream >>>(d_A, d_B);
}

// Matrix maxExpand kernel called by MatMaxExpand()
__global__ void MatMaxExpandKernel(const CudaMatrix A, const CudaMatrix B, CudaMatrix C)
{
	// Each thread computes one element of B
	int col = blockIdx.x * blockDim.x + threadIdx.x;
	int row = blockIdx.y * blockDim.y + threadIdx.y;
	int sCol = threadIdx.x;
	int sRow = threadIdx.y;
	matrixFormat val = 0.0f;

	if ((col < C.width) && (row < C.height))
	{
		int hCol = sCol / 2, hRow = sRow / 2;

		__shared__ volatile matrixFormat As[BLOCK_SIZE / 2][BLOCK_SIZE / 2];
		__shared__ volatile matrixFormat Bs[BLOCK_SIZE / 2][BLOCK_SIZE / 2];
		__shared__ volatile matrixFormat Cs[BLOCK_SIZE][BLOCK_SIZE];

		if (((row % 2) == 0) && ((col % 2) == 0))
		{
			As[hRow][hCol] = GetElement(A, row / 2, col / 2);
			Bs[hRow][hCol] = GetElement(B, row / 2, col / 2);
		}

		Cs[sRow][sCol] = GetElement(C, row, col);

		__syncthreads();

		if (Cs[sRow][sCol] == Bs[hRow][hCol])
			val = As[hRow][hCol];

		SetElement(C, row, col, val);
	}
}

// Matrix maxExpand - Host code; A input, B referenz, C in oldInput : out expandedError
void MatMaxExpand(const CudaMatrix & d_A, const CudaMatrix & d_B, CudaMatrix & d_C, cudaStream_t &stream)
{
	// Invoke kernel
	dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
	dim3 dimGrid(d_C.width / BLOCK_SIZE + ((d_C.width % BLOCK_SIZE != 0) ? 1 : 0), d_C.height / BLOCK_SIZE + ((d_C.height % BLOCK_SIZE != 0) ? 1 : 0));
	MatMaxExpandKernel <<<dimGrid, dimBlock, 0, stream >>>(d_A, d_B, d_C);
}

#define KERNEL_SIZE 3

// Matrix convolution kernel called by MatConvolution()
__global__ void MatConvolutionKernel(const CudaMatrix A, const CudaMatrix B, CudaMatrix C, int turnB)
{
	matrixFormat Cvalue = 0.0f;

	// Thread row and column within Block
	int sCol = threadIdx.x;
	int sRow = threadIdx.y;

	int col = blockIdx.x * blockDim.x + sCol;
	int row = blockIdx.y * blockDim.y + sRow;

	// Shared memory used to store Asub and Bsub respectively
	__shared__ volatile matrixFormat As[BLOCK_SIZE + (KERNEL_SIZE - 1)][BLOCK_SIZE + (KERNEL_SIZE - 1)];
	__shared__ volatile matrixFormat Bs[KERNEL_SIZE * KERNEL_SIZE];

	// Load Asub and Bsub from device memory to shared memory
	if ((col < A.width) && (row < A.height))
		As[sRow][sCol] = GetElement(A, row, col);

	// load two extra rows and columns
	if ((sRow < (KERNEL_SIZE - 1)) && ((row + BLOCK_SIZE) < A.height) && (col < A.width))
	{
		As[sRow + BLOCK_SIZE][sCol] = GetElement(A, row + BLOCK_SIZE, col);

		if ((sCol < (KERNEL_SIZE - 1)) && ((col + BLOCK_SIZE) < A.width) && (row < A.height))
			As[sRow + BLOCK_SIZE][sCol + BLOCK_SIZE] = GetElement(A, row + BLOCK_SIZE, col + BLOCK_SIZE);
	}

	if ((sCol < (KERNEL_SIZE - 1)) && ((col + BLOCK_SIZE) < A.width) && (row < A.height))
		As[sRow][sCol + BLOCK_SIZE] = GetElement(A, row, col + BLOCK_SIZE);

	if ((sCol < KERNEL_SIZE) && (sRow < KERNEL_SIZE))
	{
		if (turnB == 0)
			Bs[sCol + sRow * KERNEL_SIZE] = GetElement(B, (KERNEL_SIZE - 1) - sRow, (KERNEL_SIZE - 1) - sCol);
		else
			Bs[sCol + sRow * KERNEL_SIZE] = GetElement(B, sRow, sCol);
	}

	__syncthreads();

	if ((col < (C.width - 2)) && (row < (C.height - 2)))
	{
		int sCol1 = sCol + 1, sCol2 = sCol + 2;

		Cvalue += As[sRow][sCol] * Bs[0];
		Cvalue += As[sRow][sCol1] * Bs[1];
		Cvalue += As[sRow][sCol2] * Bs[2];
		++sRow;
		Cvalue += As[sRow][sCol] * Bs[3];
		Cvalue += As[sRow][sCol1] * Bs[4];
		Cvalue += As[sRow][sCol2] * Bs[5];
		++sRow;
		Cvalue += As[sRow][sCol] * Bs[6];
		Cvalue += As[sRow][sCol1] * Bs[7];
		Cvalue += As[sRow][sCol2] * Bs[8];

		SetElement(C, row + 1, col + 1, Cvalue);
	}
}

// Matrix multiplication - Host code
void MatConvolution(const CudaMatrix & d_A, const CudaMatrix & d_B, CudaMatrix & d_C, int turnB, cudaStream_t &stream )
{
	// Invoke kernel
	dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
	dim3 dimGrid(d_C.width / BLOCK_SIZE + ((d_C.width % BLOCK_SIZE != 0) ? 1 : 0), d_C.height / BLOCK_SIZE + ((d_C.height % BLOCK_SIZE != 0) ? 1 : 0));
	MatConvolutionKernel <<<dimGrid, dimBlock, 0, stream >>>(d_A, d_B, d_C, turnB);
}

#define ERROR_SUM_SPLIT 16
#define WARP_SIZE		32
#define LOAD_SIZE (WARP_SIZE / sizeof(matrixFormat))

__global__ void MatConvolutionErrorSummKernel(const CudaMatrix A, const CudaMatrix B, matrixFormat lRate)
{
	matrixFormat Bvalue = 0.0f;

	// Thread row and column within Block
	int index = threadIdx.x;
	int split = threadIdx.y;
	int row = index / CONVOLUTION_COLUMNS;
	int startRow = row + 1;
	int stopRow = startRow + (A.height - (CONVOLUTION_ROWS + 1));
	int column = index % CONVOLUTION_COLUMNS;
	int startColumn = column + 1;
	int stopColumn = startColumn + (A.width - (CONVOLUTION_COLUMNS + 1));

	// Shared memory used to store Asub and Bsub respectively
	__shared__ volatile matrixFormat As[ERROR_SUM_SPLIT][MAX_CONVOLUTION_WIDTH];
	__shared__ volatile matrixFormat Bs[ERROR_SUM_SPLIT][CONVOLUTION_COLUMNS * CONVOLUTION_ROWS];

	int partSize = ( A.width / ( BLOCK_SIZE * LOAD_SIZE ) + 1 ) * LOAD_SIZE;
	int startIndex = index * partSize;
	int stopIndex = (index + 1) * partSize;
	int lastHeight = A.height / ERROR_SUM_SPLIT + 1;

	for (int k = 0; k < lastHeight; ++k)
	{
		int y = k * ERROR_SUM_SPLIT + split;

		if (y < A.height)
		{
			for (int x = startIndex; x < stopIndex; ++x)
			{
				// Load Asub from device memory to shared memory
				// Each thread loads one of nine elements of each row
				if (x < A.width)
					As[split][x] = GetElement(A, y, x);
			}
		}

		__syncthreads();
		
		if ((y >= startRow) && (y < stopRow))
		{
			for (int x = startColumn; x < stopColumn; ++x)
			{
				Bvalue += As[split][x];
			}
		}
	}
	
	if ( index < (CONVOLUTION_COLUMNS * CONVOLUTION_ROWS))
		Bs[split][index] = Bvalue;

	__syncthreads();

	if ((split == 0) && (index < CONVOLUTION_COLUMNS * CONVOLUTION_ROWS))
	{
		Bvalue = Bs[0][index];

		for ( int j = 1; j < ERROR_SUM_SPLIT; ++j)
			Bvalue += Bs[j][index];
			
		SetElement(B, row, column, Bvalue * lRate);
	}
}

void MatConvolutionErrorSumm(const CudaMatrix & d_A, CudaMatrix & d_B, matrixFormat lRate, cudaStream_t &stream)
{
	// Invoke kernel, run nine threads per Block to build summ over matrix 
	dim3 dimBlock1(BLOCK_SIZE, ERROR_SUM_SPLIT);
	dim3 dimGrid1(1, 1);
	MatConvolutionErrorSummKernel <<<dimGrid1, dimBlock1, 0, stream >>>(d_A, d_B, lRate);
}
//

void MatPredictDense(CudaMatrix & inputBuffer, CudaMatrix & inputSignal, CudaMatrix & signals, CudaMatrix & weights, CudaMatrix & outputSignal)
{
	CudaMatrix d_A;
	CudaMatrix d_B;
	CudaMatrix d_C;
	cudaStream_t stream = NULL;

#ifdef USE_SEPARATE_STREAM
	cudaStreamCreate(&stream);
#endif

	// Invoke kernel
	// inputBuffer ist transponiertes inputSignal
	d_A = inputSignal;
	d_B = inputBuffer;
	dim3 dimBlock1(BLOCK_SIZE, BLOCK_SIZE);
	dim3 dimGrid1(d_B.width / BLOCK_SIZE + ((d_B.width % BLOCK_SIZE != 0) ? 1 : 0), d_B.height / BLOCK_SIZE + ((d_B.height % BLOCK_SIZE != 0) ? 1 : 0));
	MatCopyKernel <<<dimGrid1, dimBlock1, 0, stream >>>(d_A, d_B);

	// Invoke kernel
	d_A = weights;
	d_B = inputBuffer;
	d_C = outputSignal;
	dim3 dimBlock2(BLOCK_SIZE, BLOCK_SIZE);
	dim3 dimGrid2(d_B.width / BLOCK_SIZE + ((d_B.width % BLOCK_SIZE != 0) ? 1 : 0), d_A.height / BLOCK_SIZE + ((d_A.height % BLOCK_SIZE != 0) ? 1 : 0));
	MatMulKernelWithActivation <<<dimGrid2, dimBlock2, 0, stream >>>(d_A, d_B, d_C);

	// Invoke kernel
	/*
	d_A = signals;
	d_B = outputSignal;
	dim3 dimBlock3(BLOCK_SIZE, BLOCK_SIZE);
	dim3 dimGrid3(d_A.width / BLOCK_SIZE + ((d_A.width % BLOCK_SIZE != 0) ? 1 : 0), d_A.height / BLOCK_SIZE + ((d_A.height % BLOCK_SIZE != 0) ? 1 : 0));
	MatTanhKernel <<<dimGrid3, dimBlock3, 0, stream >>>(d_A, d_B);
	*/
	/*
	predictParams.inputBuffer->Copy(predictParams.inputSignal);							// add bais if used
	predictParams.signals->Dot(predictParams.weights, predictParams.inputBuffer, 1.0);
	predictParams.outputSignal->Activation(predictParams.signals);
	*/

#ifdef USE_SEPARATE_STREAM
	cudaStreamSynchronize(stream);

	cudaStreamDestroy(stream);
#endif
}

void MatBackPropagationDense(CudaMatrix & errorPreviousLayer, CudaMatrix & weights, CudaMatrix & errorCurrentLayer, CudaMatrix & errorMatrix, CudaMatrix & outputCurrentLayer, CudaMatrix & delta, CudaMatrix & outputPreviousLayer, matrixFormat lRate)
{
	CudaMatrix d_A;
	CudaMatrix d_B;
	CudaMatrix d_C;
	cudaStream_t stream = NULL;

#ifdef USE_SEPARATE_STREAM
	cudaStreamCreate(&stream);
#endif

	if (errorPreviousLayer.elements != NULL)
	{
		// Invoke kernel
		d_A = weights;
		d_B = errorCurrentLayer;
		d_C = errorPreviousLayer;
		dim3 dimBlock1(BLOCK_SIZE, BLOCK_SIZE);
		dim3 dimGrid1(d_B.width / BLOCK_SIZE + ((d_B.width % BLOCK_SIZE != 0) ? 1 : 0), d_A.width / BLOCK_SIZE + ((d_A.width % BLOCK_SIZE != 0) ? 1 : 0));
		MatMulKernelT_A <<<dimGrid1, dimBlock1, 0, stream >>>(d_A, d_B, d_C, 1.0);
	}

	// Invoke kernel
	d_A = outputCurrentLayer;
	d_B = errorCurrentLayer;
	d_C = errorMatrix;
	// Invoke kernel
	dim3 dimBlock3(BLOCK_SIZE, BLOCK_SIZE);
	dim3 dimGrid3(d_A.width / BLOCK_SIZE + ((d_A.width % BLOCK_SIZE != 0) ? 1 : 0), d_A.height / BLOCK_SIZE + ((d_A.height % BLOCK_SIZE != 0) ? 1 : 0));
	MatGradientTanhKernel <<<dimGrid3, dimBlock3, 0, stream >>>(d_A, d_B, d_C);

	// Invoke kernel
	d_A = errorMatrix;
	d_B = outputPreviousLayer;
	d_C = delta;
	// Invoke kernel
	dim3 dimBlock4(BLOCK_SIZE, BLOCK_SIZE);
	dim3 dimGrid4(d_B.height / BLOCK_SIZE + ((d_B.height % BLOCK_SIZE != 0) ? 1 : 0), d_A.height / BLOCK_SIZE + ((d_A.height % BLOCK_SIZE != 0) ? 1 : 0));
	MatMulKernelT_B <<<dimGrid4, dimBlock4, 0, stream >>>(d_A, d_B, d_C, lRate);

	// Invoke kernel
	d_A = delta;
	d_B = weights;
	// Invoke kernel
	dim3 dimBlock5(BLOCK_SIZE, BLOCK_SIZE);
	dim3 dimGrid5(d_A.width / BLOCK_SIZE + ((d_A.width % BLOCK_SIZE != 0) ? 1 : 0), d_A.height / BLOCK_SIZE + ((d_A.height % BLOCK_SIZE != 0) ? 1 : 0));
	MatAddKernel <<<dimGrid5, dimBlock5, 0, stream >>>(d_A, d_B);

	/*
	// errorPreviousLayer has one row less than weights columns ( last column is added for bais )
	// it works because k is set by matrix B and lma is set from matrix A.column in gemm
	if (backPropagationParams.errorPreviousLayer != NULL)
	backPropagationParams.errorPreviousLayer->Dot_aT(backPropagationParams.weights, backPropagationParams.errorCurrentLayer, 1.0);

	backPropagationParams.errorMatrix->Gradient(backPropagationParams.outputCurrentLayer, backPropagationParams.errorCurrentLayer);

	backPropagationParams.delta->Dot_bT(backPropagationParams.errorMatrix, backPropagationParams.outputPreviousLayer, backPropagationParams.lRate);
	backPropagationParams.weights->Add(backPropagationParams.delta);
	*/
#ifdef USE_SEPARATE_STREAM
	cudaStreamSynchronize(stream);

	cudaStreamDestroy(stream);
#endif
}

void MatBackPropagationConvolution(CudaMatrix & errorPreviousLayer, CudaMatrix & weights, CudaMatrix & errorCurrentLayer, CudaMatrix & errorMatrix, CudaMatrix & outputCurrentLayer, CudaMatrix & delta, CudaMatrix & outputPreviousLayer, matrixFormat lRate, ActivationType activation)
{
	CudaMatrix d_A;
	CudaMatrix d_B;
	CudaMatrix d_C;
	cudaStream_t stream = NULL;

#ifdef USE_SEPARATE_STREAM
	cudaStreamCreate(&stream);
#endif

	if (errorPreviousLayer.elements != NULL)
	{
		// Invoke kernel
		d_A = errorCurrentLayer;
		d_B = weights;
		d_C = errorPreviousLayer;
		dim3 dimBlock1(BLOCK_SIZE, BLOCK_SIZE);
		dim3 dimGrid1(d_C.width / BLOCK_SIZE + ((d_C.width % BLOCK_SIZE != 0) ? 1 : 0), d_C.height / BLOCK_SIZE + ((d_C.height % BLOCK_SIZE != 0) ? 1 : 0));
		MatConvolutionKernel <<<dimGrid1, dimBlock1, 0, stream >>>(d_A, d_B, d_C, 1);
	}

	if (lRate != 0)
	{
		// Invoke kernel
		d_A = outputCurrentLayer;
		d_B = errorCurrentLayer;
		d_C = errorMatrix;
		// Invoke kernel
		dim3 dimBlock2(BLOCK_SIZE, BLOCK_SIZE);
		dim3 dimGrid2(d_A.width / BLOCK_SIZE + ((d_A.width % BLOCK_SIZE != 0) ? 1 : 0), d_A.height / BLOCK_SIZE + ((d_A.height % BLOCK_SIZE != 0) ? 1 : 0));
		
		if ( activation == ActivationType::ActivationTypeTanh)
			MatGradientTanhKernel <<<dimGrid2, dimBlock2, 0, stream >>> (d_A, d_B, d_C);

		if (activation == ActivationType::ActivationTypeRelu)
			MatGradientReluKernel <<<dimGrid2, dimBlock2, 0, stream >>> (d_A, d_B, d_C);

		// Invoke kernel
		d_A = outputPreviousLayer;
		d_B = errorMatrix;
		// Invoke kernel
		dim3 dimBlock3(BLOCK_SIZE, BLOCK_SIZE);
		dim3 dimGrid3(d_A.width / BLOCK_SIZE + ((d_A.width % BLOCK_SIZE != 0) ? 1 : 0), d_A.height / BLOCK_SIZE + ((d_A.height % BLOCK_SIZE != 0) ? 1 : 0));
		MatMulEKernel <<<dimGrid3, dimBlock3, 0, stream >>>(d_A, d_B);

		d_A = errorMatrix;
		d_B = delta;
		MatConvolutionErrorSumm(d_A, d_B, lRate, stream);

		// Invoke kernel
		d_A = delta;
		d_B = weights;
		// Invoke kernel
		dim3 dimBlock5(BLOCK_SIZE, BLOCK_SIZE);
		dim3 dimGrid5(d_A.width / BLOCK_SIZE + ((d_A.width % BLOCK_SIZE != 0) ? 1 : 0), d_A.height / BLOCK_SIZE + ((d_A.height % BLOCK_SIZE != 0) ? 1 : 0));
		MatAddKernel <<<dimGrid5, dimBlock5, 0, stream >>> (d_A, d_B);
	}

#ifdef USE_SEPARATE_STREAM
	cudaStreamSynchronize(stream);

	cudaStreamDestroy(stream);
#endif
}

void MatBackPropagationPooling(CudaMatrix & errorPreviousLayer, CudaMatrix & errorCurrentLayer, CudaMatrix & outputCurrentLayer, CudaMatrix & outputPreviousLayer)
{
	CudaMatrix d_A;
	CudaMatrix d_B;
	CudaMatrix d_C;
	cudaStream_t stream = NULL;

#ifdef USE_SEPARATE_STREAM
	cudaStreamCreate(&stream);
#endif

	// Invoke kernel
	d_A = outputPreviousLayer;
	d_B = errorPreviousLayer;
	dim3 dimBlock1(BLOCK_SIZE, BLOCK_SIZE);
	dim3 dimGrid1(d_A.width / BLOCK_SIZE + ((d_A.width % BLOCK_SIZE != 0) ? 1 : 0), d_A.height / BLOCK_SIZE + ((d_A.height % BLOCK_SIZE != 0) ? 1 : 0));
	MatCopyKernel <<<dimGrid1, dimBlock1, 0, stream >>>(d_A, d_B);

	// Invoke kernel
	d_A = errorCurrentLayer;
	d_B = outputCurrentLayer;
	d_C = errorPreviousLayer;
	dim3 dimBlock2(BLOCK_SIZE, BLOCK_SIZE);
	dim3 dimGrid2(d_C.width / BLOCK_SIZE + ((d_C.width % BLOCK_SIZE != 0) ? 1 : 0), d_C.height / BLOCK_SIZE + ((d_C.height % BLOCK_SIZE != 0) ? 1 : 0));
	MatMaxExpandKernel <<<dimGrid2, dimBlock2, 0, stream >>>(d_A, d_B, d_C);

	/*
	// mit input initialisieren
	backPropagationParams.errorPreviousLayer->Copy(backPropagationParams.outputPreviousLayer);
	// mit fehler oder 0 f�llem
	backPropagationParams.errorPreviousLayer->MaxExpand(backPropagationParams.errorCurrentLayer, backPropagationParams.outputCurrentLayer);
	*/

#ifdef USE_SEPARATE_STREAM
	cudaStreamSynchronize(stream);

	cudaStreamDestroy(stream);
#endif
}

