// IfMatrix.cpp : Definiert die exportierten Funktionen f�r die DLL-Anwendung.
// approx. 1 ms to launch a cernel
//

#define CL_IF_MATRIX
#define USE_CUDA

#include <math.h>
#include <memory.h>
#include "..\..\IfMatrix\IfMatrix.hpp"

#include <mkl.h>

#include "cuda_runtime.h"

extern void MatMul(const CudaMatrix &, const CudaMatrix &, CudaMatrix &, matrixFormat, cudaStream_t &stream);
extern void MatMulT_A(const CudaMatrix &, const CudaMatrix &, CudaMatrix &, matrixFormat, cudaStream_t &stream);
extern void MatMulT_B(const CudaMatrix &, const CudaMatrix &, CudaMatrix &, matrixFormat, cudaStream_t &stream);
extern void MatActivationTanh(const CudaMatrix & d_A, CudaMatrix & d_B, cudaStream_t &stream);
extern void MatActivationRelu(const CudaMatrix & d_A, CudaMatrix & d_B, cudaStream_t &stream);
extern void MatGradientTanh(const CudaMatrix & d_A, CudaMatrix & d_B, CudaMatrix & d_C, cudaStream_t &stream);
extern void MatGradientRelu(const CudaMatrix & d_A, CudaMatrix & d_B, CudaMatrix & d_C, cudaStream_t &stream);
extern void MatAdd(const CudaMatrix & d_A, CudaMatrix & d_B, cudaStream_t &stream);
extern void MatSub(const CudaMatrix & d_A, CudaMatrix & d_B, cudaStream_t &stream);
extern void MatMulE(const CudaMatrix & d_A, CudaMatrix & d_B, cudaStream_t &stream);
extern void MatCopy(const CudaMatrix & d_A, CudaMatrix & d_B, cudaStream_t &stream);
extern void MatCopyToFlat(const CudaMatrix & d_A, CudaMatrix & d_B, int startIndex, cudaStream_t &stream);
extern void MatCopyFromFlat(const CudaMatrix & d_A, CudaMatrix & d_B, int startIndex, cudaStream_t &stream);
extern void MatRot180(const CudaMatrix & d_A, CudaMatrix & d_B, cudaStream_t &stream);
extern void MatMaxPooling(const CudaMatrix & d_A, CudaMatrix & d_B, cudaStream_t &stream);
extern void MatMaxExpand(const CudaMatrix & d_A, const CudaMatrix & d_B, CudaMatrix & d_C, cudaStream_t &stream);
extern void MatConvolution(const CudaMatrix & d_A, const CudaMatrix & d_B, CudaMatrix & d_C, int turnB, cudaStream_t &stream);
extern void MatConvolutionErrorSumm(const CudaMatrix & d_A, CudaMatrix & d_B, matrixFormat lRate, cudaStream_t &stream);
extern void MatPredictDense(CudaMatrix & inputBuffer, CudaMatrix & inputSignal, CudaMatrix & signals, CudaMatrix & weights, CudaMatrix & outputSignal);
extern void MatBackPropagationDense( CudaMatrix & errorPreviousLayer, CudaMatrix & weights, CudaMatrix & errorCurrentLayer, CudaMatrix & errorMatrix, CudaMatrix & outputCurrentLayer, CudaMatrix & delta, CudaMatrix & outputPreviousLayer, matrixFormat lRate);
extern void	MatBackPropagationConvolution(CudaMatrix & errorPreviousLayer, CudaMatrix & weights, CudaMatrix & errorCurrentLayer, CudaMatrix & errorMatrix, CudaMatrix & outputCurrentLayer, CudaMatrix & delta, CudaMatrix & outputPreviousLayer, matrixFormat lRate, ActivationType activation);
extern void	MatBackPropagationPooling(CudaMatrix & errorPreviousLayer, CudaMatrix & errorCurrentLayer, CudaMatrix & outputCurrentLayer, CudaMatrix & outputPreviousLayer);

class PtMatrix : public IfMatrix
{
	int columns;
	int rows;
	size_t pitch;
	int stride;
	matrixFormat * matrixData;
	static int seed;
	int instance;
	static cudaStream_t stream;
	static int globalInstances;

public:

	matrixFormat * Buffer(void)
	{
		return(matrixData);
	}

	PtMatrix(void)
	{
		rows = 0;
		columns = 0;
		matrixData = NULL;
		instance = 0;

#ifdef USE_SEPARATE_STREAM
		if (stream == NULL )
		{
			if (cudaStreamCreate(&stream) != cudaSuccess)
			{
				stream = NULL;
			}
		}
#endif
		++globalInstances;
	}

	PtMatrix(int _rows, int _columns)
	{
		rows = _rows;
		columns = _columns;

		cudaMallocPitch(&matrixData, &pitch, columns * sizeof(matrixFormat), rows);
		stride = (int)(pitch / sizeof(matrixFormat));

		instance = 0;

#ifdef USE_SEPARATE_STREAM
		if (stream == NULL)
		{
			if (cudaStreamCreate(&stream) != cudaSuccess)
			{
				stream = NULL;
			}
		}
#endif
		++globalInstances;

		matrixFormat * tempData = (matrixFormat *)malloc(rows * columns * sizeof(matrixFormat));

		for (int count = 0; count < (rows * columns); ++count)
		{
			tempData[count] = 0.0;
		}

		cudaMemcpy2D(matrixData, pitch, tempData, columns * sizeof(matrixFormat), columns * sizeof(matrixFormat), rows, cudaMemcpyHostToDevice);

		free(tempData);
	}

	PtMatrix(int _rows, int _columns, matrixFormat * data)
	{
		rows = _rows;
		columns = _columns;

		cudaMallocPitch(&matrixData, &pitch, columns * sizeof(matrixFormat), rows);
		stride = (int)(pitch / sizeof(matrixFormat));

		instance = 0;

#ifdef USE_SEPARATE_STREAM
		if (stream == NULL)
		{
			if (cudaStreamCreate(&stream) != cudaSuccess)
			{
				stream = NULL;
			}
		}
#endif
		++globalInstances;

		cudaMemcpy2D(matrixData, pitch, data, columns * sizeof(matrixFormat), columns * sizeof(matrixFormat), rows, cudaMemcpyHostToDevice);
	}

	~PtMatrix(void)
	{
		cudaFree(matrixData);

		--globalInstances;

#ifdef USE_SEPARATE_STREAM
		if (globalInstances == 0)
		{
			cudaStreamDestroy(stream);
			stream = NULL;
		}
#endif
	}

	void __stdcall DuplicateObject(void)
	{
		++instance;
	}

	void __stdcall ReleaseObject(void)
	{
		if (--instance == 0)
			delete this;
	}

	void InitWithRandomValues(bool lastColumnIsBais)
	{
		VSLStreamStatePtr randomStream;
		matrixFormat range = (matrixFormat)(1 / sqrt(rows * columns));
		int stride = columns;
		int count = rows * (columns - (lastColumnIsBais ? 1 : 0));
		int * buff = new int[count];

		if (buff != NULL)
		{
			if (VSL_STATUS_OK == vslNewStream(&randomStream, VSL_BRNG_R250, seed))
			{
				viRngUniform(VSL_RNG_METHOD_UNIFORM_STD, randomStream, count, buff, 0, 16777216);

				count = 0;

				matrixFormat * tempData = (matrixFormat *)malloc(rows * columns * sizeof(matrixFormat));

				for (int row = 0; row < rows; ++row)
				{
					int column;

					for (column = 0; column < (columns - (lastColumnIsBais ? 1 : 0)); ++column)
					{
						tempData[column + row * stride] = (matrixFormat)(buff[count++] * 2 * range / 16777216.0 - range);
					}

					if (lastColumnIsBais)
						tempData[column + row * stride] = 1.0f;
				}

				cudaMemcpy2D(matrixData, pitch, tempData, columns * sizeof(matrixFormat), columns * sizeof(matrixFormat), rows, cudaMemcpyHostToDevice);

				free(tempData);

				seed = buff[count - 1];

				vslDeleteStream(&randomStream);
			}

			delete[] buff;
		}
	}

	void InitWithZero(void)
	{
		cudaMemset2D(matrixData, pitch, 0, columns * sizeof(matrixFormat), rows);
	}
	
	void Dot(IfMatrix * _matrixA, IfMatrix * _matrixB, matrixFormat alpha)
	{
		PtMatrix * matrixA = (PtMatrix*)_matrixA;
		PtMatrix * matrixB = (PtMatrix*)_matrixB;
		CudaMatrix d_A, d_B, d_C;

		d_A.width = matrixA->columns;
		d_A.height = matrixA->rows;
		d_A.stride = matrixA->stride;
		d_A.elements = matrixA->Buffer();
			
		d_B.width = matrixB->columns;
		d_B.height = matrixB->rows;
		d_B.stride = matrixB->stride;
		d_B.elements = matrixB->Buffer();

		d_C.width = columns;
		d_C.height = rows;
		d_C.stride = stride;
		d_C.elements = matrixData;

		int m = matrixA->rows;					// A.rows and C.rows
		int n = matrixB->columns;				// B.columns and C.columns
		int k = matrixB->rows;					// A.colums and B.rows

		if ((m == rows) && (n == columns) && (k == matrixA->columns))
			MatMul(d_A, d_B, d_C, alpha, stream);
	}
	
	void Dot_aT(IfMatrix * _matrixA, IfMatrix * _matrixB, matrixFormat alpha)
	{
		PtMatrix * matrixA = (PtMatrix*)_matrixA;
		PtMatrix * matrixB = (PtMatrix*)_matrixB;
		CudaMatrix d_A, d_B, d_C;

		d_A.width = matrixA->columns;
		d_A.height = matrixA->rows;
		d_A.stride = matrixA->stride;
		d_A.elements = matrixA->Buffer();

		d_B.width = matrixB->columns;
		d_B.height = matrixB->rows;
		d_B.stride = matrixB->stride;
		d_B.elements = matrixB->Buffer();

		d_C.width = columns;
		d_C.height = rows;
		d_C.stride = stride;
		d_C.elements = matrixData;

		int m = (matrixA->columns > rows) ? rows : matrixA->columns;	// A.rows and C.rows, exception to exclude bais from backprop
		int n = matrixB->columns;										// B.columns and C.columns
		int k = matrixB->rows;											// A.colums and B.rows

		if ((m == rows) && (n == columns) && (k == matrixA->rows))
			MatMulT_A(d_A, d_B, d_C, alpha, stream);
	}

	void Dot_bT(IfMatrix * _matrixA, IfMatrix * _matrixB, matrixFormat alpha)
	{
		PtMatrix * matrixA = (PtMatrix*)_matrixA;
		PtMatrix * matrixB = (PtMatrix*)_matrixB;
		CudaMatrix d_A, d_B, d_C;

		d_A.width = matrixA->columns;
		d_A.height = matrixA->rows;
		d_A.stride = matrixA->stride;
		d_A.elements = matrixA->Buffer();

		d_B.width = matrixB->columns;
		d_B.height = matrixB->rows;
		d_B.stride = matrixB->stride;
		d_B.elements = matrixB->Buffer();

		d_C.width = columns;
		d_C.height = rows;
		d_C.stride = stride;
		d_C.elements = matrixData;

		int m = matrixA->rows;					// A.rows and C.rows
		int n = matrixB->rows;					// B.columns and C.columns
		int k = matrixB->columns;				// A.colums and B.rows

		if ((m == rows) && (n == columns) && (k == matrixA->columns))
			MatMulT_B(d_A, d_B, d_C, alpha, stream);
	}
	
	void ActivationTanh(IfMatrix * _matrixA)
	{
		PtMatrix * matrixA = (PtMatrix*)_matrixA;

		if ((matrixA->rows == rows) && (matrixA->columns == columns))
		{
			CudaMatrix d_A, d_B;

			d_A.width = matrixA->columns;
			d_A.height = matrixA->rows;
			d_A.stride = matrixA->stride;
			d_A.elements = matrixA->Buffer();

			d_B.width = columns;
			d_B.height = rows;
			d_B.stride = stride;
			d_B.elements = matrixData;

			MatActivationTanh(d_A, d_B, stream);
		}
	}

	void ActivationRelu(IfMatrix * _matrixA)
	{
		PtMatrix * matrixA = (PtMatrix*)_matrixA;

		if ((matrixA->rows == rows) && (matrixA->columns == columns))
		{
			CudaMatrix d_A, d_B;

			d_A.width = matrixA->columns;
			d_A.height = matrixA->rows;
			d_A.stride = matrixA->stride;
			d_A.elements = matrixA->Buffer();

			d_B.width = columns;
			d_B.height = rows;
			d_B.stride = stride;
			d_B.elements = matrixData;

			// �ndern
			MatActivationRelu(d_A, d_B, stream);
		}
	}

	void GradientTanh(IfMatrix * _matrixA, IfMatrix * _matrixB)
	{
		PtMatrix * matrixA = (PtMatrix*)_matrixA;
		PtMatrix * matrixB = (PtMatrix*)_matrixB;

		if ((matrixA->rows == rows) && (matrixA->columns == columns) && (matrixB->rows == rows) && (matrixB->columns == columns))
		{
			CudaMatrix d_A, d_B, d_C;

			d_A.width = matrixA->columns;
			d_A.height = matrixA->rows;
			d_A.stride = matrixA->stride;
			d_A.elements = matrixA->Buffer();

			d_B.width = matrixB->columns;
			d_B.height = matrixB->rows;
			d_B.stride = matrixB->stride;
			d_B.elements = matrixB->Buffer();

			d_C.width = columns;
			d_C.height = rows;
			d_C.stride = stride;
			d_C.elements = matrixData;

			MatGradientTanh(d_A, d_B, d_C, stream);
		}
	}

	void GradientRelu(IfMatrix * _matrixA, IfMatrix * _matrixB)
	{
		PtMatrix * matrixA = (PtMatrix*)_matrixA;
		PtMatrix * matrixB = (PtMatrix*)_matrixB;

		if ((matrixA->rows == rows) && (matrixA->columns == columns) && (matrixB->rows == rows) && (matrixB->columns == columns))
		{
			CudaMatrix d_A, d_B, d_C;

			d_A.width = matrixA->columns;
			d_A.height = matrixA->rows;
			d_A.stride = matrixA->stride;
			d_A.elements = matrixA->Buffer();

			d_B.width = matrixB->columns;
			d_B.height = matrixB->rows;
			d_B.stride = matrixB->stride;
			d_B.elements = matrixB->Buffer();

			d_C.width = columns;
			d_C.height = rows;
			d_C.stride = stride;
			d_C.elements = matrixData;

			MatGradientRelu(d_A, d_B, d_C, stream);
		}
	}

	void Add(IfMatrix * _matrixA)
	{
		PtMatrix * matrixA = (PtMatrix*)_matrixA;

		if ((matrixA->rows == rows) && (matrixA->columns == columns))
		{
			CudaMatrix d_A, d_B;

			d_A.width = matrixA->columns;
			d_A.height = matrixA->rows;
			d_A.stride = matrixA->stride;
			d_A.elements = matrixA->Buffer();

			d_B.width = columns;
			d_B.height = rows;
			d_B.stride = stride;
			d_B.elements = matrixData;

			MatAdd(d_A, d_B, stream);
		}
	}

	void Subtract(IfMatrix * _matrixA)
	{
		PtMatrix * matrixA = (PtMatrix*)_matrixA;

		if ((matrixA->rows == rows) && (matrixA->columns == columns))
		{
			CudaMatrix d_A, d_B;

			d_A.width = matrixA->columns;
			d_A.height = matrixA->rows;
			d_A.stride = matrixA->stride;
			d_A.elements = matrixA->Buffer();

			d_B.width = columns;
			d_B.height = rows;
			d_B.stride = stride;
			d_B.elements = matrixData;

			MatSub(d_A, d_B, stream);
		}
	}

	void Multiply(IfMatrix * _matrixA)
	{
		PtMatrix * matrixA = (PtMatrix*)_matrixA;

		if ((matrixA->rows == rows) && (matrixA->columns == columns))
		{
			CudaMatrix d_A, d_B;

			d_A.width = matrixA->columns;
			d_A.height = matrixA->rows;
			d_A.stride = matrixA->stride;
			d_A.elements = matrixA->Buffer();

			d_B.width = columns;
			d_B.height = rows;
			d_B.stride = stride;
			d_B.elements = matrixData;

			MatMulE(d_A, d_B, stream);
		}
	}

	void Copy(IfMatrix * _matrixA)
	{
		PtMatrix * matrixA = (PtMatrix*)_matrixA;
		CudaMatrix d_A, d_B;

		d_A.width = matrixA->columns;
		d_A.height = matrixA->rows;
		d_A.stride = matrixA->stride;
		d_A.elements = matrixA->Buffer();

		d_B.width = columns;
		d_B.height = rows;
		d_B.stride = stride;
		d_B.elements = matrixData;

		MatCopy(d_A, d_B, stream);
	}

	// used toFlat
	void Copy(int startIndex, IfMatrix * _matrixA)
	{
		PtMatrix * matrixA = (PtMatrix*)_matrixA;
		CudaMatrix d_A, d_B;

		d_A.width = matrixA->columns;
		d_A.height = matrixA->rows;
		d_A.stride = matrixA->stride;
		d_A.elements = matrixA->Buffer();

		d_B.width = columns;
		d_B.height = rows;
		d_B.stride = stride;
		d_B.elements = matrixData;

		MatCopyToFlat(d_A, d_B, startIndex, stream);
	}

	// used fromFlat
	void Copy(IfMatrix * _matrixA, int sourceIndex)
	{
		PtMatrix * matrixA = (PtMatrix*)_matrixA;
		CudaMatrix d_A, d_B;

		d_A.width = matrixA->columns;
		d_A.height = matrixA->rows;
		d_A.stride = matrixA->stride;
		d_A.elements = matrixA->Buffer();

		d_B.width = columns;
		d_B.height = rows;
		d_B.stride = stride;
		d_B.elements = matrixData;

		MatCopyFromFlat(d_A, d_B, sourceIndex, stream);
	}

	void Rotate180(IfMatrix * _matrixA)
	{
		PtMatrix * matrixA = (PtMatrix*)_matrixA;
		CudaMatrix d_A, d_B;

		d_A.width = matrixA->columns;
		d_A.height = matrixA->rows;
		d_A.stride = matrixA->stride;
		d_A.elements = matrixA->Buffer();

		d_B.width = columns;
		d_B.height = rows;
		d_B.stride = stride;
		d_B.elements = matrixData;

		MatRot180(d_A, d_B, stream);
	}

	matrixFormat SquareSumm(void)
	{
		matrixFormat a, summ = 0;

		matrixFormat * sData, * tempData = (matrixFormat *)malloc(rows * columns * sizeof(matrixFormat));

		sData = tempData;

		GetBuffer(sData, rows * columns * sizeof(matrixFormat));

		for (int count = 0; count < (rows * columns); ++count)
		{
			a = *sData++;
			summ += a * a;
		}

		free(tempData);

		return(summ);
	}

	void Convolution(IfMatrix * _weights, IfMatrix * _inputMatrix, ConvolutionInfo * convolutionInfo)
	{
		PtMatrix * weights = (PtMatrix*)_weights;
		PtMatrix * inputMatrix = (PtMatrix*)_inputMatrix;
		CudaMatrix d_A, d_B, d_C;

		d_A.width = inputMatrix->columns;
		d_A.height = inputMatrix->rows;
		d_A.stride = inputMatrix->stride;
		d_A.elements = inputMatrix->Buffer();

		d_B.width = weights->columns;
		d_B.height = weights->rows;
		d_B.stride = weights->stride;
		d_B.elements = weights->Buffer();

		d_C.width = columns;
		d_C.height = rows;
		d_C.stride = stride;
		d_C.elements = matrixData;

		if ((d_B.width == 3) && (d_B.height == 3))
			MatConvolution(d_A, d_B, d_C, 0, stream);
	}

	void ConvolutionErrorSumm(IfMatrix * _errorMatrix, float lRate)
	{
		PtMatrix * errorMatrix = (PtMatrix*)_errorMatrix;
		CudaMatrix d_A, d_B;

		d_A.width = errorMatrix->columns;
		d_A.height = errorMatrix->rows;
		d_A.stride = errorMatrix->stride;
		d_A.elements = errorMatrix->Buffer();

		d_B.width = columns;
		d_B.height = rows;
		d_B.stride = stride;
		d_B.elements = matrixData;

		MatConvolutionErrorSumm(d_A, d_B, lRate, stream);
	}

	void MaxPooling(IfMatrix * _input)
	{
		PtMatrix * input = (PtMatrix*)_input;

		if ((input->Columns() / 2 <= columns) && (input->Rows() / 2 <= rows))
		{
			CudaMatrix d_A, d_B;

			d_A.width = input->columns;
			d_A.height = input->rows;
			d_A.stride = input->stride;
			d_A.elements = input->Buffer();

			d_B.width = columns;
			d_B.height = rows;
			d_B.stride = stride;
			d_B.elements = matrixData;

			MatMaxPooling(d_A, d_B, stream);
		}
	}

	void MaxExpand(IfMatrix * _input, IfMatrix * _referenceData)
	{
		PtMatrix * input = (PtMatrix*)_input;
		PtMatrix * referenceData = (PtMatrix*)_referenceData;

		if ((input->Columns() * 2 >= columns) && (input->Rows() * 2 >= rows) && (referenceData->Columns() * 2 == columns) && (referenceData->Rows() * 2 == rows))
		{
			CudaMatrix d_A, d_B, d_C;

			d_A.width = input->columns;
			d_A.height = input->rows;
			d_A.stride = input->stride;
			d_A.elements = input->Buffer();

			d_B.width = referenceData->columns;
			d_B.height = referenceData->rows;
			d_B.stride = referenceData->stride;
			d_B.elements = referenceData->Buffer();

			d_C.width = columns;
			d_C.height = rows;
			d_C.stride = stride;
			d_C.elements = matrixData;

			MatMaxExpand(d_A, d_B, d_C, stream);
		}
	}
	
	void SetBuffer(matrixFormat * data, int dataSize)
	{
		if ((matrixData != NULL) && (dataSize >= (rows * columns)))
			cudaMemcpy2D(matrixData, pitch, data, columns * sizeof(matrixFormat), columns * sizeof(matrixFormat), rows, cudaMemcpyHostToDevice);
	}

	void GetBuffer(matrixFormat * data, int dataSize)
	{
		if ((matrixData != NULL) && (dataSize >= (rows * columns)))
			cudaMemcpy2D(data, columns * sizeof(matrixFormat), matrixData, pitch, columns * sizeof(matrixFormat), rows, cudaMemcpyDeviceToHost);
	}

	int Rows(void)
	{
		return(rows);
	}

	int Columns(void)
	{
		return(columns);
	}

	int Stride(void)
	{
		return(stride);
	}
};

cudaStream_t PtMatrix::stream = NULL;
int PtMatrix::globalInstances = 0;

int PtMatrix::seed = 3456;

__declspec(dllexport) IfMatrix * CreateIfMatrix(void)
{
	PtMatrix * matrix = new PtMatrix();

	if (matrix != NULL)
		matrix->DuplicateObject();

	return((IfMatrix *)matrix );
}

__declspec(dllexport) IfMatrix * CreateIfMatrix(int _rows, int _columns)
{
	PtMatrix * matrix = new PtMatrix(_rows, _columns);

	if (matrix != NULL)
		matrix->DuplicateObject();

	return((IfMatrix *)matrix);
}

__declspec(dllexport) IfMatrix * CreateIfMatrix(int _rows, int _columns, matrixFormat * data)
{
	PtMatrix * matrix = new PtMatrix(_rows, _columns, data);

	if (matrix != NULL)
		matrix->DuplicateObject();

	return((IfMatrix *)matrix);
}

class ClNnPrimitives : public IfNnPrimitives
{
public:
	ClNnPrimitives(void) {};
	~ClNnPrimitives(void) {};

	void PredictDense(PredictParams &predictParams)
	{
		PtMatrix * inputBuffer = (PtMatrix*)predictParams.inputBuffer;
		PtMatrix * inputSignal = (PtMatrix*)predictParams.inputSignal;
		PtMatrix * signals = (PtMatrix*)predictParams.signals;
		PtMatrix * weights = (PtMatrix*)predictParams.weights;
		PtMatrix * outputSignal = (PtMatrix*)predictParams.outputSignal;
		CudaMatrix d_A, d_B, d_C, d_D, d_E;

		d_A.width = inputBuffer->Columns();
		d_A.height = inputBuffer->Rows();
		d_A.stride = inputBuffer->Stride();
		d_A.elements = inputBuffer->Buffer();

		d_B.width = inputSignal->Columns();
		d_B.height = inputSignal->Rows();
		d_B.stride = inputSignal->Stride();
		d_B.elements = inputSignal->Buffer();

		d_C.width = signals->Columns();
		d_C.height = signals->Rows();
		d_C.stride = signals->Stride();
		d_C.elements = signals->Buffer();

		d_D.width = weights->Columns();
		d_D.height = weights->Rows();
		d_D.stride = weights->Stride();
		d_D.elements = weights->Buffer();

		d_E.width = outputSignal->Columns();
		d_E.height = outputSignal->Rows();
		d_E.stride = outputSignal->Stride();
		d_E.elements = outputSignal->Buffer();

		MatPredictDense( d_A, d_B, d_C, d_D, d_E);
	}

	void BackPropagationDense(BackPropagationParams &backPropagationParams)
	{
		PtMatrix * errorPreviousLayer = (PtMatrix*)backPropagationParams.errorPreviousLayer;
		PtMatrix * weights = (PtMatrix*)backPropagationParams.weights;
		PtMatrix * errorCurrentLayer = (PtMatrix*)backPropagationParams.errorCurrentLayer;
		PtMatrix * errorMatrix = (PtMatrix*)backPropagationParams.errorMatrix;
		PtMatrix * outputCurrentLayer = (PtMatrix*)backPropagationParams.outputCurrentLayer;
		PtMatrix * delta = (PtMatrix*)backPropagationParams.delta;
		PtMatrix * outputPreviousLayer = (PtMatrix*)backPropagationParams.outputPreviousLayer;
		CudaMatrix d_A, d_B, d_C, d_D, d_E, d_F, d_G;

		if (errorPreviousLayer != NULL)
		{
			d_A.width = errorPreviousLayer->Columns();
			d_A.height = errorPreviousLayer->Rows();
			d_A.stride = errorPreviousLayer->Stride();
			d_A.elements = errorPreviousLayer->Buffer();
		}
		else
		{
			d_A.elements = NULL;
		}

		d_B.width = weights->Columns();
		d_B.height = weights->Rows();
		d_B.stride = weights->Stride();
		d_B.elements = weights->Buffer();

		d_C.width = errorCurrentLayer->Columns();
		d_C.height = errorCurrentLayer->Rows();
		d_C.stride = errorCurrentLayer->Stride();
		d_C.elements = errorCurrentLayer->Buffer();

		d_D.width = errorMatrix->Columns();
		d_D.height = errorMatrix->Rows();
		d_D.stride = errorMatrix->Stride();
		d_D.elements = errorMatrix->Buffer();

		d_E.width = outputCurrentLayer->Columns();
		d_E.height = outputCurrentLayer->Rows();
		d_E.stride = outputCurrentLayer->Stride();
		d_E.elements = outputCurrentLayer->Buffer();

		d_F.width = delta->Columns();
		d_F.height = delta->Rows();
		d_F.stride = delta->Stride();
		d_F.elements = delta->Buffer();

		d_G.width = outputPreviousLayer->Columns();
		d_G.height = outputPreviousLayer->Rows();
		d_G.stride = outputPreviousLayer->Stride();
		d_G.elements = outputPreviousLayer->Buffer();

		MatBackPropagationDense(d_A, d_B, d_C, d_D, d_E, d_F, d_G, backPropagationParams.lRate);
	}

	void PredictConvolution(PredictParams &predictParams)
	{
		PtMatrix * inputSignal = (PtMatrix*)predictParams.inputSignal;
		PtMatrix * weights = (PtMatrix*)predictParams.weights;
		PtMatrix * signals = (PtMatrix*)predictParams.signals;
		PtMatrix * outputSignal = (PtMatrix*)predictParams.outputSignal;
		CudaMatrix d_A, d_B, d_C, d_D;

		d_A.width = inputSignal->Columns();
		d_A.height = inputSignal->Rows();
		d_A.stride = inputSignal->Stride();
		d_A.elements = inputSignal->Buffer();

		d_B.width = weights->Columns();
		d_B.height = weights->Rows();
		d_B.stride = weights->Stride();
		d_B.elements = weights->Buffer();

		d_C.width = signals->Columns();
		d_C.height = signals->Rows();
		d_C.stride = signals->Stride();
		d_C.elements = signals->Buffer();

		d_D.width = outputSignal->Columns();
		d_D.height = outputSignal->Rows();
		d_D.stride = outputSignal->Stride();
		d_D.elements = outputSignal->Buffer();

		if ((d_B.width == 3) && (d_B.height == 3))
		{
			cudaStream_t stream = NULL;

#ifdef USE_SEPARATE_STREAM
			cudaStreamCreate(&stream);
#endif
			if (predictParams.convolutionInfo->activation == ActivationType::ActivationTypeTanh)
			{
				MatConvolution(d_A, d_B, d_C, 0, stream);
				MatActivationTanh(d_C, d_D, stream);
			}

			if (predictParams.convolutionInfo->activation == ActivationType::ActivationTypeRelu)
			{
				MatConvolution(d_A, d_B, d_C, 0, stream);
				MatActivationRelu(d_C, d_D, stream);
			}

#ifdef USE_SEPARATE_STREAM
			cudaStreamDestroy(stream);
#endif
		}
	}

	void BackPropagationConvolution(BackPropagationParams &backPropagationParams)
	{
		PtMatrix * errorPreviousLayer = (PtMatrix*)backPropagationParams.errorPreviousLayer;
		PtMatrix * weights = (PtMatrix*)backPropagationParams.weights;
		PtMatrix * errorCurrentLayer = (PtMatrix*)backPropagationParams.errorCurrentLayer;
		PtMatrix * errorMatrix = (PtMatrix*)backPropagationParams.errorMatrix;
		PtMatrix * outputCurrentLayer = (PtMatrix*)backPropagationParams.outputCurrentLayer;
		PtMatrix * delta = (PtMatrix*)backPropagationParams.delta;
		PtMatrix * outputPreviousLayer = (PtMatrix*)backPropagationParams.outputPreviousLayer;
		CudaMatrix d_A, d_B, d_C, d_D, d_E, d_F, d_G;

		if (errorPreviousLayer != NULL)
		{
			d_A.width = errorPreviousLayer->Columns();
			d_A.height = errorPreviousLayer->Rows();
			d_A.stride = errorPreviousLayer->Stride();
			d_A.elements = errorPreviousLayer->Buffer();
		}
		else
		{
			d_A.elements = NULL;
		}

		d_B.width = weights->Columns();
		d_B.height = weights->Rows();
		d_B.stride = weights->Stride();
		d_B.elements = weights->Buffer();

		d_C.width = errorCurrentLayer->Columns();
		d_C.height = errorCurrentLayer->Rows();
		d_C.stride = errorCurrentLayer->Stride();
		d_C.elements = errorCurrentLayer->Buffer();

		d_D.width = errorMatrix->Columns();
		d_D.height = errorMatrix->Rows();
		d_D.stride = errorMatrix->Stride();
		d_D.elements = errorMatrix->Buffer();

		d_E.width = outputCurrentLayer->Columns();
		d_E.height = outputCurrentLayer->Rows();
		d_E.stride = outputCurrentLayer->Stride();
		d_E.elements = outputCurrentLayer->Buffer();

		d_F.width = delta->Columns();
		d_F.height = delta->Rows();
		d_F.stride = delta->Stride();
		d_F.elements = delta->Buffer();

		d_G.width = outputPreviousLayer->Columns();
		d_G.height = outputPreviousLayer->Rows();
		d_G.stride = outputPreviousLayer->Stride();
		d_G.elements = outputPreviousLayer->Buffer();

		MatBackPropagationConvolution(d_A, d_B, d_C, d_D, d_E, d_F, d_G, backPropagationParams.lRate, backPropagationParams.convolutionInfo->activation);
	}

	void PredictPooling(PredictParams &predictParams)
	{
		PtMatrix * inputSignal = (PtMatrix*)predictParams.inputSignal;
		PtMatrix * outputSignal = (PtMatrix*)predictParams.outputSignal;
		CudaMatrix d_A, d_B;

		d_A.width = inputSignal->Columns();
		d_A.height = inputSignal->Rows();
		d_A.stride = inputSignal->Stride();
		d_A.elements = inputSignal->Buffer();

		d_B.width = outputSignal->Columns();
		d_B.height = outputSignal->Rows();
		d_B.stride = outputSignal->Stride();
		d_B.elements = outputSignal->Buffer();

		cudaStream_t stream = NULL;
#ifdef USE_SEPARATE_STREAM
		cudaStreamCreate(&stream);
#endif
		MatMaxPooling(d_A, d_B, stream);
#ifdef USE_SEPARATE_STREAM
		cudaStreamDestroy(stream);
#endif
	}

	void BackPropagationPooling(BackPropagationParams &backPropagationParams)
	{
		PtMatrix * errorPreviousLayer = (PtMatrix*)backPropagationParams.errorPreviousLayer;
		PtMatrix * errorCurrentLayer = (PtMatrix*)backPropagationParams.errorCurrentLayer;
		PtMatrix * outputCurrentLayer = (PtMatrix*)backPropagationParams.outputCurrentLayer;
		PtMatrix * outputPreviousLayer = (PtMatrix*)backPropagationParams.outputPreviousLayer;
		CudaMatrix d_A, d_B, d_C, d_D;

		d_A.width = errorPreviousLayer->Columns();
		d_A.height = errorPreviousLayer->Rows();
		d_A.stride = errorPreviousLayer->Stride();
		d_A.elements = errorPreviousLayer->Buffer();

		d_B.width = errorCurrentLayer->Columns();
		d_B.height = errorCurrentLayer->Rows();
		d_B.stride = errorCurrentLayer->Stride();
		d_B.elements = errorCurrentLayer->Buffer();

		d_C.width = outputCurrentLayer->Columns();
		d_C.height = outputCurrentLayer->Rows();
		d_C.stride = outputCurrentLayer->Stride();
		d_C.elements = outputCurrentLayer->Buffer();

		d_D.width = outputPreviousLayer->Columns();
		d_D.height = outputPreviousLayer->Rows();
		d_D.stride = outputPreviousLayer->Stride();
		d_D.elements = outputPreviousLayer->Buffer();

		MatBackPropagationPooling(d_A, d_B, d_C, d_D);
	}
};

ClNnPrimitives clNnPrimitives;

__declspec(dllexport) IfNnPrimitives * GetPrimitives(void)
{
	return((IfNnPrimitives *)(&clNnPrimitives));
}

