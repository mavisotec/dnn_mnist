#include "stdafx.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "IfMatrix.hpp"
#include "DnnLayer.h"
#include <ippi.h>

void transformImage(int * data, const int * source, int sizeX, int sizeY, unsigned int move, unsigned int turn, int noiseInfluence)
{
	int stride = sizeX;
	unsigned int _move = move % 9;
	unsigned int _turn = turn % 3;
	int offsetX;
	int offsetY;
	double phi;
	double pi = 3.1415926;

	switch (_move)
	{
		case 0:
			offsetX = -1;
			offsetY = -1;
			break;

		case 1:
			offsetX = 0;
			offsetY = -1;
			break;

		case 2:
			offsetX = 1;
			offsetY = -1;
			break;

		case 3:
			offsetX = -1;
			offsetY = 0;
			break;

		case 4:
			offsetX = 0;
			offsetY = 0;
			break;

		case 5:
			offsetX = 1;
			offsetY = 0;
			break;

		case 6:
			offsetX = -1;
			offsetY = 1;
			break;

		case 7:
			offsetX = 0;
			offsetY = 1;
			break;

		case 8:
			offsetX = 1;
			offsetY = 1;
			break;
	}

	switch (_turn)
	{
		case 0:
			phi = 10 * pi / 180.0;
			break;

		case 1:
			phi = 0;
			break;

		case 2:
			phi = -10 * pi / 180.0;
			break;
	}

	for (int y = 0; y < sizeY; ++y)
	{
		for (int x = 0; x < sizeX; ++x)
		{
			double a = x - sizeX / 2;
			double b = y - sizeY / 2;
			double arc = atan2(b, a) + phi;
			double r = sqrt(a * a + b * b);
			double x1 = sizeX / 2 + offsetX + r * cos(arc);
			double y1 = sizeY / 2 + offsetY + r * sin(arc);
			int c = (int)x1;
			int d = (int)y1;
			double restX = x1 - c;
			double restY = y1 - d;
			double val1d;
			double val3d;
			int noise = rand();
			int toDark = (noise < noiseInfluence) ? 1 : 0;
			int toBright = (noise > ( RAND_MAX - noiseInfluence)) ? 1 : 0;

			if (c < 0)
				c = 0;

			if (d < 0)
				d = 0;

			if (c >= sizeX)
				c = sizeX - 1;

			if (d >= sizeY)
				d = sizeY - 1;

			int val1 = ((source[c + d * stride]) & 0x00FF0000) >> 16;
			int val2 = ((c + 1) < sizeX) ? ((source[c + 1 + d * stride]) & 0x00FF0000) >> 16 : val1;
			int val3 = ((d + 1) < sizeY) ? ((source[c + (d + 1) * stride]) & 0x00FF0000) >> 16 : val1;
			int val4 = (((c + 1) < sizeX) && ((d + 1) < sizeY)) ? ((source[c + 1 + (d + 1) * stride]) & 0x00FF0000) >> 16 : val1;

			val1d = val1 + (val2 - val1) * restX;
			val3d = val3 + (val4 - val3) * restX;
			val1d += (val3d - val1d) * restY;

			int valR = (int)(val1d );

			if ((valR < 0) || toDark )
				valR = 0;

			if ((valR > 255) || toBright )
				valR = 255;

			val1 = ((source[c + d * stride]) & 0x0000FF00) >> 8;
			val2 = ((c + 1) < sizeX) ? ((source[c + 1 + d * stride]) & 0x0000FF00) >> 8 : val1;
			val3 = ((d + 1) < sizeY) ? ((source[c + (d + 1) * stride]) & 0x0000FF00) >> 8 : val1;
			val4 = (((c + 1) < sizeX) && ((d + 1) < sizeY)) ? ((source[c + 1 + (d + 1) * stride]) & 0x0000FF00) >> 8 : val1;

			val1d = val1 + (val2 - val1) * restX;
			val3d = val3 + (val4 - val3) * restX;
			val1d += (val3d - val1d) * restY;

			int valG = (int)(val1d);

			if ((valG < 0) || toDark )
				valG = 0;

			if ((valG > 255) || toBright)
				valG = 255;

			val1 = ((source[c + d * stride]) & 0x000000FF);
			val2 = ((c + 1) < sizeX) ? ((source[c + 1 + d * stride]) & 0x000000FF) : val1;
			val3 = ((d + 1) < sizeY) ? ((source[c + (d + 1) * stride]) & 0x000000FF) : val1;
			val4 = (((c + 1) < sizeX) && ((d + 1) < sizeY)) ? ((source[c + 1 + (d + 1) * stride]) & 0x000000FF) : val1;

			val1d = val1 + (val2 - val1) * restX;
			val3d = val3 + (val4 - val3) * restX;
			val1d += (val3d - val1d) * restY;

			int valB = (int)(val1d);

			if ((valB < 0) || toDark)
				valB = 0;

			if ((valB > 255) || toBright)
				valB = 255;

			data[x + y * stride] = (valR << 16) + (valG << 8) + valB;
		}
	}
}

matrixFormat CompareMatrix(IfMatrix * mA, IfMatrix * mB)
{
	matrixFormat delta = 100;
	int sizeA = mA->Columns() * mA->Rows();
	int sizeB = mB->Columns() * mB->Rows();

	if (sizeA == sizeB)
	{
		matrixFormat a;
		matrixFormat * bufferA = new matrixFormat[sizeA];
		matrixFormat * bufferB = new matrixFormat[sizeB];

		delta = 0;

		mA->GetBuffer(bufferA, sizeA);
		mB->GetBuffer(bufferB, sizeB);

		for (int i = 0; i < sizeA; ++i)
		{
			a = bufferA[i] - bufferB[i];

			if (a > 1e-8)
				a = -a;

			delta += a * a;
		}

		delete[] bufferA;
		delete[] bufferB;
	}

	return((float)sqrt(delta));
}


IfPtDeepNN * deepNN_1 = NULL;

extern "C"
{
	__declspec(dllexport) void WINAPI createDNN(bool useGPU)
	{
		PREFER_DEVICE device = (useGPU) ? PREFER_DEVICE::PREFER_GPU : PREFER_DEVICE::PREFER_CPU;

		if (deepNN_1 != NULL)
			deepNN_1->ReleaseObject();

		deepNN_1 = NULL;

		deepNN_1 = CreateDnn(device);

		deepNN_1->SetInputLayerSize(28 * 28, 1);
		deepNN_1->AddLayer(LayerType::LayerTypeDense, 28 * 28, 50, 0);
		deepNN_1->AddLayer(LayerType::LayerTypeDense, 50, 10, 0);
	}

	__int64 frequency, start, stop, elapsedTime;

	__declspec(dllexport) void WINAPI destroyDNN(void)
	{
		if (deepNN_1 != NULL)
			deepNN_1->ReleaseObject();

		deepNN_1 = NULL;
	}

	__declspec(dllexport) double WINAPI trainDNN(int * data, int size, int label, double lRate)
	{
		double cost = 1000000;
		IfMatrix * output_1 = NULL;
		matrixFormat max = -0.98F;
		int * tempData = new int[28 * 28];
		static unsigned int move = 4, turn = 1;

		transformImage(tempData, data, 28, 28, ++move, ++turn, RAND_MAX / 50);

		if (deepNN_1 != NULL)
		{
			if (28 * 28 == deepNN_1->Inputsize())
			{
				matrixFormat * table = new matrixFormat[28 * 28];

				memset(table, 0, 28 * 28 * sizeof(matrixFormat));

				for (int count = 0; count < size; ++count)
					table[count] = (float)((tempData[count] & 0x000000ff) / 128.0 - 1.0);

				IfMatrix * input = deepNN_1->GetInputSignal();
				input->SetBuffer(table, size);

				QueryPerformanceCounter((LARGE_INTEGER *)(&start));

				cost = deepNN_1->Train(label, (float)lRate);

				QueryPerformanceCounter((LARGE_INTEGER *)(&stop));

				elapsedTime += stop - start;

				delete[] table;
			}
		}

		return(cost);
	}

	__declspec(dllexport) int WINAPI querryDNN(int * data, int size, double & score)
	{
		int label = -1;
		IfMatrix * output_1 = NULL;
		matrixFormat max = -0.98F;

		if (deepNN_1 != NULL)
		{
			if (28 * 28 == deepNN_1->Inputsize())
			{
				matrixFormat * table = new matrixFormat[28 * 28];

				memset(table, 0, 28 * 28 * sizeof(matrixFormat));

				for (int count = 0; count < size; ++count)
					table[count] = (float)((data[count] & 0x000000ff) / 128.0 - 1.0);

				IfMatrix * input = deepNN_1->GetInputSignal();
				input->SetBuffer(table, size);

				QueryPerformanceCounter((LARGE_INTEGER *)(&start));

				output_1 = deepNN_1->Querry();

				QueryPerformanceCounter((LARGE_INTEGER *)(&stop));

				elapsedTime += stop - start;

				delete[] table;
			}
		}

		if (output_1 != NULL)
		{
			matrixFormat * outBuffer = new matrixFormat[output_1->Rows()];

			output_1->GetBuffer(outBuffer, output_1->Rows());

			for (int count = 0; count < output_1->Rows(); ++count)
			{
				if (outBuffer[count] > max)
				{
					max = outBuffer[count];
					label = count;
					score = max;
				}
			}

			delete[] outBuffer;
		}

		return(label);
	}

	__declspec(dllexport) void WINAPI saveDNN(char * data)
	{
		char fnBuffer[512];

		if (deepNN_1 != NULL)
		{
			strcpy_s(fnBuffer, sizeof(fnBuffer), data);
			strcat_s(fnBuffer, sizeof(fnBuffer), "_part1.ptnd");
			deepNN_1->SaveDNN(fnBuffer);
		}
	}

	__declspec(dllexport) void WINAPI loadDNN(char * data)
	{
		char fnBuffer[512];

		if (deepNN_1 != NULL)
		{
			strcpy_s(fnBuffer, sizeof(fnBuffer), data);
			strcat_s(fnBuffer, sizeof(fnBuffer), "_part1.ptnd");
			deepNN_1->LoadDNN(fnBuffer);
		}
	}

	__declspec(dllexport) void WINAPI timerStart(void)
	{
		QueryPerformanceFrequency((LARGE_INTEGER *)(&frequency));

		elapsedTime = 0;
	}

	__declspec(dllexport) double WINAPI elapsedTime_ms(void)
	{
		return((elapsedTime * 1000.0) / (double)frequency);
	}

	__declspec(dllexport) void WINAPI transformImageData(int * data, const int * source, int sizeX, int sizeY, unsigned int move, unsigned int turn, int noiseInfluence)
	{
		transformImage(data, source, sizeX, sizeY, move, turn, noiseInfluence);
	}
}
