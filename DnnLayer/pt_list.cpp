//	Zinc Application Framework - Z_LIST.CPP ----> PT_LIST
/* noafx */
#include "stdafx.h"

#include <stdio.h>
#include <string.h>

#include "Interface.h"
#include "if_threadpool.hpp"
#include "pt_list.hpp"

// ----- PtElement ----------------------------------------------------------

__callcnv PtElement::PtElement( void ) : previous( NULL ), next( NULL )
{
}

__callcnv PtElement::~PtElement(void)
{
}

int __callcnv PtElement::ListIndex( void ) const
{
	// Get the element index.
	int index = 0;
	for ( PtElement *element = previous ; element ; element = element->previous )
		index++;

	// Return the element index.
	return (index);
}

PtElement * __callcnv PtElement::Next( void ) const
{
	return( next ) ;
}

void __callcnv PtElement::Next( PtElement * element )
{
	next = element ;
}

PtElement * __callcnv PtElement::Previous( void ) const
{
	return( previous ) ;
}

void __callcnv PtElement::Previous( PtElement * element )
{
	previous = element ;
}

// ----- PtList -------------------------------------------------------------

__callcnv PtList::PtList( void ) : first( NULL ), last( NULL ), current( NULL ), count(0)
{
}

__callcnv PtList::~PtList(void)
{
	Destroy() ;
}

PtElement * __callcnv PtList::Add( PtElement *newElement )
{
	return ( Add( newElement, NULL ) ) ;
}

PtElement * __callcnv PtList::Add( PtElement *newElement, PtElement *_positionElement )
{
	PtElement * pElement, * positionElement = NULL ;

    for ( pElement = First() ; pElement ; pElement = pElement->Next() )
    {
    	if ( pElement == _positionElement )
        	positionElement = _positionElement ;
    }

	// Add the element to the list.
	if ( !first )					// Put at the first of the list.
	{
		newElement->Previous( NULL ) ;
        newElement->Next( NULL ) ;
		first = last = newElement ;
	}
	else if ( !positionElement )	// Put at the end of the list.
	{
		newElement->Previous( last ) ;
		newElement->Next( NULL ) ;
		last->Next( newElement ) ;
		last = newElement ;
	}
	else							// Put before the specified element.
	{
		newElement->Previous( positionElement->Previous() ) ;
		newElement->Next( positionElement ) ;
		if ( !positionElement->Previous() )
			first = newElement ;
		else
			positionElement->Previous()->Next( newElement ) ;
		positionElement->Previous( newElement ) ;
	}

	++count ;

	// Return a pointer to the element.
	return ( newElement ) ;
}

PtElement * __callcnv PtList::Subtract( PtElement *element )
{
	// Make sure an element is specified.
	if ( !element )
		return( NULL ) ;

	// Make sure the element is in the list.
	PtElement *tElement = element ;
	while ( tElement->Previous() )
		tElement = tElement->Previous() ;
	if ( tElement != first )
		return ( element ) ;

	// Delete the specified element from the list.
	if ( element->Previous() )
		element->Previous()->Next( element->Next() ) ;
	else
		first = element->Next() ;
	if ( element->Next() )
		element->Next()->Previous( element->Previous() ) ;
	else
		last = element->Previous() ;
	if ( current == element )
		current = NULL ;

	--count ;

	// Return the next element.
	tElement = element->Next() ;
	element->Next( NULL ) ;
    element->Previous( NULL ) ;
	return ( tElement ) ;
}

void __callcnv PtList::Destroy(void)
{
	PtElement *tElement ;

	// Delete all the elements in the list.
	for ( PtElement *element = first ; element ; )
	{
		tElement = element ;
		element = element->Next() ;
		delete tElement ;
	}
	// Reset the list element pointers and count.
	first = last = current = NULL ;
	count = 0 ;
}

PtElement * __callcnv PtList::Get( int index )
{
	// Negative indexes don't match any elements.
	if ( index < 0 )
		return ( NULL ) ;

	// Get the list element.
	PtElement * element ;
	for ( element = first ; index > 0 && element ; element = element->Next() )
		index-- ;

	// Return the matching element.  If no match, element will be NULL.
	return ( element ) ;
}

int __callcnv PtList::Index( const PtElement *element ) const
{
	// Make sure there is an element.
	if ( !element )
		return ( -1 ) ;

	// Get the element index.
	int index = 0 ;
	PtElement *tElement ;
	for ( tElement = first ; tElement && tElement != element ; tElement = tElement->Next() )
		index++ ;

	// Return the element index.  If no match, return value is -1.
	if ( tElement )
		return ( index ) ;
	return ( -1 ) ;
}

int __callcnv PtList::Count( void ) const
{
	return ( count ) ;
}

PtElement * __callcnv PtList::First( void ) const
{
	return ( first ) ;
}

PtElement * __callcnv PtList::Last( void ) const
{
	return ( last ) ;
}

PtElement *	__callcnv PtList::Current( void ) const
{
	return ( current ) ;
}

void __callcnv PtList::SetCurrent( PtElement *element )
{
	current = element ;
}

// end of file

