// IfMatrix.cpp : Definiert die exportierten Funktionen f�r die DLL-Anwendung.
//

#include "stdafx.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "Interface.h"
#include "if_threadpool.hpp"
#include "pt_list.hpp"

#include "IfMatrix.hpp"
#include "DnnLayer.h"

#include <ippi.h>

typedef TPtElement<IfMatrix> IfMatrixElement;
typedef TPtList<IfMatrixElement> IfMatrixList;

__interface NeuralLayer : PtInterface
{
	void Predict(IfMatrix * inputSignal, IfMatrix * outputSignal, int index);
	void Backpropagation(IfMatrix * errorCurrentLayer, IfMatrix * outputCurrentLayer, IfMatrix * outputPreviousLayer, IfMatrix * errorPreviousLayer, float lRate, int index);
	void Predict(IfMatrixList * inputSignal);
	void Backpropagation(IfMatrixList * errorCurrentLayer, IfMatrixList * outputPreviousLayer, IfMatrixList * errorPreviousLayer, float lRate);
	IfMatrixList * ResultList(void);
	IfMatrixList * ErrorList(void);
	void GetLayerInfo(LayerData & layerData);
	void GetLayerData(const LayerData & layerData, matrixFormat * data);
	void SetLayerData(const LayerData & layerData, matrixFormat * data);
};

typedef TPtElement<NeuralLayer> PtNeuralLayerElement;
typedef TPtList<PtNeuralLayerElement> PtNeuralLayerList;

class DenseLayer : public NeuralLayer
{
	int instances;

	int inputs;
	int neurons;
	bool useBais;

	IfMatrix	* inputBuffer;
	IfMatrix	* weights;
	IfMatrix	* signals;												// signale vor der Activierungsfunktion

	IfMatrixList * resultList;
	IfMatrixList * errorList;

	// common use for temp values
	IfMatrix * delta;
	IfMatrix * errorMatrix;

	MatrixClassFactory * matrixClassFactory;

public:

	DenseLayer(MatrixClassFactory * _matrixClassFactory, int _inputs, int _neurons, bool _useBais)
	{
		matrixClassFactory = _matrixClassFactory;
		matrixClassFactory->DuplicateObject();

		instances = 0;

		inputs = _inputs;
		neurons = _neurons;
		useBais = _useBais;

		inputBuffer = matrixClassFactory->CreateIfMatrix(inputs + (useBais ? 1 : 0), 1);
		weights = matrixClassFactory->CreateIfMatrix(neurons, inputs + (useBais ? 1 : 0));
		signals = matrixClassFactory->CreateIfMatrix(neurons, 1);

		weights->InitWithRandomValues(useBais);

		resultList = new IfMatrixList();
		errorList = new IfMatrixList();

		resultList->Add(new IfMatrixElement(matrixClassFactory->CreateIfMatrix(neurons, 1)));
		errorList->Add(new IfMatrixElement(matrixClassFactory->CreateIfMatrix(neurons, 1)));

		delta = matrixClassFactory->CreateIfMatrix(neurons, inputs + (useBais ? 1 : 0));
		errorMatrix = matrixClassFactory->CreateIfMatrix(neurons, 1);

		DuplicateObject();
	}

	virtual ~DenseLayer(void)
	{
		delete resultList;
		delete errorList;
		inputBuffer->ReleaseObject();
		weights->ReleaseObject();
		signals->ReleaseObject();

		delta->ReleaseObject();
		errorMatrix->ReleaseObject();

		matrixClassFactory->ReleaseObject();
	}

	void __stdcall DuplicateObject(void)
	{
		++instances;
	}

	void __stdcall ReleaseObject(void)
	{
		if (--instances == 0)
			delete this;
	}

	void Predict(IfMatrix * inputSignal, IfMatrix * outputSignal, int index)
	{
		if ((outputSignal->Rows() == neurons) && (outputSignal->Columns() == 1))
		{
			PredictParams predictParams;

			predictParams.inputSignal = inputSignal;
			predictParams.outputSignal = outputSignal;
			predictParams.inputBuffer = inputBuffer;
			predictParams.weights = weights;
			predictParams.signals = signals;

			matrixClassFactory->GetPrimitives()->PredictDense(predictParams);
		}
	}

	void Backpropagation(IfMatrix * errorCurrentLayer, IfMatrix * outputCurrentLayer, IfMatrix * outputPreviousLayer, IfMatrix * errorPreviousLayer, float lRate, int index)
	{
		IfMatrix * outPreviousLayer = (useBais || (outputPreviousLayer == NULL)) ? inputBuffer : outputPreviousLayer;

		BackPropagationParams backPropagationParams;

		backPropagationParams.errorCurrentLayer = errorCurrentLayer;
		backPropagationParams.weights = weights;
		backPropagationParams.errorMatrix = errorMatrix;
		backPropagationParams.outputPreviousLayer = outPreviousLayer;
		backPropagationParams.outputCurrentLayer = outputCurrentLayer;
		backPropagationParams.delta = delta;
		backPropagationParams.errorPreviousLayer = errorPreviousLayer;
		backPropagationParams.lRate = lRate;

		matrixClassFactory->GetPrimitives()->BackPropagationDense(backPropagationParams);
	}

	void Predict(IfMatrixList * inputSignal)
	{
		IfMatrixElement * meOut = resultList->First();

		for (IfMatrixElement * meIn = inputSignal->First(); meIn != NULL; meIn = meIn->Next())
		{
			Predict(meIn->item, meOut->item, 0);
			meOut = meOut->Next();
		}
	}

	void Backpropagation(IfMatrixList * errorCurrentLayer, IfMatrixList * outputPreviousLayer, IfMatrixList * errorPreviousLayer, float lRate)
	{
		IfMatrixElement * meOCL = resultList->First();
		IfMatrixElement * meOPL = outputPreviousLayer->First();
		IfMatrixElement * meEPL = (errorPreviousLayer != NULL ) ? errorPreviousLayer->First() : NULL;
		
		for (IfMatrixElement * meIn = errorCurrentLayer->First(); meIn != NULL; meIn = meIn->Next())
		{
			Backpropagation(meIn->item, meOCL->item, meOPL->item, ( meEPL != NULL) ? meEPL->item : NULL, lRate, meIn->ListIndex());
			meOCL = meOCL->Next();
			meOPL = meOPL->Next();
			if ( meEPL )
				meEPL = meEPL->Next();
		}
	}

	IfMatrixList * ResultList(void)
	{
		return(resultList);
	}

	IfMatrixList * ErrorList(void)
	{
		return(errorList);
	}

	void GetLayerInfo(LayerData & layerData)
	{
		layerData.type = LayerType::LayerTypeDense;
		layerData.param1 = inputs;
		layerData.param2 = neurons;
		layerData.param3 = useBais ? 1 : 0;
		layerData.param4 = 0;
		layerData.param5 = 0;
		layerData.param6 = 0;
		layerData.dataSize = weights->Rows() * weights->Columns();
	}

	void GetLayerData(const LayerData & layerData, matrixFormat * data)
	{
		if (layerData.dataSize == (weights->Rows() * weights->Columns()))
			weights->GetBuffer(data, layerData.dataSize);
	}

	void SetLayerData(const LayerData & layerData, matrixFormat * data)
	{
		if ((layerData.type == LayerType::LayerTypeDense) && (layerData.param1 == inputs) && (layerData.param2 == neurons) &&
			(layerData.dataSize == (weights->Rows() * weights->Columns())))
			weights->SetBuffer(data, layerData.dataSize);
	}
};

class ConvolutionLayer2D : public NeuralLayer
{
	int instances;

	int inputRows;
	int inputColumns;
	int frames;
	int kernels;

	ActivationType activation;
	int weightsAreFix;

	IfMatrixList * weightList;

	IfMatrixList * resultList;
	IfMatrixList * errorList;

	// common use for temp values
	IfMatrix * delta;
	IfMatrix * errorMatrix;

	MatrixClassFactory * matrixClassFactory;

	int interval;

	ConvolutionInfo convolutionInfo;

public:

	ConvolutionLayer2D(MatrixClassFactory * _matrixClassFactory, int _inputRows, int _inputColumns, int _frames, int _kernels, int _activation, int _weightsAreFix = 0)
	{
		matrixClassFactory = _matrixClassFactory;
		matrixClassFactory->DuplicateObject();

		instances = 0;

		inputRows = _inputRows;
		inputColumns = _inputColumns;
		frames = _frames;
		kernels = _kernels;
		activation = (ActivationType)_activation;

		if (_weightsAreFix != 0)
		{
			// jeden zyklus die Gewichte aktualisieren
			weightsAreFix = 1;
		}
		else
		{
			weightsAreFix = 0;
			interval = 0;
		}

		weightList = new IfMatrixList();

		for (int i = 0; i < kernels * frames; ++i)
		{
			IfMatrix * weight = matrixClassFactory->CreateIfMatrix(CONVOLUTION_ROWS, CONVOLUTION_COLUMNS);

			weight->InitWithRandomValues(false);
			weightList->Add(new IfMatrixElement(weight));
		}

		resultList = new IfMatrixList();
		errorList = new IfMatrixList();

		for (int i = 0; i < kernels * frames; ++i)
		{
			IfMatrixElement * mResult = new IfMatrixElement(matrixClassFactory->CreateIfMatrix(inputRows, inputColumns));

			mResult->item->InitWithZero();
			resultList->Add(mResult);

			errorList->Add(new IfMatrixElement(matrixClassFactory->CreateIfMatrix(inputRows, inputColumns)));
		}

		delta = matrixClassFactory->CreateIfMatrix(CONVOLUTION_ROWS, CONVOLUTION_COLUMNS);
		errorMatrix = matrixClassFactory->CreateIfMatrix(inputRows, inputColumns);

		convolutionInfo.sizeS1_width = inputColumns;
		convolutionInfo.sizeS1_height = inputRows;
		convolutionInfo.sizeS2_width = CONVOLUTION_COLUMNS;
		convolutionInfo.sizeS2_height = CONVOLUTION_ROWS;
		convolutionInfo.convolutionTempBuffer = NULL;
		convolutionInfo.activation = activation;

		IppiSize sizeS1, sizeS2;
		pt32s ippBufferSize;

		sizeS1.width = convolutionInfo.sizeS1_width;
		sizeS1.height = convolutionInfo.sizeS1_height;
		sizeS2.width = convolutionInfo.sizeS2_width;
		sizeS2.height = convolutionInfo.sizeS2_height;

		if (ippStsNoErr == ippiConvGetBufferSize(sizeS1, sizeS2, IppDataType::ipp32f, 1, IppiROIShape::ippiROIValid, &ippBufferSize))
		{
			convolutionInfo.convolutionTempBuffer = new pt08u[ippBufferSize];
		}

		DuplicateObject();
	}

	virtual ~ConvolutionLayer2D(void)
	{
		if (convolutionInfo.convolutionTempBuffer != NULL)
			delete[] convolutionInfo.convolutionTempBuffer;

		delete resultList;
		delete errorList;
		delete weightList;

		delta->ReleaseObject();
		errorMatrix->ReleaseObject();

		matrixClassFactory->ReleaseObject();
	}

	void __stdcall DuplicateObject(void)
	{
		++instances;
	}

	void __stdcall ReleaseObject(void)
	{
		if (--instances == 0)
			delete this;
	}

	void Predict(IfMatrix * inputSignal, IfMatrix * outputSignal, int index)
	{
		if ((outputSignal->Rows() == inputRows) && (outputSignal->Columns() == inputColumns))
		{
			PredictParams predictParams;

			predictParams.inputSignal = inputSignal;
			predictParams.outputSignal = outputSignal;
			predictParams.weights = weightList->Get(index)->item;
			predictParams.convolutionInfo = &convolutionInfo;
			predictParams.signals = errorMatrix;				// used as temp bufffer

			matrixClassFactory->GetPrimitives()->PredictConvolution(predictParams);
		}
	}

	void Backpropagation(IfMatrix * errorCurrentLayer, IfMatrix * outputCurrentLayer, IfMatrix * outputPreviousLayer, IfMatrix * errorPreviousLayer, float lRate, int index)
	{
		BackPropagationParams backPropagationParams;

		backPropagationParams.errorCurrentLayer = errorCurrentLayer;
		backPropagationParams.weights = weightList->Get(index)->item;
		backPropagationParams.errorMatrix = errorMatrix;
		backPropagationParams.outputPreviousLayer = outputPreviousLayer;
		backPropagationParams.outputCurrentLayer = outputCurrentLayer;
		backPropagationParams.delta = delta;
		backPropagationParams.errorPreviousLayer = errorPreviousLayer;
		backPropagationParams.lRate = (weightsAreFix == 0) ? lRate : 0;
		backPropagationParams.convolutionInfo = &convolutionInfo;

		matrixClassFactory->GetPrimitives()->BackPropagationConvolution(backPropagationParams);
	}

	void Predict(IfMatrixList * inputSignal)
	{
		IfMatrixElement * meOut = resultList->First();

		for (IfMatrixElement * meIn = inputSignal->First(); meIn != NULL; meIn = meIn->Next())
		{
			for (int i = 0; i < kernels; ++i)
			{
				Predict(meIn->item, meOut->item, i);
				meOut = meOut->Next();
			}
		}
	}

	void Backpropagation(IfMatrixList * errorCurrentLayer, IfMatrixList * outputPreviousLayer, IfMatrixList * errorPreviousLayer, float lRate)
	{
		IfMatrixElement * meECL = errorCurrentLayer->First();
		IfMatrixElement * meOCL = resultList->First();
		IfMatrixElement * meEPL = ( errorPreviousLayer != NULL ) ? errorPreviousLayer->First() : NULL;
		
		for (IfMatrixElement * meOPL = outputPreviousLayer->First(); meOPL != NULL; meOPL = meOPL->Next())
		{
			for (int i = 0; i < kernels; ++i)
			{
				if ( i == interval)
					Backpropagation(meECL->item, meOCL->item, meOPL->item, (meEPL != NULL) ? meEPL->item : NULL, lRate, i);

				meECL = meECL->Next();
				meOCL = meOCL->Next();
			}

			// only one time for a kernel per iteration 
			interval += (kernels + 1);

			while (interval >= kernels)
				interval -= kernels;

			if (meEPL != NULL)
				meEPL = meEPL->Next();
		}
	}

	IfMatrixList * ResultList(void)
	{
		return(resultList);
	}

	IfMatrixList * ErrorList(void)
	{
		return(errorList);
	}

	void GetLayerInfo(LayerData & layerData)
	{
		layerData.type = LayerType::LayerTypeConvolution;
		layerData.param1 = inputRows;
		layerData.param2 = inputColumns;
		layerData.param3 = frames;
		layerData.param4 = kernels;
		layerData.param5 = activation;

		switch (weightsAreFix)
		{
			case 0:
				layerData.param6 = 0;
				break;

			case 1:
				layerData.param6 = 1000;
				break;

			default:
				layerData.param6 = weightsAreFix;
				break;
		}

		layerData.dataSize = CONVOLUTION_ROWS * CONVOLUTION_COLUMNS * kernels;
	}

	void GetLayerData(const LayerData & layerData, matrixFormat * data)
	{
		if (layerData.dataSize == (CONVOLUTION_ROWS * CONVOLUTION_COLUMNS * kernels))
		{
			for (int i = 0; i < kernels; ++i)
				weightList->Get(i)->item->GetBuffer(data + i * CONVOLUTION_ROWS * CONVOLUTION_COLUMNS, layerData.dataSize - i * CONVOLUTION_ROWS * CONVOLUTION_COLUMNS);
		}
	}

	void SetLayerData(const LayerData & layerData, matrixFormat * data)
	{
		if ((layerData.type == LayerType::LayerTypeConvolution) && (layerData.param1 == inputRows) && (layerData.param2 == inputColumns) && (layerData.param4 == kernels) &&
			(layerData.dataSize == (CONVOLUTION_ROWS * CONVOLUTION_COLUMNS * kernels)))
		{
			for (int i = 0; i < kernels; ++i)
				weightList->Get(i)->item->SetBuffer(data + i * CONVOLUTION_ROWS * CONVOLUTION_COLUMNS, CONVOLUTION_ROWS * CONVOLUTION_COLUMNS);
		}
	}
};

class MaxPoolLayer2D : public NeuralLayer
{
	int instances;

	int inputRows;
	int inputColumns;
	int frames;

	int isAbleToLearn;

	IfMatrixList * resultList;
	IfMatrixList * errorList;

	MatrixClassFactory * matrixClassFactory;

public:

	MaxPoolLayer2D(MatrixClassFactory * _matrixClassFactory, int _inputRows, int _inputColumns, int _frames)
	{
		matrixClassFactory = _matrixClassFactory;
		matrixClassFactory->DuplicateObject();

		int outputRows = _inputRows / 2 + (((_inputRows % 2) != 0) ? 1 : 0);
		int outputColumns = _inputColumns / 2 + (((_inputColumns % 2) != 0) ? 1 : 0);

		instances = 0;

		inputRows = _inputRows;
		inputColumns = _inputColumns;
		frames = _frames;

		resultList = new IfMatrixList();
		errorList = new IfMatrixList();

		for (int i = 0; i < frames; ++i)
		{
			resultList->Add(new IfMatrixElement(matrixClassFactory->CreateIfMatrix(outputRows, outputColumns)));
			errorList->Add(new IfMatrixElement(matrixClassFactory->CreateIfMatrix(outputRows, outputColumns)));
		}

		DuplicateObject();
	}

	virtual ~MaxPoolLayer2D(void)
	{
		delete resultList;
		delete errorList;

		matrixClassFactory->ReleaseObject();
	}

	void __stdcall DuplicateObject(void)
	{
		++instances;
	}

	void __stdcall ReleaseObject(void)
	{
		if (--instances == 0)
			delete this;
	}

	void Predict(IfMatrix * inputSignal, IfMatrix * outputSignal, int index)
	{
		PredictParams predictParams;

		predictParams.inputSignal = inputSignal;
		predictParams.outputSignal = outputSignal;

		matrixClassFactory->GetPrimitives()->PredictPooling(predictParams);
	}

	void Backpropagation(IfMatrix * errorCurrentLayer, IfMatrix * outputCurrentLayer, IfMatrix * outputPreviousLayer, IfMatrix * errorPreviousLayer,float lRate, int index)
	{
		if (errorPreviousLayer != NULL)
		{
			BackPropagationParams backPropagationParams;

			backPropagationParams.errorCurrentLayer = errorCurrentLayer;
			backPropagationParams.outputPreviousLayer = outputPreviousLayer;
			backPropagationParams.outputCurrentLayer = outputCurrentLayer;
			backPropagationParams.errorPreviousLayer = errorPreviousLayer;
			backPropagationParams.lRate = lRate;

			matrixClassFactory->GetPrimitives()->BackPropagationPooling(backPropagationParams);
		}
	}

	void Predict(IfMatrixList * inputSignal)
	{
		IfMatrixElement * meOut = resultList->First();

		for (IfMatrixElement * meIn = inputSignal->First(); meIn != NULL; meIn = meIn->Next())
		{
			Predict(meIn->item, meOut->item, 0);
			meOut = meOut->Next();
		}
	}

	void Backpropagation(IfMatrixList * errorCurrentLayer, IfMatrixList * outputPreviousLayer, IfMatrixList * errorPreviousLayer, float lRate)
	{
		IfMatrixElement * meECL = errorCurrentLayer->First();
		IfMatrixElement * meOCL = resultList->First();
		IfMatrixElement * meEPL = (errorPreviousLayer != NULL) ? errorPreviousLayer->First() : NULL;

		for (IfMatrixElement * meOPL = outputPreviousLayer->First(); meOPL != NULL; meOPL = meOPL->Next())
		{
			Backpropagation(meECL->item, meOCL->item, meOPL->item, (meEPL != NULL) ? meEPL->item : NULL, lRate, 0);

			meECL = meECL->Next();
			meOCL = meOCL->Next();

			if (meEPL != NULL)
				meEPL = meEPL->Next();
		}
	}

	IfMatrixList * ResultList(void)
	{
		return(resultList);
	}

	IfMatrixList * ErrorList(void)
	{
		return(errorList);
	}

	void GetLayerInfo(LayerData & layerData)
	{
		layerData.type = LayerType::LayerTypeMaxPooling;
		layerData.param1 = inputRows;
		layerData.param2 = inputColumns;
		layerData.param3 = frames;
		layerData.param4 = 0;
		layerData.param5 = 0;
		layerData.param6 = 0;
		layerData.dataSize = 0;
	}

	void GetLayerData(const LayerData & layerData, matrixFormat * data)
	{
	}

	void SetLayerData(const LayerData & layerData, matrixFormat * data)
	{
	}
};

class FlattenLayer : public NeuralLayer
{
	int instances;

	int inputRows;
	int inputColumns;
	int frames;

	int isAbleToLearn;

	IfMatrixList * resultList;
	IfMatrixList * errorList;

	MatrixClassFactory * matrixClassFactory;

public:

	FlattenLayer(MatrixClassFactory * _matrixClassFactory, int _inputRows, int _inputColumns, int _frames)
	{
		matrixClassFactory = _matrixClassFactory;
		matrixClassFactory->DuplicateObject();

		int outputRows = _inputRows * _inputColumns * _frames;

		instances = 0;

		inputRows = _inputRows;
		inputColumns = _inputColumns;
		frames = _frames;

		resultList = new IfMatrixList();
		errorList = new IfMatrixList();

		resultList->Add(new IfMatrixElement(matrixClassFactory->CreateIfMatrix(outputRows, 1)));
		errorList->Add(new IfMatrixElement(matrixClassFactory->CreateIfMatrix(outputRows, 1)));

		DuplicateObject();
	}

	virtual ~FlattenLayer(void)
	{
		delete resultList;
		delete errorList;

		matrixClassFactory->ReleaseObject();
	}

	void __stdcall DuplicateObject(void)
	{
		++instances;
	}

	void __stdcall ReleaseObject(void)
	{
		if (--instances == 0)
			delete this;
	}

	void Predict(IfMatrix * inputSignal, IfMatrix * outputSignal, int index)
	{
		outputSignal->Copy(inputRows * inputColumns * index, inputSignal);
	}

	void Backpropagation(IfMatrix * errorCurrentLayer, IfMatrix * outputCurrentLayer, IfMatrix * outputPreviousLayer, IfMatrix * errorPreviousLayer, float lRate, int index)
	{
		errorPreviousLayer->Copy(errorCurrentLayer, inputRows * inputColumns * index);
	}

	void Predict(IfMatrixList * inputSignal)
	{
		IfMatrixElement * meOut = resultList->First();

		for (IfMatrixElement * meIn = inputSignal->First(); meIn != NULL; meIn = meIn->Next())
		{
			Predict(meIn->item, meOut->item, meIn->ListIndex());
		}
	}

	void Backpropagation(IfMatrixList * errorCurrentLayer, IfMatrixList * outputPreviousLayer, IfMatrixList * errorPreviousLayer, float lRate)
	{
		IfMatrixElement * meECL = errorCurrentLayer->First();
		IfMatrixElement * meEPL = (errorPreviousLayer != NULL) ? errorPreviousLayer->First() : NULL;

		if (meEPL != NULL)
		{
			for (IfMatrixElement * meOPL = outputPreviousLayer->First(); meOPL != NULL; meOPL = meOPL->Next())
			{
				Backpropagation(meECL->item, NULL, meOPL->item, (meEPL != NULL) ? meEPL->item : NULL, lRate, meOPL->ListIndex());

				if (meEPL != NULL)
					meEPL = meEPL->Next();
			}
		}
	}

	IfMatrixList * ResultList(void)
	{
		return(resultList);
	}

	IfMatrixList * ErrorList(void)
	{
		return(errorList);
	}

	void GetLayerInfo(LayerData & layerData)
	{
		layerData.type = LayerType::LayerTypeFlatten;
		layerData.param1 = inputRows;
		layerData.param2 = inputColumns;
		layerData.param3 = frames;
		layerData.param4 = 0;
		layerData.param5 = 0;
		layerData.param6 = 0;
		layerData.dataSize = 0;
	}

	void GetLayerData(const LayerData & layerData, matrixFormat * data)
	{
	}

	void SetLayerData(const LayerData & layerData, matrixFormat * data)
	{
	}
};

enum DNN_WT_JOB
{
	DNN_STOP = -1,
	DNN_NONE,
	DNN_QUERRY,
	DNN_TRAIN
};

struct ThreadInfo
{
	DNN_WT_JOB				inUse;
	HANDLE					workThread;
	HANDLE					startEvent;
	HANDLE					rdyEvent;
	unsigned long			threadID;
	PtNeuralLayerList		* layerList;
	IfMatrixList			* inputSignalList;
	IfMatrixList			* inputErrorList;
	IfMatrix				* out;
	matrixFormat			lRate;
};

LPTHREAD_START_ROUTINE workThread(ThreadInfo * wtInfo)
{
	DNN_WT_JOB inUse = DNN_NONE;
	PtNeuralLayerElement * le;
	matrixFormat lRate;

	SetEvent(wtInfo->rdyEvent);

	while (inUse != DNN_STOP)
	{
		WaitForSingleObject(wtInfo->startEvent, INFINITE);

		ResetEvent(wtInfo->startEvent);

		inUse = wtInfo->inUse;

		switch (inUse)
		{
			case DNN_NONE:
				SleepEx(0, false);
				break;

			case DNN_QUERRY:
				le = wtInfo->layerList->First();

				while (le != NULL)
				{
					IfMatrixList * previousResultList = (le->Previous() != NULL) ? le->Previous()->item->ResultList() : wtInfo->inputSignalList;
					le->item->Predict(previousResultList);
					le = le->Next();
				}

				le = wtInfo->layerList->Last();

				if (le != NULL)
					wtInfo->out = le->item->ResultList()->First()->item;

				SetEvent(wtInfo->rdyEvent);
				break;

			case DNN_TRAIN:
				le = wtInfo->layerList->Last();

				lRate = wtInfo->lRate;

				while (le != NULL)
				{
					le->item->Backpropagation(le->item->ErrorList(), (le->Previous() != NULL) ? le->Previous()->item->ResultList() : wtInfo->inputSignalList, (le->Previous() != NULL) ? le->Previous()->item->ErrorList() : wtInfo->inputErrorList, lRate);
					le = le->Previous();
				}

				SetEvent(wtInfo->rdyEvent);
				break;
		}
	}

	return(0);
}

class PtDeepNN : public IfPtDeepNN
{
	int inputLayerSize;
	IfMatrixList * inputSignalList;
	IfMatrixList * inputErrorList;						// Fehler des Eingangssignals, normalerweise NULL

	PtNeuralLayerList * layerList = NULL;

	MatrixClassFactory * matrixClassFactory = NULL;

	ThreadInfo	tInfo;
	HANDLE tMutex;

	pt32s instance;

public:

	PtDeepNN(PREFER_DEVICE device, bool backPropInputError)
	{
		matrixClassFactory = new MatrixClassFactory(device);

		inputLayerSize = 0;
		inputSignalList = new IfMatrixList();
		inputErrorList = NULL;

		if (backPropInputError)
			inputErrorList = new IfMatrixList();

		layerList = new PtNeuralLayerList();

		tInfo.inUse = DNN_NONE;
		tInfo.startEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
		tInfo.rdyEvent = CreateEvent(NULL, TRUE, FALSE, NULL);

		tMutex = CreateMutex(NULL, false, NULL);
		tInfo.workThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)workThread, &tInfo, 0, &tInfo.threadID);

		instance = 0;
	}

	~PtDeepNN(void)
	{
		delete inputSignalList;

		if (inputErrorList != NULL)
			delete inputErrorList;

		PtNeuralLayerElement * le = layerList->First();

		delete layerList;

		matrixClassFactory->ReleaseObject();

		tInfo.inUse = DNN_STOP;
		SetEvent(tInfo.startEvent);

		WaitForSingleObject(tInfo.workThread, 200);
		CloseHandle(tInfo.workThread);
		CloseHandle(tInfo.startEvent);
		CloseHandle(tInfo.rdyEvent);

		CloseHandle(tMutex);
	}

	void __stdcall DuplicateObject(void)
	{
		++instance;
	}

	void __stdcall ReleaseObject(void)
	{
		if (--instance == 0)
			delete this;
	}

	void AddLayer(LayerType type, int param1, int param2, int param3, int param4 = 0, int param5 = 0, int param6 = 0)
	{
		PtNeuralLayerElement * le;

		WaitForSingleObject(tMutex, INFINITE);

		switch (type)
		{
			case LayerTypeConvolution:
				le = new PtNeuralLayerElement(new ConvolutionLayer2D(matrixClassFactory, param1, param2, param3, param4, param5, param6));
				layerList->Add(le);
				break;

			case LayerTypeMaxPooling:
				le = new PtNeuralLayerElement(new MaxPoolLayer2D(matrixClassFactory, param1, param2, param3));
				layerList->Add(le);
				break;

			case LayerTypeFlatten:
				le = new PtNeuralLayerElement(new FlattenLayer(matrixClassFactory, param1, param2, param3));
				layerList->Add(le);
				break;

			case LayerTypeDense:
				le = new PtNeuralLayerElement(new DenseLayer(matrixClassFactory, param1, param2, (param3 == 1) ? true : false));
				layerList->Add(le);
				break;
		}

		ReleaseMutex(tMutex);
	}

	void AddLayer(const LayerData & layerData, matrixFormat * data)
	{
		PtNeuralLayerElement * le;

		WaitForSingleObject(tMutex, INFINITE);

		switch (layerData.type)
		{
			case LayerTypeConvolution:
				le = new PtNeuralLayerElement(new ConvolutionLayer2D(matrixClassFactory, layerData.param1, layerData.param2, layerData.param3, layerData.param4, layerData.param5, layerData.param6));
				le->item->SetLayerData(layerData, data);
				layerList->Add(le);
				break;

			case LayerTypeMaxPooling:
				le = new PtNeuralLayerElement(new MaxPoolLayer2D(matrixClassFactory, layerData.param1, layerData.param2, layerData.param3));
				le->item->SetLayerData(layerData, data);
				layerList->Add(le);
				break;

			case LayerTypeFlatten:
				le = new PtNeuralLayerElement(new FlattenLayer(matrixClassFactory, layerData.param1, layerData.param2, layerData.param3));
				le->item->SetLayerData(layerData, data);
				layerList->Add(le);
				break;

			case LayerTypeDense:
				le = new PtNeuralLayerElement(new DenseLayer(matrixClassFactory, layerData.param1, layerData.param2, (layerData.param3 == 1) ? true : false));
				le->item->SetLayerData(layerData, data);
				layerList->Add(le);
				break;
		}

		ReleaseMutex(tMutex);
	}

	IfMatrix * Querry(void)
	{
		WaitForSingleObject(tMutex, INFINITE);

		tInfo.layerList = layerList;
		tInfo.inputSignalList = inputSignalList;
		tInfo.inputErrorList = NULL;
		tInfo.out = NULL;

		ResetEvent(tInfo.rdyEvent);

		tInfo.inUse = DNN_QUERRY;
		SetEvent(tInfo.startEvent);

		WaitForSingleObject(tInfo.rdyEvent, INFINITE);

		ReleaseMutex(tMutex);

		return(tInfo.out);
	}

	double Train(int label, float lRate, matrixFormat * inputError, int sizeInputError)
	{
		double summ = 100;

		WaitForSingleObject(tMutex, INFINITE);

		tInfo.layerList = layerList;
		tInfo.inputSignalList = inputSignalList;
		tInfo.inputErrorList = NULL;
		tInfo.out = NULL;
		tInfo.lRate = lRate;

		ResetEvent(tInfo.rdyEvent);

		tInfo.inUse = DNN_QUERRY;
		SetEvent(tInfo.startEvent);

		WaitForSingleObject(tInfo.rdyEvent, INFINITE);

		if (tInfo.out != NULL)
		{
			int outputLayerSize = tInfo.out->Rows();
			matrixFormat * dataQuerry = new matrixFormat[outputLayerSize];

			tInfo.out->GetBuffer(dataQuerry, outputLayerSize);

			summ = 0;

			// compare with target
			for (int count = 0; count < outputLayerSize; ++count)
			{
				dataQuerry[count] = ((count == label) ? 0.98F : -0.98F) - dataQuerry[count];
				summ += dataQuerry[count] * dataQuerry[count];
			}

			layerList->Last()->item->ErrorList()->First()->item->SetBuffer(dataQuerry, outputLayerSize);

			ResetEvent(tInfo.rdyEvent);

			tInfo.inputErrorList = inputErrorList;
			tInfo.inUse = DNN_TRAIN;

			SetEvent(tInfo.startEvent);

			WaitForSingleObject(tInfo.rdyEvent, INFINITE);

			if ((inputErrorList != NULL) && (inputError != NULL))
				inputErrorList->First()->item->GetBuffer(inputError, sizeInputError);

			delete[] dataQuerry;
		}

		ReleaseMutex(tMutex);

		return(summ);
	}

	void BackPropError(matrixFormat * outputError, int sizeOutputError, float lRate, matrixFormat * inputError, int sizeInputError)
	{
		WaitForSingleObject(tMutex, INFINITE);

		tInfo.layerList = layerList;
		tInfo.inputSignalList = inputSignalList;
		tInfo.inputErrorList = NULL;
		tInfo.out = NULL;
		tInfo.lRate = lRate;

		layerList->Last()->item->ErrorList()->First()->item->SetBuffer(outputError, sizeOutputError);

		ResetEvent(tInfo.rdyEvent);

		tInfo.inputErrorList = inputErrorList;
		tInfo.inUse = DNN_TRAIN;

		SetEvent(tInfo.startEvent);

		WaitForSingleObject(tInfo.rdyEvent, INFINITE);

		if ((inputErrorList != NULL) && (inputError != NULL))
			inputErrorList->First()->item->GetBuffer(inputError, sizeInputError);

		ReleaseMutex(tMutex);
	}

	void SetInputLayerSize(int _rows, int _coulumns)
	{
		WaitForSingleObject(tMutex, INFINITE);

		inputLayerSize = _coulumns * _rows;
		inputSignalList->Add(new IfMatrixElement(matrixClassFactory->CreateIfMatrix(_rows, _coulumns)));

		if ( inputErrorList != NULL)
			inputErrorList->Add(new IfMatrixElement(matrixClassFactory->CreateIfMatrix(_rows, _coulumns)));

		ReleaseMutex(tMutex);
	}

	int Inputsize(void)
	{
		return (inputLayerSize);
	}

	IfMatrix * GetResultOfLayer(int layer, int index)
	{
		IfMatrix * out = NULL;
		PtNeuralLayerElement * le = layerList->Get( layer );

		if (le != NULL)
		{
			out = le->item->ResultList()->Get(index)->item;
		}

		return (out);
	}

	pt32s SaveDNN(pt08s * filename)
	{
		char * stringID = "pt_cnn_layer_file";
		HANDLE file;
		DWORD writeCount;
		pt32s success = 0;
		int dataFormat = sizeof(matrixFormat);
		int layerCount = layerList->Count();
		LayerData layerData;
		int inputColumns = inputSignalList->First()->item->Columns();
		int inputRows = inputSignalList->First()->item->Rows();

		WaitForSingleObject(tMutex, INFINITE);

		file = CreateFile(filename, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, 0, NULL);

		if (file != INVALID_HANDLE_VALUE)
		{
			WriteFile(file, stringID, (DWORD)(strlen( stringID ) + 1), &writeCount, NULL);				// file kennung
			WriteFile(file, &dataFormat, sizeof(int), &writeCount, NULL);								// datenformat 16s, float
			WriteFile(file, &layerCount, sizeof(int), &writeCount, NULL);								// layer count
			WriteFile(file, &inputColumns, sizeof(int), &writeCount, NULL);								// inputColumnSize
			WriteFile(file, &inputRows, sizeof(int), &writeCount, NULL);								// inputRowSize

			for (int i = 0; i < layerCount; ++i)
			{
				matrixFormat * data = NULL;

				layerList->Get(i)->item->GetLayerInfo(layerData);

				if (layerData.dataSize > 0)
					data = new matrixFormat[layerData.dataSize];

				WriteFile(file, &layerData, sizeof(LayerData), &writeCount, NULL);

				layerList->Get(i)->item->GetLayerData(layerData, data);

				WriteFile(file, data, layerData.dataSize * dataFormat, &writeCount, NULL);

				if (data != NULL)
					delete[] data;
			}

			CloseHandle(file);

			success = 1;
		}

		ReleaseMutex(tMutex);

		return(success);
	}

	pt32s LoadDNN(pt08s * filename)
	{
		char * stringID = "pt_cnn_layer_file";
		char * idBuffer = new char[strlen(stringID) + 1];
		HANDLE file;
		DWORD readCount;
		pt32s success = 0;
		int dataFormat = 0;
		int layerCount = 0;
		LayerData layerData;
		int inputColumns = 0;
		int inputRows = 0;

		WaitForSingleObject(tMutex, INFINITE);

		file = CreateFile(filename, GENERIC_READ, 0, NULL, OPEN_EXISTING, 0, NULL);

		if (file != INVALID_HANDLE_VALUE)
		{
			ReadFile(file, idBuffer, (DWORD)(strlen(stringID) + 1), &readCount, NULL);

			if ((readCount == strlen(stringID) + 1) && ( strcmp( stringID, idBuffer) == 0))
			{
				ReadFile(file, &dataFormat, sizeof(int), &readCount, NULL);							// datenformat 16s, float

				if (dataFormat == sizeof(matrixFormat))
				{
					ReadFile(file, &layerCount, sizeof(int), &readCount, NULL);						// layer count
					ReadFile(file, &inputColumns, sizeof(int), &readCount, NULL);					// inputColumnSize
					ReadFile(file, &inputRows, sizeof(int), &readCount, NULL);						// inputRowSize

					PtNeuralLayerElement * le;

					layerList->Destroy();

					inputSignalList->Destroy();

					if (inputErrorList != NULL)
						inputErrorList->Destroy();

					SetInputLayerSize(inputRows, inputColumns);

					for (int i = 0; i < layerCount; ++i)
					{
						matrixFormat * data = NULL;

						ReadFile(file, &layerData, sizeof(LayerData), &readCount, NULL);

						if (layerData.dataSize > 0)
							data = new matrixFormat[layerData.dataSize];

						ReadFile(file, data, layerData.dataSize * dataFormat, &readCount, NULL);

						switch (layerData.type)
						{
							case LayerTypeConvolution:
								le = new PtNeuralLayerElement(new ConvolutionLayer2D(matrixClassFactory, layerData.param1, layerData.param2, layerData.param3, layerData.param4, layerData.param5, layerData.param6));
								le->item->SetLayerData(layerData, data);
								layerList->Add(le);
								break;

							case LayerTypeMaxPooling:
								le = new PtNeuralLayerElement(new MaxPoolLayer2D(matrixClassFactory, layerData.param1, layerData.param2, layerData.param3));
								le->item->SetLayerData(layerData, data);
								layerList->Add(le);
								break;

							case LayerTypeFlatten:
								le = new PtNeuralLayerElement(new FlattenLayer(matrixClassFactory, layerData.param1, layerData.param2, layerData.param3));
								le->item->SetLayerData(layerData, data);
								layerList->Add(le);
								break;

							case LayerTypeDense:
								le = new PtNeuralLayerElement(new DenseLayer(matrixClassFactory, layerData.param1, layerData.param2, (layerData.param3 == 1) ? true : false));
								le->item->SetLayerData(layerData, data);
								layerList->Add(le);
								break;
						}

						if (data != NULL)
							delete[] data;
					}

					success = 1;
				}
			}

			CloseHandle(file);
		}

		delete idBuffer;

		ReleaseMutex(tMutex);

		return(success);
	}

	IfMatrix * GetInputSignal(void)
	{
		return(inputSignalList->First()->item);
	}

	IfMatrix * GetInputError(void)
	{
		IfMatrix * inputError = NULL;

		if (inputErrorList != NULL)
			inputError = inputErrorList->First()->item;
		
		return (inputError);
	}

};

IfPtDeepNN * CreateDnn(PREFER_DEVICE device, bool backPropInputError)
{
	PtDeepNN * dnn = new PtDeepNN(device, backPropInputError);

	if (dnn)
		dnn->DuplicateObject();

	return((IfPtDeepNN * )dnn);
}

