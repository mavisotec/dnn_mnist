#pragma once

#include "Interface.h"

enum LayerType : int
{
	LayerTypeConvolution = 0,
	LayerTypeMaxPooling,
	LayerTypeFlatten,
	LayerTypeDense
};

#ifndef ACTIVATION_TYPE
enum ActivationType : int
{
	ActivationTypeTanh = 0,
	ActivationTypeRelu
};
#endif

struct LayerData
{
	LayerType type;
	int param1;
	int param2;
	int param3;
	int param4;
	int param5;
	int param6;
	int dataSize;
};

__interface IfPtDeepNN : public PtInterface
{
	void			AddLayer(LayerType type, int param1, int param2, int param3, int param4 = 0, int param5 = 0, int param6 = 0);
	void			AddLayer(const LayerData & layerData, matrixFormat * data);
	IfMatrix *		Querry(void);
	double			Train(int label, float lRate, matrixFormat * inputError = NULL, int sizeInputError = 0);
	void			BackPropError(matrixFormat * outputError, int sizeOutputError, float lRate, matrixFormat * inputError = NULL, int sizeInputError = 0);
	void			SetInputLayerSize(int _rows, int _coulumns);
	int				Inputsize(void);
	IfMatrix *		GetResultOfLayer(int layer, int index);
	pt32s			SaveDNN(pt08s * filename);
	pt32s			LoadDNN(pt08s * filename);
	IfMatrix *		GetInputSignal(void);
	IfMatrix *		GetInputError(void);
};

extern IfPtDeepNN * CreateDnn(PREFER_DEVICE device, bool backPropInputError = false);




