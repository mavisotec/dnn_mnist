//	Zinc Application Framework - Z_LIST.HPP  ----> PT_LIST

#if !defined(__PT_LIST_H)
#define __PT_LIST_H

#define __callcnv

// --------------------------------------------------------------------------
// ----- PtElement ----------------------------------------------------------
// --------------------------------------------------------------------------

class PtElement
{
public:
	// --- General members ---
									__callcnv PtElement( void ) ;
	virtual  						__callcnv ~PtElement( void ) ;

	int 							__callcnv ListIndex( void ) const ;
	PtElement * 					__callcnv Next( void ) const ;
	void							__callcnv Next( PtElement * ) ;
	PtElement * 					__callcnv Previous( void ) const  ;
	void		 					__callcnv Previous( PtElement * ) ;

private:

	// --- General members ---
	PtElement *						previous ;
    PtElement *						next ;
} ;

template <class T> class TPtElement : public PtElement
{
	pt08s *							nameID ;
							 
public:
									__callcnv TPtElement( void ) ;
									__callcnv TPtElement( T * _init ) ;
	virtual 						__callcnv ~TPtElement( void ) ;

	int 							__callcnv ListIndex( void ) const ;
	TPtElement<T> *					__callcnv Next( void ) const ;
	void							__callcnv Next( TPtElement<T> * ) ;
	TPtElement<T> *					__callcnv Previous( void ) const ;
	void							__callcnv Previous( TPtElement<T> * ) ;
    void							__callcnv SetNameID( const pt08s * _name ) ;
    const pt08s * 					__callcnv GetNameID( void ) ;

	T *								item;
} ;

template <class T> __callcnv TPtElement<T>::TPtElement( void ) : nameID( NULL ), item( NULL)
{
}

template <class T> __callcnv TPtElement<T>::TPtElement( T * _init ) : nameID(NULL)
{
	item = _init ;
}

template <class T> __callcnv TPtElement<T>::~TPtElement( void )
{
	if ( nameID )
    	delete [] nameID ;

	if (item)
		item->ReleaseObject();
}

template <class T> int __callcnv TPtElement<T>::ListIndex( void ) const
{
	return( PtElement::ListIndex() ) ;
}

template <class T> TPtElement<T> * __callcnv TPtElement<T>::Next( void ) const
{
	return ( ( TPtElement<T> * )( PtElement::Next() ) ) ;
}

template <class T> void __callcnv TPtElement<T>::Next( TPtElement<T> * element )
{
	PtElement::Next( ( PtElement * )element ) ;
}

template <class T> TPtElement<T> * __callcnv TPtElement<T>::Previous( void ) const
{
	return ( ( TPtElement<T> * )( PtElement::Previous() ) ) ;
}

template <class T> void __callcnv TPtElement<T>::Previous( TPtElement<T> * element )
{
	PtElement::Previous( ( PtElement * )element ) ;
}

template <class T> void	__callcnv TPtElement<T>::SetNameID( const pt08s * _name )
{
	pt32s size = StringLen( _name ) + 1 ;

   	if ( nameID )
    	delete [] nameID ;

    nameID = new pt08s[size] ;

    if ( nameID )
    	strcpy_s( nameID, size, _name ) ;
}

template <class T> const pt08s * __callcnv TPtElement<T>::GetNameID( void )
{
	return ( nameID ? nameID : "" ) ;
}

// --------------------------------------------------------------------------
// ----- PtList -------------------------------------------------------------
// --------------------------------------------------------------------------

class PtList
{
public:
	// --- General members ---
									__callcnv PtList( void ) ;
	virtual 						__callcnv ~PtList( void ) ;

	PtElement * 					__callcnv Add( PtElement * newElement ) ;
	PtElement * 					__callcnv Add( PtElement * element, PtElement * position ) ;
	int 							__callcnv Count( void ) const ;
	virtual void 					__callcnv Destroy( void ) ;
	PtElement * 					__callcnv First( void ) const ;
	PtElement * 					__callcnv Get( int index ) ;
	int 							__callcnv Index( PtElement const * element ) const ;
	PtElement * 					__callcnv Last( void ) const ;
	PtElement * 					__callcnv Subtract( PtElement * element ) ;

	// --- Attributes ---
	// get
	PtElement *						__callcnv Current( void ) const ;
	// set
	void 		 					__callcnv SetCurrent( PtElement *element ) ;

private:
	// --- General members ---
	PtElement *						first ;
    PtElement *						last ;
    PtElement *						current ;
	int 							count ;
} ;

template <class T> class TPtList : public PtList
{
public:
									__callcnv TPtList( void ) ;
	virtual 						__callcnv ~TPtList( void ) ;

	T *								__callcnv Add( T * newElement ) ;
	T * 							__callcnv Add( T * element, T * position ) ;
	int 							__callcnv Count( void ) const ;
	virtual void 					__callcnv Destroy( void ) ;
	T *								__callcnv First( void ) const ;
	T *								__callcnv Get( int index ) ;
	T *								__callcnv Get( const pt08s * _name ) ;
	int 							__callcnv Index( T const *element ) const ;
	T *								__callcnv Last( void ) const ;
	T *								__callcnv Subtract( T *element ) ;

	T *			  					__callcnv Current( void ) const ;
	void 		 					__callcnv SetCurrent( T *element ) ;
} ;

template <class T> __callcnv TPtList<T>::TPtList( void )
{
}

template <class T> __callcnv TPtList<T>::~TPtList( void )
{
}

template <class T> T * __callcnv TPtList<T>::Add( T * newElement )
{
	return ( ( T * )( PtList::Add( ( PtElement * )newElement ) ) ) ;
}

template <class T> T * __callcnv TPtList<T>::Add( T * element, T * position )
{
	return ( ( T * )( PtList::Add( ( PtElement * )element, ( PtElement * )position ) ) ) ;
}

template <class T> int __callcnv TPtList<T>::Count( void ) const
{
	return ( PtList::Count() ) ;
}

template <class T> void __callcnv TPtList<T>::Destroy( void )
{
	PtList::Destroy() ;
}

template <class T> T * __callcnv TPtList<T>::First( void ) const
{
	return ( ( T * )( PtList::First() ) ) ;
}

template <class T> T * __callcnv TPtList<T>::Get( int index )
{
	return( ( T * )( PtList::Get( index ) ) ) ;
}

template <class T> T * __callcnv TPtList<T>::Get( const pt08s * _name )
{
   	T * element = First() ;
    while ( element && strcmp( element->GetNameID(), _name ) )
    	element = element->Next() ;
   	return( element ) ;
} ;

template <class T> int __callcnv TPtList<T>::Index( T const *element ) const
{
	return( PtList::Index( ( PtElement * )element ) ) ;
}

template <class T> T * __callcnv TPtList<T>::Last( void ) const
{
	return ( ( T * )( PtList::Last() ) ) ;
}

template <class T> T * __callcnv TPtList<T>::Subtract( T *element )
{
	return ( ( T * )( PtList::Subtract( ( PtElement * )element ) ) ) ;
}

template <class T> T * __callcnv TPtList<T>::Current( void ) const
{
	return ( ( T * )( PtList::Current() ) ) ;
}

template <class T> void __callcnv TPtList<T>::SetCurrent( T *element )
{
	PtList::SetCurrent( ( PtElement * )element ) ;
}

#endif
